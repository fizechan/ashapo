<%
Function htmlspecialchars(someString)
	htmlspecialchars = replace(replace(replace(replace(someString, "&", "&amp;"), ">", "&gt;"), "<", "&lt;"), """", "&quot;")
End Function
Function file_get_contents(url,encode)
	If encode = "" Or encode = Null Then
		encode = "UTF-8"
	End If
	On Error Resume Next
	Dim Khttp 
	Set Khttp = server.createobject("Microsoft.XMLHTTP") 
	Khttp.open "GET",url,False 
	Khttp.send()
	If Khttp.readystate<>4 Or khttp.status<>200 Then  
	  file_get_contents = "{""multiPageResult"":{""results"":[],""page"":{""pageSize"":20,""pageCount"":0,""totalCount"":0,""pageNo"":1}}}"
	  Exit Function 
	End If 
	file_get_contents = bytesToBstr(Khttp.responseBody,encode) 	
	Set Khttp=Nothing 
	If err.number<>0 Then
		file_get_contents = "{""multiPageResult"":{""results"":[],""page"":{""pageSize"":20,""pageCount"":0,""totalCount"":0,""pageNo"":1}}}"
	End If
End Function
Function BytesToBstr(body,Cset) 
	Dim objstream 
	Set objstream = Server.CreateObject("adodb.stream") 
	objstream.Type = 1 
	objstream.Mode =3 
	objstream.Open 
	objstream.Write body 
	objstream.Position = 0 
	objstream.Type = 2 
	objstream.Charset = Cset 
	BytesToBstr = objstream.ReadText  
	objstream.Close 
	Set objstream = Nothing 
End Function
Function fileExt(filename)
	Dim tempd
	tempd = Split(filename,".")
	fileExt = LCase(tempd(UBound(tempd)))
End Function
Function DirectoryExists(dirPath)
	Dim fso
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	DirectoryExists = fso.FolderExists(Server.MapPath(dirPath))
	Set fso = Nothing
End Function
Function CreateFolderIfNotExit(path)
	'On Error Resume Next
	Dim FolderPath,FSO
	FolderPath = server.MapPath(path)
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	If FSO.FolderExists(FolderPath)=False Then
		FSO.CreateFolder(FolderPath)
	End If
	Set FSO=Nothing
End Function
Function DelDirAllFile(path,delself)
	'On Error Resume Next
	Dim FolderPath,FSO,Files
	FolderPath = server.MapPath(path)
	Set FSO = Server.CreateObject("Scripting.FileSystemObject")
	If FSO.FolderExists(FolderPath)=False Then
		Set FSO=Nothing
		Exit Function
	Else
		Files = fileList(path)
		For Each file In Files
			Call FSO.DeleteFile(Server.MapPath(file),True)
		Next
		If delself = True Then
			Call FSO.DeleteFolder(FolderPath,True)
		End If
	End If
	Set FSO=Nothing
End Function
Function fileList(dirPath)
	If Len(dirPath) <> InStrRev(dirPath,"/") Then
		dirPath = dirPath + "/"
	End If
	tempDIr = dirPath
	If DirectoryExists(dirPath) = False Then
		fileList = Null
	Else
		Dim fs,fo,ListArr()
		Set fs=Server.CreateObject("Scripting.FileSystemObject")
		Set fo=fs.GetFolder(Server.MapPath(dirPath))
		If fo.Files.Count = 0 Then
			fileList = ListArr
			Exit Function
		End If
		ReDim ListArr(fo.Files.Count-1)
		Dim i : i = 0
		For Each file In fo.Files
			ListArr(i) = tempDir & file.Name
			i = i + 1
		Next
		fileList = ListArr
	End If
End Function
Function fileAcceptList(dirPath,extStr)
	Dim templist,temparr()
	templist = fileList(dirPath)
	Dim arri : arri = 0
	For Each filestr In templist
		If InStr(extStr,fileExt(filestr)) > 0 Then
			ReDim Preserve temparr(arri)
			temparr(arri) = filestr
			arri = arri + 1
		End If
	Next
	fileAcceptList = temparr
End Function
Function GetTimeStr()
	Dim dtmNow : dtmNow = Date()
	Dim m_strDate,m_lngTime
	m_strDate  = Year(dtmNow)&Right("0"&Month(dtmNow),2)&Right("0"&Day(dtmNow),2)
	m_lngTime  = Clng(Timer()*1000)
	m_lngTime=m_lngTime+1
	GetTimeStr=m_strDate&Right("00000000"&m_lngTime,8)
End Function
Function NewFileName(path,filetype)
	Dim tmpfullpath
	tmpfullpath = path & GetTimeStr() & "." & filetype
	If isFile(tmpfullpath) = True Then
		NewFileName = NewFileName(path,filetype)
	Else
		NewFileName = tmpfullpath
	End If
End Function
Function CheckURL(strUrl)
	On Error Resume Next
	Dim XMLHTTP
	Set XMLHTTP = Server.CreateObject("Microsoft.XMLHTTP")
	XMLHTTP.open "HEAD",strUrl,false
	XMLHTTP.send()
	CheckURL=(XMLHTTP.status=200)
	Set XMLHTTP = Nothing
End Function
Function isFile(filePath)
	isFile = False
	On Error Resume Next
	Dim fso
	Set fso = Server.CreateObject("Scripting.FileSystemObject")
	If fso.FileExists(Server.MapPath(filePath)) Then isFile = True
	Set fso = Nothing
End Function
Function get_headers(imguri)
	On Error Resume Next
	Dim Retrieval
	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP") 
	Retrieval.Open "Get", imguri, False, "", "" 
	Retrieval.Send
	If Retrieval.readyState <> 4 Then
		get_headers = "error:server is down"
		Set Retrieval = Nothing
		Exit Function
	ElseIf Retrieval.Status = 200 Then
		get_headers = Retrieval.getAllResponseHeaders()
	End If
	If Err.Number<>0 Then
		get_headers = "error:system error happen"
	End If
	Set Retrieval = Nothing
End Function
Function SaveRemoteFile(LocalFileName,RemoteFileUrl) 
	On Error Resume Next
	Dim Ads,Retrieval,GetRemoteData 
	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP") 
	Retrieval.Open "Get", RemoteFileUrl, False, "", "" 
	Retrieval.Send 
	GetRemoteData = Retrieval.ResponseBody
	Set Retrieval = Nothing 
	Set Ads = Server.CreateObject("Adodb.Stream") 
	Ads.Type = 1 
	Ads.Open 
	Ads.Write GetRemoteData 
	Ads.SaveToFile server.MapPath(LocalFileName),2 
	Ads.Cancel() 
	Ads.Close()
	Set Ads=Nothing 
	If Err.Number<>0 Then
		SaveRemoteFile = False
	Else
		SaveRemoteFile = True
	End If
End Function
Function RemoteFileSize(RemoteFileUrl)
	On Error Resume Next
	Dim Ads,Retrieval,GetRemoteData,FileSize
	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
	Retrieval.Open "Get", RemoteFileUrl, False, "", ""
	Retrieval.Send
	GetRemoteData = Retrieval.ResponseBody
	Set Retrieval = Nothing
	FileSize = Len(GetRemoteData)
	If Err.Number<>0 Then
		RemoteFileSize = FileSize/8
	Else
		RemoteFileSize = 0
	End If
End Function
Function SaveBase64ToImg(fullpath,base64str)
	On Error Resume Next
	Set oStream = Server.Createobject("ADODB.Stream")
	oStream.Type = 1
	oStream.Mode = 3
	oStream.Open
	Set oDoc = Server.CreateObject("Msxml2.DOMDocument")
	oDoc.loadXML "<root/>"
	oDoc.documentElement.DataType = "bin.base64"
	oDoc.documentElement.text = base64str
	oStream.Write oDoc.DocumentElement.NodeTypedValue
	oStream.SaveToFile server.MapPath(fullpath),2
	oStream.Close
	Set oDoc = Nothing
	Set oStream = Nothing
	If Err.Number<>0 Then
		SaveBase64ToImg = False
	Else
		SaveBase64ToImg = True
	End If
End Function
%>