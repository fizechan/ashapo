<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001" %>
<%
Response.ContentType="application/json"
Response.Charset = "UTF-8"
%>
<!--#include file="UpLoadClass.asp"-->
<!--#include file="JSON_2.0.4.asp"-->
<!--#include file="FunctionList.asp"-->
<%
Dim savePath,maxSize,allowFiles,upload
savePath = "/UploadFiles/images/"
maxSize = 1048576	'1M
allowFiles = "gif/jpg/jpeg/png/bmp"
Dim s_url,s_title,s_original,s_state
s_url = ""
s_title = ""
s_original = ""
s_state = "初始值"
CreateFolderIfNotExit(savePath)
Set upload = New UpLoadClass
upload.Charset = "UTF-8"
upload.FileType = allowFiles
upload.MaxSize = maxSize
upload.SavePath = savePath
upload.Open()
s_title =  htmlspecialchars(upload.Form("pictitle"))
s_original = htmlspecialchars(upload.Form("upfile_Name"))
Select Case upload.error
	Case -1
		s_state = "请选择文件。"
	Case 0
		s_state = "SUCCESS"
		s_url = savePath & upload.Form("upfile")
	Case 1
		s_state = "文件过大！"
	Case 2
		s_state = "上传文件扩展名是不允许的扩展名！"
	Case 3
		s_state = "上传文件扩展名是不允许的扩展名！"
	Case Else
		s_state = "文件上传出错！"
End Select
Set upload = Nothing
Dim objjs
Set objjs = jsObject()
objjs("url") = s_url
objjs("title") = s_title
objjs("original") = s_original
objjs("state") = s_state
Response.Clear()
objjs.Flush
%>