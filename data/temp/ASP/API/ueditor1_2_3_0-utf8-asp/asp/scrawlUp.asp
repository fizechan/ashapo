<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001" %>
<%
Response.ContentType="text/html"
'Response.ContentType="application/json"
Response.Charset = "UTF-8"
%>
<!--#include file="UpLoadClass.asp"-->
<!--#include file="JSON_2.0.4.asp"-->
<!--#include file="FunctionList.asp"-->
<%
Dim savePath,maxSize,allowFiles,tmpPath,upload
savePath = "/UploadFiles/images/"
maxSize = 1048576	'1M
allowFiles = "gif/jpg/jpeg/png/bmp"
tmpPath = "/UploadFiles/tmp/"
Dim action : action = htmlspecialchars(Request("action"))
CreateFolderIfNotExit(savePath)
CreateFolderIfNotExit(tmpPath)
Dim s_url,s_state
If action = "tmpImg" Then
	Set upload = New UpLoadClass
	upload.Charset = "UTF-8"
	upload.FileType = allowFiles
	upload.MaxSize = maxSize
	upload.SavePath = tmpPath
	upload.Open()
	Select Case upload.error
		Case -1
			s_state = "请选择文件。"
		Case 0
			s_state = "SUCCESS"
			s_url = tmpPath & upload.Form("upfile")
		Case 1
			s_state = "文件过大！"
		Case 2
			s_state = "上传文件扩展名是不允许的扩展名！"
		Case 3
			s_state = "上传文件扩展名是不允许的扩展名！"
		Case Else
			s_state = "文件上传出错！"
	End Select
	Set upload = Nothing
	Dim outstr
	outstr = "<script>parent.ue_callback('" & s_url & "','" & s_state & "')</script>"
	Response.Write(outstr)
Else
	Dim imgcontent
	imgcontent = Request.Form("content")
	s_url = NewFileName(savePath,"png")
	Dim result
	result = SaveBase64ToImg(s_url,imgcontent)
	If result = False Then
		s_url = ""
		s_state = "涂鸦上传上传出错"
	Else
		s_state = "SUCCESS"
	End If
	Call DelDirAllFile(tmpPath,False)
	Dim objjs
	Set objjs = jsObject()
	objjs("url") = s_url
	objjs("state") = s_state
	Response.Clear()
	objjs.Flush
End If
%>