<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001" %>
<%
Response.ContentType="application/json"
Response.Charset = "UTF-8"
%>
<!--#include file="JSON_2.0.4.asp"-->
<!--#include file="FunctionList.asp"-->
<%
Dim savePath,maxSize,allowFiles
savePath = "/UploadFiles/images/"
maxSize = 1048576	'1M
allowFiles = "gif/jpg/jpeg/png/bmp"
Dim uri,ArrUri,tmpName(),tmpNamei
tmpNamei = 0
uri = htmlspecialchars(Request.Form("upfile"))
uri = replace(uri,"&amp;","&")
ArrUri = Split(uri, "ue_separate_ue")
Dim filetype,heads,typetrue,headtrue,filefullpath,result
For Each tempuri In ArrUri
	filetype = fileExt(tempuri)
	heads = get_headers(tempuri)
	If InStr(allowFiles,filetype) <= 0 Then
		typetrue = False
	Else
		typetrue = True
	End If
	If InStr(heads,"image") <= 0 Then
		headtrue = False
	Else
		headtrue = True
	End If
	ReDim Preserve tmpName(tmpNamei)
	If InStr(LCase(tempuri),"http") <> 1 Then
		tmpName(tmpNamei) = "error"
	ElseIf CheckURL(tempuri) = False Then
		tmpName(tmpNamei) = "error"
	ElseIf typetrue = False Or headtrue = False Then
		tmpName(tmpNamei) = "error"
	ElseIf RemoteFileSize(tempuri) > maxSize Then
		tmpName(tmpNamei) = "error"
	Else
		filefullpath = NewFileName(savePath,filetype)
		result = SaveRemoteFile(filefullpath,tempuri)
		If result = True Then
			tmpName(tmpNamei) = filefullpath
		Else
			tmpName(tmpNamei) = "error"
		End If
	End If
	tmpNamei = tmpNamei + 1
Next
Dim objjs
Set objjs = jsObject()
objjs("url") = Join(tmpName,"ue_separate_ue")
objjs("tip") = "远程图片抓取成功！"
objjs("srcUrl") = uri
Response.Clear()
objjs.Flush
%>