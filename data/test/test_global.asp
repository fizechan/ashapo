<%
Session.CodePage = 65001
Response.Charset = "UTF-8"
Const ASHAPO_DEBUG = True
%>
<!--#include file="../ashapo/Global.asp"-->
<%
Dim Global
Set Global = New AshapoGlobal
''Global.Config("abc") = "123456"
''Response.Write(Global.Config("abc"))
''Global.Fso.CreateFolder("cfz123")

Call Global.Asp.Check("test_global")
Call Global.Asp.Use("/test/Test")
Call Global.Asp.UseAs("/test/kkk/Test", "TestKKK")

Dim t_test
Set t_test = New Test
Call t_test.test01()
Set t_test = Nothing

Dim t_test_kkk
Set t_test_kkk = New TestKKK
Call t_test_kkk.test01()
Set t_test_kkk = Nothing

Set Global = Nothing
%>