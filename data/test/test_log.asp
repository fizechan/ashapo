<%
Session.CodePage = 65001
Response.Charset = "UTF-8"
%>
<!--#include file="../Ashapo/Global.asp"-->
<!--#include file="../Ashapo/Core/AshapoLog.class.asp"-->
<%
Dim Global  ''必须使用这个变量名，否则出错
Set Global = New AshapoGlobal
Dim log
Set log = New AshapoLog
log.Path = "/Data/log/test.log"
log.Level = "info"
log.Format = "full"
log.Write("这是日志主体")
log.Debug("测试信息")
log.Info("系统信息")
log.Warn("警告信息")
log.SQL("SQL语句")
log.Error("错误信息")
Set log = Nothing
Set Global = Nothing
%>