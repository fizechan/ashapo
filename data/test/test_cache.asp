<%
Session.CodePage = 65001
Response.Charset = "UTF-8"
Const ASHAPO_DEBUG = True
%>
<!--#include file="../ashapo/Global.asp"-->
<%
Dim Global
Set Global = New AshapoGlobal
Call Global.Asp.Check("test_cache")

Call Global.Asp.Use("/ashapo/core/AshapoCache")

Dim cache1
Set cache1 = (New AshapoCache).Init( "File", 3600, "")
Response.Write(cache1("cfz"))
Response.Write("<br/>")
cache1("cfz") = "陈峰展"
Response.Write(cache1("cfz"))
Set cache1 = Nothing

Response.Write("<br/>")

Dim cache2
Set cache2 = (New AshapoCache).Init( "Application", 3600, "")
Response.Write(cache2("cfz2"))
Response.Write("<br/>")
cache2("cfz2") = "陈峰展2"
Response.Write(cache2("cfz2"))
Set cache2 = Nothing

Set Global = Nothing
%>