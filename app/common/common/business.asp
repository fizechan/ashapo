<%
'''加密密码
'p_data:待加密字符串
Function encrypt(Byval p_data)
	Sa.Use(ASHAPO_ROOT_PATH & "/extend/Md5")
	Dim t_md5 : Set t_md5 = New Md5
	encrypt = t_md5.To32( Sa.Config("AUTH_CODE") & t_md5.To32( p_data ) )
End Function

'''系统邮件发送函数,成功时返回空字符，失败时返回错误描述字符串
'p_to:接收邮件者邮箱,支持数组
Function send_mail(Byval p_to, Byval p_name, Byval p_subject, Byval p_body, Byval p_basedir, Byval p_attachment)
	Sa.Use(ASHAPO_ROOT_PATH & "/extend/Mail")
	Dim t_mail : Set t_mail = New Mail
	t_mail.SMTP = Sa.Config("SMTP_HOST")
	t_mail.Port = Sa.Config("SMTP_PORT")
	t_mail.LoginName = Sa.Config("SMTP_USER")
	t_mail.LoginPass = Sa.Config("SMTP_PASS")
	t_mail.FromMail = Sa.Config("FROM_EMAIL")
	t_mail.FromName = Sa.Config("FROM_NAME")
	
	'''todo 需要验证
	t_mail.ToMail = p_to & "(" & p_name & ")"

	t_mail.ToMail = p_to

	t_mail.Subject = p_subject
	t_mail.Content = p_body
	
	If t_mail.Send() Then
		send_mail = ""
	Else
		send_mail = t_mail.Error
	End If
	Set t_mail = Nothing
End Function

'''发送EDM邮件,返回发送结果
'''todo 暂不支持直接图片
Function send_edm(Byval p_to, Byval p_name, Byval p_edm, Byval p_subject, Byval p_replace)
	Sa.Use(ASHAPO_ROOT_PATH & "/extend/Fso")
	Dim t_fso : Set t_fso = New Fso
	Dim t_body : t_body = t_fso.Read( ASHAPO_ROOT_PATH & "/media/edm/" & p_edm & "/" & p_edm & ".html" )
	If Has(t_body) Then
		Dim t_place
		For Each t_place In p_replace
			t_body = Replace( t_body, t_place, p_replace(t_place) )
		Next
		Dim t_rst : t_rst = send_mail( p_to, p_name, p_subject, t_body, ASHAPO_ROOT_PATH & "/media/edm/" & p_edm & "/", "")
		If t_rst = "" Then
			send_edm = True
		Else
			'log_app("邮件发送失败:" & t_rst)
			send_edm = False
		End If
	Else
		'log_app("系统错误,未找到EDM模版" & p_edm)
		send_edm = False
	End If
End Function
%>