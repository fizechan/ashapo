<%
Class CoreConfig

	Private s_model

	'''构造
	Private Sub Class_Initialize()
		Set s_model = Sa.Model("core_config")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_model = Nothing
	End Sub
	
	'获取动态配置值，仅支持标量
	'p_key:配置名称
	Public Function getOne(Byval p_key)
		Dim t_cache_key : t_cache_key = "config_" & p_key
		If Has(Sa.Cache(t_cache_key)) Then
			getOne = Sa.Cache(t_cache_key)
			Exit Function
		End If
		Dim t_one : Set t_one = s_model.Where("[key]='" & p_key & "'").Find()
		If Has(t_one) Then
			Sa.Cache(t_cache_key) = t_one("value")
			getOne = t_one("value")
		Else
			getOne = ""
		End If
	End Function

End Class
%>