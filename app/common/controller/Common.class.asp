<%

'''通用控制器基类
Class Common

	Private s_pageNow

	'''构造
	Private Sub Class_Initialize()
		Call Sa.UseAs(ASHAPO_ROOT_PATH & "/extend/Pager", "AshapoPager")
		
		s_pageNow = 0
		checkGroup_()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub
	
	'''操作不存在时返回404,渲染各自的404页面
    Public Sub empty_()
		''使HTTP返回404状态码
		Response.Status = "404 Not Found" 
		Sa.View.Path("Public:404").Display()
    End Sub
	
	'''检测分组站点状态
	Private Sub checkGroup_()
		Dim t_errcode : t_errcode = 0
		Dim t_errmsg : t_errmsg = ""
		Dim t_options : Set t_options = Sa.Config("APP_OPTIONS")
		If t_options.Exists( Sa.Constant("MODULE_NAME") ) Then
			Select Case t_options( Sa.Constant("MODULE_NAME") )
			Case 0
				''正常,Nothing
			Case 1
				''该分组关闭
				t_errcode = 100
				t_errmsg = "工程师正在加班加点更新中，<br/>请稍后再访问吧！"
			End Select
		End If
		If t_errcode <> 0 Then
			If Sa.Request.IsAjax() Then
				Dim t_json : Set t_json = Sa.Dictionary
				t_json("errcode") = t_errcode
				t_json("errmsg") = t_errmsg
				Sa.AjaxReturn( t_json )
			Else
				Call Sa.Error( t_errmsg, "NULL", 0 )
			End If
		End If
	End Sub

	'''取得当前分页
	Public Property Get PageNow()
		If s_pageNow > 0 Then
			PageNow = s_pageNow
		Else
			Dim t_now : t_now = Sa.Request("p")
			If t_now<>"" And IsNumeric(t_now) Then
				t_now = Cint(t_now)
			Else
				t_now = 1
			End If
			s_pageNow = t_now
			PageNow = s_pageNow
		End If
	End Property

	'''返回一个分页对象
	'p_target:指定目标页
	'p_count:记录总个数
	Public Function pager(Byval p_target, Byval p_count)
		Dim t_pager : Set t_pager = New AshapoPager
		Call t_pager.Set("first", "<a href=""{url}"" class=""first"">{page}</a>")
		Call t_pager.Set("last", "<a href=""{url}"" class=""last"">{page}</a>")
		Call t_pager.Set("prev", "<a href=""{url}"" class=""prev"">&lt;</a>")
		Call t_pager.Set("next", "<a href=""{url}"" class=""prev"">&gt;</a>")
		Call t_pager.Set("prevg", "<a href=""{url}"" class=""prevg"">&lt;&lt;</a>")
		Call t_pager.Set("nextg", "<a href=""{url}"" class=""prevg"">&gt;&gt;</a>")
		Call t_pager.Set("nmod", "<a href=""{url}"" class=""current"">{page}</a>")
		t_pager.Model = "{first}{prevg}{prev}{list}{next}{nextg}{last}"
		
		'Dim t_target
		't_target = Sa.Constant("__SELF__")
		'If InStr(t_target, "?")=0 Then
		'	t_target = t_target & "?p={p}"
		'Else
		'	t_target = t_target & "&p={p}"
		'End If
		
		Dim t_size : t_size = Sa.Config("PAGE_SIZE")
		Call t_pager.Init(p_target, t_size, PageNow, p_count)
		pager = t_pager.OutPut
	End Function
End Class
%>