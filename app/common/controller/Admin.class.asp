<%
'''后台控制器基类
Class Admin
	
	'''通用控制器基类对象
	Public BaseCommon

	'''完整的一级菜单目录
	Private s_admin_big_menu

	'''完整的二级菜单目录
	Private s_admin_sub_menu

	'''完整的后台菜单目录
	Private s_admin_all_menu

	'''构造
	Private Sub Class_Initialize()
		Sa.Use(ASHAPO_APP_PATH & "/common/controller/Common")
		Sa.Use(ASHAPO_APP_PATH & "/common/model/CoreConfig")

		Set BaseCommon = New Common
		setAdminBigMenu_()
		setAdminSubMenu_()
		setAdminAllMenu_()
		checkLogin_()
		checkToken_()
		checkRBAC_()

		Sa.Dim("big_menu")
		big_menu = big_menu_()

		Sa.Dim("sub_menu")
		sub_menu = sub_menu_()

		Sa.Dim("current_nav")
		current_nav = current_nav_()
		Sa.Dim("my")
		Set my = Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") )

		Sa.Dim("site")
		Set site = Sa.Dictionary

		Dim t_config : Set t_config = New CoreConfig
		site("BACK_NAME") = t_config.getOne("BACK_NAME")
		site("WEB_ICP") = t_config.getOne("WEB_ICP")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set site = Nothing
		Set s_admin_sub_menu = Nothing
		Set s_admin_big_menu = Nothing
		Set s_admin_all_menu = Nothing
		Set BaseCommon = Nothing
	End Sub
	
	'''设置完整的一级菜单目录
	Private Sub setAdminBigMenu_()
		Set s_admin_big_menu = Sa.Dictionary
		
		Set s_admin_big_menu("Index") = Sa.Dictionary
		s_admin_big_menu("Index")("name") = "首页"
		s_admin_big_menu("Index")("url") = "/?m=Admin&c=Info&a=index"
		s_admin_big_menu("Index")("module") = "Index|Info"
		
		Set s_admin_big_menu("Member") = Sa.Dictionary
		s_admin_big_menu("Member")("name") = "用户管理"
		
		Set s_admin_big_menu("Zixun") = Sa.Dictionary
		s_admin_big_menu("Zixun")("name") = "资讯管理"
		
		Set s_admin_big_menu("Store") = Sa.Dictionary
		s_admin_big_menu("Store")("name") = "门店管理"
		
		Set s_admin_big_menu("News") = Sa.Dictionary
		s_admin_big_menu("News")("name") = "文章管理"
		
		Set s_admin_big_menu("Interact") = Sa.Dictionary
		s_admin_big_menu("Interact")("name") = "互动管理"
		s_admin_big_menu("Interact")("url") = "/?m=Admin&c=Faq&a=index"
		s_admin_big_menu("Interact")("module") = "Faq|Guessing|Employee|Vote"
		
		Set s_admin_big_menu("Weixin") = Sa.Dictionary
		s_admin_big_menu("Weixin")("name") = "微信管理"
		
		Set s_admin_big_menu("Mall") = Sa.Dictionary
		s_admin_big_menu("Mall")("name") = "商城管理"
		s_admin_big_menu("Mall")("url") = "/?m=Admin&c=Mall&a=cate"
		
		Set s_admin_big_menu("Info") = Sa.Dictionary
		s_admin_big_menu("Info")("name") = "系统设置"
		
		Set s_admin_big_menu("Data") = Sa.Dictionary
		s_admin_big_menu("Data")("name") = "数据管理"
		
		Set s_admin_big_menu("User") = Sa.Dictionary
		s_admin_big_menu("User")("name") = "权限管理"
	End Sub
	
	'''设置完整的二级菜单目录
	Private Sub setAdminSubMenu_()
		Set s_admin_sub_menu = Sa.Dictionary
		
		Set s_admin_sub_menu("Index") = Sa.Dictionary
		s_admin_sub_menu("Index")("修改密码") = "/?m=Admin&c=User&a=chgpwd"
		s_admin_sub_menu("Index")("缓存清理") = "/?m=Admin&c=Info&a=cache"
		s_admin_sub_menu("Index")("文章发布") = "/?m=Admin&c=News&a=add"
		
		Set s_admin_sub_menu("Member") = Sa.Dictionary
		s_admin_sub_menu("Member")("注册用户列表") =  "/?m=Admin&c=Member&a=index"
		s_admin_sub_menu("Member")("修改审核列表") =  "/?m=Admin&c=Member&a=change"
		s_admin_sub_menu("Member")("内部提名名单") =  "/?m=Admin&c=Member&a=inside"
		s_admin_sub_menu("Member")("下载客户名单") =  "/?m=Admin&c=Member&a=down"
		s_admin_sub_menu("Member")("操作日志") =  "/?m=Admin&c=Member&a=log"
		
		Set s_admin_sub_menu("Store") = Sa.Dictionary
		s_admin_sub_menu("Store")("门店管理") =  "/?m=Admin&c=Store&a=index"
		s_admin_sub_menu("Store")("添加门店") =  "/?m=Admin&c=Store&a=add"
		s_admin_sub_menu("Store")("MSP记录") =  "/?m=Admin&c=Msp&a=index"
		s_admin_sub_menu("Store")("添加MSP") =  "/?m=Admin&c=Msp&a=add"
		
		Set s_admin_sub_menu("Zixun") = Sa.Dictionary
		s_admin_sub_menu("Zixun")("嘉宾管理") =  "/?m=Admin&c=Zixun&a=index"
		s_admin_sub_menu("Zixun")("案例管理") =  "/?m=Admin&c=Zixun&a=cases"
		
		Set s_admin_sub_menu("Interact") = Sa.Dictionary
		s_admin_sub_menu("Zixun")("在线交流") =  "/?m=Admin&c=Faq&a=indx"
		s_admin_sub_menu("Zixun")("抢楼竞猜") =  "/?m=Admin&c=Guessing&a=index"
		s_admin_sub_menu("Zixun")("企业成长史") =  "/?m=Admin&c=Guessing&a=history"
		s_admin_sub_menu("Zixun")("内部员工互动") =  "/?m=Admin&c=Employee&a=index"
		s_admin_sub_menu("Zixun")("投票") =  "/?m=Admin&c=Vote&a=index"
		
		Set s_admin_sub_menu("Weixin") = Sa.Dictionary
		s_admin_sub_menu("Weixin")("接口信息") =  "/?m=Admin&c=Weixin&a=index"
		s_admin_sub_menu("Weixin")("上传素材管理") =  "/?m=Admin&c=Weixin&a=media"
		s_admin_sub_menu("Weixin")("微信消息列表") =  "/?m=Admin&c=Weixin&a=wechat"
		s_admin_sub_menu("Weixin")("发送客服消息") =  "/?m=Admin&c=Weixin&a=cservice"
		s_admin_sub_menu("Weixin")("群发消息") =  "/?m=Admin&c=Weixin&a=mass"
		s_admin_sub_menu("Weixin")("重要服务通知") =  "/?m=Admin&c=Weixin&a=notice"
		s_admin_sub_menu("Weixin")("用户分组管理") =  "/?m=Admin&c=Weixin&a=group"
		s_admin_sub_menu("Weixin")("关注者列表") =  "/?m=Admin&c=Weixin&a=follower"
		s_admin_sub_menu("Weixin")("自定义菜单") =  "/?m=Admin&c=Weixin&a=menu"
		
		Set s_admin_sub_menu("News") = Sa.Dictionary
		s_admin_sub_menu("News")("文章列表") =  "/?m=Admin&c=News&a=index"
		s_admin_sub_menu("News")("分类管理") =  "/?m=Admin&c=News&a=cate"
		s_admin_sub_menu("News")("发布文章") =  "/?m=Admin&c=News&a=add"
		
		Set s_admin_sub_menu("Mall") = Sa.Dictionary
		s_admin_sub_menu("Mall")("产品列表") =  "/?m=Admin&c=Mall&a=index"
		s_admin_sub_menu("Mall")("产品分类") =  "/?m=Admin&c=Mall&a=cate"
		s_admin_sub_menu("Mall")("订单处理") =  "/?m=Admin&c=Mall&a=order"
		
		Set s_admin_sub_menu("Info") = Sa.Dictionary
		s_admin_sub_menu("Info")("站点配置") =  "/?m=Admin&c=Info&a=config"
		s_admin_sub_menu("Info")("邮箱配置") =  "/?m=Admin&c=Info&a=email"
		s_admin_sub_menu("Info")("安全配置") =  "/?m=Admin&c=Info&a=safe"
		
		Set s_admin_sub_menu("Data") = Sa.Dictionary
		s_admin_sub_menu("Data")("数据库信息") =  "/?m=Admin&c=Data&a=index"
		s_admin_sub_menu("Data")("数据库备份管理") =  "/?m=Admin&c=Data&a=view"
		s_admin_sub_menu("Data")("数据库压缩包") =  "/?m=Admin&c=Data&a=zip"
		s_admin_sub_menu("Data")("数据库优化修复") =  "/?m=Admin&c=Data&a=repair"

		Set s_admin_sub_menu("User") = Sa.Dictionary
		s_admin_sub_menu("User")("用户列表") =  "/?m=Admin&c=User&a=index"
		s_admin_sub_menu("User")("添加用户") =  "/?m=Admin&c=User&a=add"
		s_admin_sub_menu("User")("角色管理") =  "/?m=Admin&c=Role&a=index"
		s_admin_sub_menu("User")("添加角色") =  "/?m=Admin&c=Role&a=add"
		s_admin_sub_menu("User")("节点管理") =  "/?m=Admin&c=Node&a=index"
		s_admin_sub_menu("User")("添加节点") =  "/?m=Admin&c=Node&a=add"
	End Sub

	'''设置完整的后台菜单目录
	Private Sub setAdminAllMenu_()
		Set s_admin_all_menu = Sa.Dictionary

		Set s_admin_all_menu("User") = Sa.Dictionary
		s_admin_all_menu("User")("index") =  "用户列表"
		s_admin_all_menu("User")("add") =  "添加用户"
		s_admin_all_menu("User")("edt") =  "修改用户"
		s_admin_all_menu("User")("del") =  "删除用户"

		Set s_admin_all_menu("Data") = Sa.Dictionary
		s_admin_all_menu("Data")("index") =  "数据库信息"
		s_admin_all_menu("Data")("backup") =  "备份数据库"
		s_admin_all_menu("Data")("view") =  "数据库备份管理"
		s_admin_all_menu("Data")("delsql") =  "删除备份数据库"
		s_admin_all_menu("Data")("send") =  "发送数据库备份"
		s_admin_all_menu("Data")("restore") =  "还原数据库"
		s_admin_all_menu("Data")("zip") =  "压缩包管理"
		s_admin_all_menu("Data")("unzip") =  "解压压缩包"
		s_admin_all_menu("Data")("delzip") =  "删除备份数据库"
		s_admin_all_menu("Data")("down") =  "下载文件"
		s_admin_all_menu("Data")("repair") =  "优化修复"

		Set s_admin_all_menu("Index") = Sa.Dictionary
		s_admin_all_menu("Index")("mmenu") =  "主菜单"

		Set s_admin_all_menu("Info") = Sa.Dictionary
		s_admin_all_menu("Info")("index") =  "配置网站信息"
		s_admin_all_menu("Info")("email") =  "网站邮箱信息"
		s_admin_all_menu("Info")("etest") =  "测试邮箱设置"
		s_admin_all_menu("Info")("safe") =  "网站安全配置"

		Set s_admin_all_menu("Member") = Sa.Dictionary
		s_admin_all_menu("Member")("index") =  "注册会员列表"

		Set s_admin_all_menu("News") = Sa.Dictionary
		s_admin_all_menu("News")("cate") =  "分类管理"
		s_admin_all_menu("News")("index") =  "新闻列表"
		s_admin_all_menu("News")("add") =  "添加新闻"
		s_admin_all_menu("News")("tcheck") =  "检测标题"
		s_admin_all_menu("News")("edt") =  "编辑新闻"
		s_admin_all_menu("News")("del") =  "删除新闻"

		Set s_admin_all_menu("Node") = Sa.Dictionary
		s_admin_all_menu("Node")("index") =  "节点列表"
		s_admin_all_menu("Node")("opns") =  "修改节点状态"
		s_admin_all_menu("Node")("opst") =  "修改节点排序"
		s_admin_all_menu("Node")("add") =  "添加节点"
		s_admin_all_menu("Node")("edt") =  "修改节点"
		s_admin_all_menu("Node")("del") =  "删除节点"

		Set s_admin_all_menu("Role") = Sa.Dictionary
		s_admin_all_menu("Role")("index") =  "角色列表"
		s_admin_all_menu("Role")("oprs") =  "修改角色状态"
		s_admin_all_menu("Role")("add") =  "添加角色"
		s_admin_all_menu("Role")("edt") =  "修改角色"
		s_admin_all_menu("Role")("del") =  "删除角色"
		s_admin_all_menu("Role")("power") =  "权限分配"
	End Sub

	'''登录检测
	Private Sub checkLogin_()
		If Not Has( Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") ) ) Then
			If Sa.Request.IsAjax() Then
				Dim t_json : Set t_json = Sa.Dictionary
				t_json("errcode") = 105
				t_json("errmsg") = "您似乎还未登录或登录超时，请重新登录"
				t_json("url") = "/?m=Admin&c=Index&a=login"
				Sa.AjaxReturn( t_json )
			Else
				Call Sa.Error( "您似乎还未登录或登录超时，请重新登录", "/?m=Admin&c=Index&a=login", 300 )
			End If
		End If
	End Sub

	'''验证TOKEN信息
	Private Sub checkToken_()
		'''todo
	End Sub

	'''RBAC权限检测
	Private Sub checkRBAC_()
		'''todo
	End Sub

	'''当前一级菜单,返回数组
	Private Function big_menu_()
		Dim t_full_big_menu : Set t_full_big_menu = s_admin_big_menu
		Dim t_mu
		If Has( Sa.Request.Get("mu") ) Then
			t_mu = Sa.Request.Get("mu")
		Else
			t_mu = sa.Constant("CONTROLLER_NAME")
		End If
		'''todo 由于RBAC暂时未启用，先返回全部菜单
		Dim t_count : t_count = s_admin_big_menu.Count
		Dim t_i : t_i = 1
		Dim t_menu, t_key, t_css, t_url
		Set t_menu = t_full_big_menu
		Dim t_menus, t_dict
		ReDim t_menus( t_count - 1)
		For Each t_key In t_menu
			If t_i = 1 Then
				If t_key = t_mu Or Not Has( t_menu( t_mu ) ) Then
					t_css = "first_current"
				Else
					t_css = "first"
				End If
			ElseIf t_i = t_count Then
				If t_key = t_mu Then
					t_css = "end_current"
				Else
					t_css = "end"
				End If
			Else
				If t_key = t_mu Then
					t_css = "current"
				Else
					t_css = ""
				End If
			End If
			If t_menu( t_key ).Exists("url") Then
				t_url = t_menu( t_key )("url")  & "&mu=" & t_key
			Else
				t_url = "/?m=Admin&c=" & t_key & "&a=index&mu=" & t_key
			End If
			Set t_dict = Sa.Dictionary
			t_dict("name") = t_menu( t_key )("name")
			t_dict("url") = t_url
			t_dict("css") = t_css
			Set t_menus( t_i - 1 ) = t_dict
			t_i = t_i + 1
		Next
		big_menu_ = t_menus
	End Function

	'''当前二级菜单,返回数组
	Private Function sub_menu_()
		Dim t_menu : Set t_menu = s_admin_sub_menu
		Dim t_mu
		If Has( Sa.Request.Get("mu") ) Then
			t_mu = Sa.Request.Get("mu")
		Else
			t_mu = sa.Constant("CONTROLLER_NAME")
		End If
		Dim t_menus, t_dict, t_name, t_i : t_i = 0
		If Has( t_menu(t_mu) ) Then
			Dim t_sub_dict : Set t_sub_dict = t_menu(t_mu)
			ReDim t_menus( t_sub_dict.Count - 1)
			For Each t_name In t_sub_dict
				Set t_dict = Sa.Dictionary
				t_dict("url") = t_sub_dict(t_name)
				t_dict("name") = t_name
				Set t_menus( t_i ) = t_dict
				Set t_dict = Nothing
				t_i = t_i + 1
			Next
		Else
			t_menus = Empty
		End If
		sub_menu_ = t_menus
	End Function

	'''当前位置
	Private Function current_nav_()
		Dim t_big_menu : Set t_big_menu = s_admin_big_menu
		Dim t_mu
		If Has( Sa.Request.Get("mu") ) Then
			t_mu = Sa.Request.Get("mu")
		Else
			t_mu = sa.Constant("CONTROLLER_NAME")
		End If
		Dim t_url, t_name
		If Has( t_big_menu(t_mu) ) Then
			t_name = t_big_menu(t_mu)("name")
			t_url = "<a href='/?m=Admin&c=" & t_mu & "&a=index'>" & t_name & "</a>"
		Else
			t_url = ""
		End If
		Dim t_all_menu : Set t_all_menu = s_admin_all_menu
		If t_all_menu.Exists(Sa.Constant("CONTROLLER_NAME")) Then
			If t_all_menu(Sa.Constant("CONTROLLER_NAME")).Exists(Sa.Constant("ACTION_NAME")) Then
				t_url = t_url & " &gt; <a href='" & Sa.Constant("__SELF__") & "'>" & t_all_menu(Sa.Constant("CONTROLLER_NAME"))(Sa.Constant("ACTION_NAME")) & "</a>"
			End If
		End If
		current_nav_ = t_url
	End Function

	'''获取当前管理员cookie
	'p_key:指定要返回的键名
	Public Function adminCookie(Byval p_key)
		Dim t_cookie : Set t_cookie = Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") )
		If t_cookie.Exists(p_key) Then
			adminCookie = t_cookie(p_key)
		Else
			adminCookie = ""
		End If
	End Function
End Class
%>