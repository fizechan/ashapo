<%
Class Index

	Private s_home

	'''构造
	Private Sub Class_Initialize()
		Sa.Use(ASHAPO_APP_PATH & "/common/controller/Home")
		Set s_home = New Home
		Response.Write("Index 已开始<br/>")
		Response.Write("cfz1 是在HomeController.class.asp中定义并初始赋值的<br/>")
		Response.Write(cfz1 & "<br/>")
		cfz1 = "嘿嘿嘿，我是梁燕萍"
		Response.Write("cfz1 可以在IndexController中重新赋值的<br/>")
		Response.Write(cfz1 & "<br/>")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Response.Write("Index 已结束<br/>")
		Set s_home = Nothing
	End Sub
	
	'''测试
	Public Sub index()
		Response.Write("Hello Ashapo!")
		Response.Write("<br/>")
	End Sub

	'''测试
	Public Sub test1()
		Response.Write("IndexController index IS OK")
		Sa.Include("/Ashapo/Org/AshapoClassBase64.class.asp")
		
		Dim t_base64
		Set t_base64 = New AshapoClassBase64
		Dim t_string
		t_string = t_base64.Encode("123456")
		Response.Write(t_string & "<br/>")
		Set t_base64 = Nothing
	End Sub
	
	'''测试
	Public Sub test2()
		Response.Write("IndexController test IS OK")
		Sa.Dim("tttcfz")
		tttcfz = "CFZCFZCFZ"
		Response.Write(tttcfz & "<br/>")
		'Sa.Dim("2tttcfz")
		
		Sa.Dim("arr(3)")
		arr(0) = "0"
		arr(1) = "1"
		arr(2) = "2"
		
		arr(3) = "3" ''没越界
		''arr(4) = "4" ''越界
		
		Response.Write(arr(1) & "<br/>")
		
		Response.Write(arr(3) & "<br/>")
		
		Response.Write(Sa.Config("VAR_CONTROLLER") & "<br/>")
		
	End Sub
End Class
%>