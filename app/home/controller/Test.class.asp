<%
Class Test
	
	'''测试
	Public Sub test1()
		Sa.Dim("head_str1")
		head_str1 = "这是在头部的变量1"
		
		Sa.Dim("head_str2")
		head_str2 = "这是在头部的变量2"
	
		Sa.Dim("dim_str1")
		dim_str1 = "这是我在控制器里定义的变量1，它将在视图中输出"
		
		Sa.Dim("dim_str2")
		dim_str2 = "这是我在控制器里定义的变量2，它将在视图中输出"
		
		Sa.View.Display()
	End Sub
	
	'''测试Theme方法指定主题
	Public Sub test2()
		Sa.Dim("head_str1")
		head_str1 = "这是在头部的变量1"
		
		Sa.Dim("head_str2")
		head_str2 = "这是在头部的变量2"
	
		Sa.Dim("dim_str1")
		dim_str1 = "这是我在控制器里定义的变量1，它将在视图中输出"
		
		Sa.Dim("dim_str2")
		dim_str2 = "这是我在控制器里定义的变量2，它将在视图中输出"
		
		Sa.View.Theme("default").Display()
	End Sub
	
	'''测试View方法指定视图
	Public Sub test3()
		Sa.Dim("head_str1")
		head_str1 = "这是在头部的变量1"
		
		Sa.Dim("head_str2")
		head_str2 = "这是在头部的变量2"
	
		Sa.Dim("dim_str1")
		dim_str1 = "这是我在控制器里定义的变量1，它将在视图中输出"
		
		Sa.Dim("dim_str2")
		dim_str2 = "这是我在控制器里定义的变量2，它将在视图中输出"
		
		sa.View.Theme("default")
		
		if Sa.Request.Get("other") = "1" then
			sa.View.Path("test3_other")
		end if
		sa.view.display()
	End Sub
	
	Public Sub tjson()
		
		Response.ContentType = "application/json"
	
		Sa.Use(ASHAPO_CORE_PATH & "/org/AshapoClassJson")
		Dim t_json
		Set t_json = New AshapoClassJson
		
		Call t_json.SetOption("UnescapedUnicode", True)
		
		Dim t_arr1(4)
		t_arr1(0) = "陈峰展"
		t_arr1(1) = "梁燕萍"
		t_arr1(2) = "陈金展"
		t_arr1(3) = "ABcd"
		t_arr1(4) = "饕餮"
		Dim t_str1
		''t_str1 = t_json.Encode(t_arr1)
		''Response.Write(t_str1)
		
		
		Dim arr(2)
		Set arr(0) = Server.CreateObject("Scripting.Dictionary")
		Call arr(0).add("name", "陈峰展~")
		Call arr(0).add("age", 26)
		Call arr(0).add("sex", "男")
		Call arr(0).add("ok", True)
		Set arr(1) = Server.CreateObject("Scripting.Dictionary")
		Call arr(1).add("name", "梁燕萍")
		Call arr(1).add("age", 27)
		Call arr(1).add("sex", "女")
		Call arr(1).add("ok", True)
		Set arr(2) = Server.CreateObject("Scripting.Dictionary")
		Call arr(2).add("name", "吴亦凡""")
		Call arr(2).add("age", 24)
		Call arr(2).add("sex", "男")
		Call arr(2).add("ok", False)
		Dim t_out1
		t_out1 = t_json.Encode(arr)
		Response.Write(t_out1)
		
	End Sub
	
	Public Sub tjson2()
		Dim arr(2)
		Set arr(0) = Server.CreateObject("Scripting.Dictionary")
		Call arr(0).add("name", "陈峰展~")
		Call arr(0).add("age", 26)
		Call arr(0).add("sex", "男")
		Call arr(0).add("ok", True)
		Set arr(1) = Server.CreateObject("Scripting.Dictionary")
		Call arr(1).add("name", "梁燕萍")
		Call arr(1).add("age", 27)
		Call arr(1).add("sex", "女")
		Call arr(1).add("ok", True)
		Set arr(2) = Server.CreateObject("Scripting.Dictionary")
		Call arr(2).add("name", "吴亦凡""")
		Call arr(2).add("age", 24)
		Call arr(2).add("sex", "男")
		Call arr(2).add("ok", False)
		Sa.Json.UnescapedUnicode = True ''对内部的json单例进行设置
		Sa.AjaxReturn(arr)
	End Sub

	'''测试JSON解析
	Public Sub t_json_decode()
		Dim t_fso : Set t_fso = New AshapoFso
		Dim t_json : t_json = t_fso.ReadFile(ASHAPO_RUNTIME_PATH & "/Cache/Data/__NEWS_CATE_LIST__")
		Dim t_obj1 : Set t_obj1 = sa.Json.Decode(t_json)
		Dim t_arr1 : t_arr1 = t_obj1("value")
		''Response.Write(t_obj1("value")(0))
		Response.Write(t_obj1("value")(0)("name"))
		Response.Write("<br/>OK<br/>")
	End Sub
	
	Public Sub t_db()
		Sa.Use(ASHAPO_CORE_PATH & "/core/AshapoDatabase")
		dim t_db : Set t_db = New AshapoDatabase
		dim t_d  : t_d = Server.MapPath("/data/##db##/mu_ashapo.mdb")
		t_db.Init("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & t_d & ";Jet OLEDB:Database Password=" & "" & ";")
		
		Dim t_rs : Set t_rs = t_db.Query("SELECT * FROM [mu_core_config]")
		While Not t_rs.Eof
			 Response.Write(t_rs("id") & "<br/>")
			 Response.Write(t_rs("key") & "<br/>")
			 Response.Write(t_rs("value") & "<br/>")
			 t_rs.MoveNext()
		Wend
	End Sub
	
	'''测试表模型1
	Public Sub t_model1()
		Dim t_city
		Set t_city = Sa.Model("core_config")
		
		Dim t_arr, t_key
		t_arr = t_city.Select()
		For Each t_row In t_arr
			For Each t_key In t_row
				Response.Write(t_key & "：" & t_row(t_key) & "<br />")
			Next
		Next
	End Sub
	
	'''测试表模型2
	Public Sub t_model2()
		Dim t_city
		Set t_city = Sa.Model("City")
		t_city.test()
		Set t_city = Nothing
	End Sub
	
	'''测试表模型3
	Public Sub t_model3()
		Dim t_city
		Set t_city = Sa.Model("CommonModelCity")
		t_city.test()
		Set t_city = Nothing
	End Sub
	
	'''测试MSSQL表模型1
	Public Sub t_model21()
		Dim t_city
		Set t_city = Sa.Model("[FY15Q2_ISR]")
		
		Dim t_arr, t_key
		t_arr = t_city.Select()
		For Each t_row In t_arr
			For Each t_key In t_row
				Response.Write(t_key & "：" & t_row(t_key) & "<br />")
			Next
		Next
	End Sub
	
	'''测试自静态化
	Public Sub t_html()
		Sa.HTML = True
		Dim t_city
		Set t_city = Sa.Model("core_config")
		
		Dim t_arr, t_key
		t_arr = t_city.Select()
		For Each t_row In t_arr
			For Each t_key In t_row
				Response.Write(t_key & "：" & t_row(t_key) & "<br />")
			Next
		Next
	End Sub

	Public Sub t_use_as()
		Call Global.Asp.UseAs("/test/kkk/Test", "TestKKK")
		Dim t_test_kkk
		Set t_test_kkk = New TestKKK
		Call t_test_kkk.test01()
		Set t_test_kkk = Nothing
	End Sub
	
	'''测试CACHE
	Public Sub t_cache()
		'Die("111")
		'sa.Cache.Expire = 200
		
		''Die(sa.Cache.Expire)
		
		'Sa.Cache.Value("cfz") = "陈峰展"
		'Response.Write( Sa.Cache.Value("cfz") & "^^^" )
		
		'Dim t_cache
		'Set t_cache = Sa.Cache
		't_cache("lyp") = "梁燕萍"
		
		'Dim t_arr(2)
		't_arr(0) = "1"
		't_arr(1) = "2"
		't_arr(2) = "3"
		't_cache("test") = t_arr
		
		Dim t_arr2
		
		t_arr2 = Sa.Cache.Value("test")
		
		'Die("OK")
		
		Response.Write(TypeName( t_arr2 ) & "111<br/>")
		
		'Die("OK")
		
		If IsArray(t_arr2) Then
			For i=0 To Ubound(t_arr2)
				Response.Write(t_arr2(i) & "<br />")
			Next
		End If
		
		'Die(t_arr2)
		
		'Response.Write(TypeName(t_arr2) & "<br />")
		
		'For i=0 To Ubound(t_arr2)
		'  Response.Write(t_arr2(i) & "<br />")
		'Next
		
		
		'Response.Write( t_cache("lyp") )
		
		Dim t_item : Set t_item = Server.CreateObject( "Scripting.Dictionary" )
		t_item("cfz") = "陈峰展"
		t_item("lyp") = "梁燕萍"
		t_item("tt") = "饕餮"
		
		Response.Write("OK")
		
		
		
		Sa.Cache.Value("cfz") = t_item
		
		'Die("OK")
		
		Dim t_item2
		
		Call assign(t_item2, Sa.Cache.Value("cfz"))
		
		'Response.Write(TypeName( t_item2 ) & "111<br/>")
		
		Response.Write(t_item2("tt"))
		
		'Die("OK")
		
	End Sub

	Public Sub tcookie()
		'Sa.Cookie("cfz") = "陈峰展"
		Response.Write( Sa.Cookie("cfz") & "<br/>" )
		Response.Write( "哈哈哈哈<br/>" )

		'Dim t_dict : Set t_dict = Sa.Dictionary
		't_dict("cfz") = "陈峰展"
		't_dict("lyp") = "梁燕萍"
		'Set Sa.Cookie("fize") = t_dict
		If Has(Sa.Cookie("fize")) Then
			Response.Write( Sa.Cookie("fize")("cfz") & "<br/>" )
			Response.Write( Sa.Cookie("fize")("lyp") & "<br/>" )
		End If
		'Response.Write( "哈哈哈哈<br/>" )

		Dim t_kkk : t_kkk = Sa.Config("COOKIE_ADMIN_KEY")
		t_kkk = "__shopkeeper__20160702__admin__"
		Response.Write(Sa.Config("COOKIE_ADMIN_KEY") & "<br/>")
		Response.Write(t_kkk & "<br/>")

		'Dim t_ckv : Set t_ckv = Sa.Dictionary
		't_ckv("user_id") = "1"
		't_ckv("boss_id") = "2"
		'Set Sa.Cookie( t_kkk ) = t_ckv

		If Has( Sa.Cookie( t_kkk ) ) Then
			Response.Write( Sa.Cookie( t_kkk )("user_id") & "<br/>" )
		Else
			Response.Write( "没有发现<br/>" )
		End If

		
		Response.Write( "哈哈哈哈<br/>" )

		Response.Write(Now())
	End Sub

	Public Sub tmd5()
		Response.Write(encrypt("123456"))
	End Sub

	Public Sub trequest()
		If Sa.Request.IS_POST Then
			Die("OK")
		Else
			Die("NO")
		End If
	End Sub

	'''测试跳转时引发的错误
	Public Sub tlocation()
		If Has(Sa.Request.Get("ok")) Then
			Response.Write("查看跳转过来是否发生错误")
		Else
			Sa.Display()
		End If
	End Sub

	'''测试Response.AppendToLog写入日志
	Public Sub tserverlog()
		Sa.Include(ASHAPO_CORE_PATH & "/Library/Driver/Log/AshapoLogDriverServer.class.asp")
		Dim t_log : Set t_log = New AshapoLogDriverServer
		t_log.Write("哈哈哈哈哈")
		t_log.Write("测试写入逗号,，")  ''文档中提示说不能写入任何逗号字符,但经测试，没有问题
		Response.Write("OK")
	End Sub
End Class
%>