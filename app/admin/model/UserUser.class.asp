<%
'''用户模型
Class UserUser

	Private s_model

	'''构造
	Private Sub Class_Initialize()
		Sa.Use(ASHAPO_ROOT_PATH & "/extend/Md5")
		
		Set s_model = Sa.Model("user_user")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_model = Nothing
	End Sub
	
	'''检测用户登录
	''' @param string p_email 邮箱名
	''' @param string p_pwd 未加密时密码
	Public Function checkLogin(Byval p_email, Byval p_pwd)
		Dim t_json : Set t_json = Sa.Dictionary
		If s_model.Where("[email]='" & p_email & "'").Count() >= 1 Then
			Dim t_info : Set t_info = s_model.Where("[email]='" & p_email & "'").Find()
			If Cstr(t_info("status")) = "0" Then
				t_json("errcode") = 106
				t_json("errmsg") = "您的账号被禁用，请联系管理员"
			Else
				If t_info("pwd") = encrypt(p_pwd) Then
					Dim t_cookie : Set t_cookie = Sa.Dictionary
					t_cookie("user_id") = t_info("user_id")
					t_cookie("role_id") = t_info("role_id")
					t_cookie("boss_id") = t_info("boss_id")
					t_cookie("awclub_id") = t_info("awclub_id")
					Set Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") ) = t_cookie
					t_json("errcode") = 0
					t_json("errmsg") = "登录成功"
					t_json("url") = "/?m=Admin&c=Info&a=index&mu=Index&kkk=" & Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") )("user_id")
				Else
					t_json("errcode") = 107
					t_json("errmsg") = "账号或密码错误" & encrypt(p_pwd)
				End If
			End If
		Else
			t_json("errcode") = 108
			t_json("errmsg") = "不存在邮箱为：" & p_email & "的管理员账号！"
		End If
		Set checkLogin = t_json
	End Function

	'''发送找回密码EDM
	'p_email:待接收邮件的邮箱地址
	Public Function sendFindPwdEDM(Byval p_email)
		Dim t_json : Set t_json = Sa.Dictionary
		If s_model.Where("[email]='" & p_email & "'").Count() >= 1 Then
			Dim t_info : Set t_info = s_model.Where("[email]='" & p_email & "'").Find()
			If Cstr(t_info("status")) = "0" Then
				t_json("errcode") = 106
				t_json("errmsg") = "您的账号被禁用，请联系管理员"
			Else
				'密码重置
				If Not Sa.Config("EMAIL_CAN_SEND") Then
					t_json("errcode") = 110
					t_json("errmsg") = "系统暂时停用密码重置功能，请联系管理员"
				Else
					Dim t_rc : t_rc = rand_code(5, 0)
					Dim t_md5 : Set t_md5 = New Md5
					Dim t_code : t_code = t_info("user_id") & t_md5.To32(t_rc)
					Dim t_url : t_url = Sa.Config("DOMAIN_URL") & "/?m=Admin&c=Index&a=findpwd&code=" & t_code
					Dim t_edm_replace : Set t_edm_replace = Sa.Dictionary
					t_edm_replace("##URL##") = t_url
					Dim t_return : t_return = send_edm(p_email, t_json("realname"), "admin_find", "找回密码", t_edm_replace)
					If t_return Then
						Dim t_data : Set t_data = Sa.Dictionary
						t_data("find_code") = t_rc
						s_model.Where("[email]='" & p_email & "'").Save( t_data )
						t_json("errcode") = 0
						t_json("errmsg") = "重置密码邮件已经发往你的邮箱" & p_email & "中，请注意查收"
					Else
						t_json("errcode") = 110
						t_json("errmsg") = "系统错误，未设置EDM模版"
					End If
				End If
			End If
		Else
			t_json("errcode") = 108
			t_json("errmsg") = "不存在邮箱为：" & p_email & "的管理员账号！"
		End If
		Set checkLogin = t_json
	End Function
End Class
%>