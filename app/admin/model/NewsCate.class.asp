<%

'''新闻分类模型
Class NewsCate

	Private s_model

	'''getList缓存名
	Private CACHE_CATE_LIST

	'''构造
	Private Sub Class_Initialize()
		Sa.Use(ASHAPO_ROOT_PATH & "/extend/Category")
		CACHE_CATE_LIST = "__NEWS_CATE_LIST__"
		Set s_model = Sa.Model("news_cate")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_model = Nothing
	End Sub
	
	'''获取分类结构
	Public Function getList()
		Dim t_cache : Call Assign(t_cache, Sa.Cache(CACHE_CATE_LIST))
		If Has(t_cache) Then
			getList = t_cache
		Else
			Dim t_cat : Set t_cat = New Category
			Dim t_map : Set t_map = Sa.Dictionary
			t_map("cid") = "id"
			t_map("fid") = "pid"
			Call t_cat.Init("news_cate", t_map)
			Dim t_list : t_list = t_cat.getList()
			Sa.Cache(CACHE_CATE_LIST) = t_list
			getList = t_list
		End If
	End Function

	'''分类操作
	'p_data:数据
	'p_action:动作
	Public Function saveOne(Byval p_data, Byval p_action)
		Dim t_out : Set t_out = Sa.Dictionary
		Select Case p_action
		Case "add"
			'''添加分类
			'''添加一下判断
			If Has(s_model.Where("ident='" & p_data("ident") & "'").Find()) Then
				t_out("errcode") = 110
				t_out("errmsg") = "添加失败，已存在该唯一标识,请更改"
				Set saveOne = t_out
				Exit Function
			End If
			If Has(s_model.Add(p_data)) Then
				t_out("errcode") = 0
				t_out("errmsg") = "分类 " & p_data("name") & " 已经成功添加到系统中"
				t_out("url") =  "/?m=Admin&c=News&a=cate&time=" & Now()
			Else
				t_out("errcode") = 110
				t_out("errmsg") = "分类 " & p_data("name") & " 添加失败"
			End If
		Case "edt"
			'''修改分类
			'''添加一下判断
			Dim t_id : t_id = p_data("id")
			p_data.Remove("id")
			If IsNumeric( s_model.Where("id=" & t_id).Update(p_data) ) Then
				t_out("errcode") = 0
				t_out("errmsg") = "分类 " & p_data("name") & " 已经成功更新"
				t_out("url") =  "/?m=Admin&c=News&a=cate&time=" & Now()
			Else
				t_out("errcode") = 110
				t_out("errmsg") = "分类 " & p_data("name") & " 更新失败"
			End If
		Case "del"
			'''删除分类
			'''添加一下判断
			'''事务
			Dim t_modelNewsNews : Set t_modelNewsNews = Sa.Model("news_news")
			s_model.DataBase.BeginTrans()
			'''删除新闻
			Dim t_del_num1 : t_del_num1 = t_modelNewsNews.Where("cid=" & p_data("id")).Delete()
			'''删除分类
			Dim t_del_num2 : t_del_num2 = s_model.Where("id=" & p_data("id")).Delete()
			If IsNumeric( t_del_num1 ) And IsNumeric( t_del_num2 ) Then
				s_model.DataBase.CommitTrans()
				t_out("errcode") = 0
				t_out("errmsg") = "分类 " & p_data("name") & " 已经成功删除"
				t_out("url") =  "/?m=Admin&c=News&a=cate&time=" & Now()
			Else
				s_model.DataBase.RollbackTrans()
				t_out("errcode") = 110
				t_out("errmsg") = "分类 " & p_data("name") & " 删除失败"
			End If
		End Select
		Sa.Cache.Remove(CACHE_CATE_LIST)
		Set saveOne = t_out
	End Function
End Class
%>