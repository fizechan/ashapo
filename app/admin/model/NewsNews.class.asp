<%
'''新闻分类模型
Class NewsNews

	Private s_model

	'''构造
	Private Sub Class_Initialize()
		Set s_model = Sa.Model("news_news")
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_model = Nothing
	End Sub

	'''取得新闻分页
	'p_where:搜索条件
	'p_page:指定页码
	'p_size:指定每页数量
	'p_aid:指定对应用户ID
	Public Function getPage(Byval p_where, Byval p_page, Byval p_size, Byval p_aid)
		Dim t_alias : t_alias = "t_n"
		''获取角色信息
		Dim t_tab_trl : t_tab_trl = "SELECT t_ta.user_id, t_tr.ident AS role_ident, t_tr.name AS role_name FROM [mu_user_user] AS t_ta LEFT JOIN [mu_user_role] AS t_tr ON t_tr.id = t_ta.role_id"
		''获取评论信息
		Dim t_tab_tnc : t_tab_tnc = "SELECT nid, COUNT(id) AS cnum FROM [mu_news_comment] GROUP BY nid"
		''获取加赞信息
		Dim t_tab_tnp : t_tab_tnp = "SELECT nid, COUNT(id) AS pnum FROM [mu_news_praise] WHERE cancel=0 GROUP BY nid"
		''当前用户评否
		Dim t_tab_myc : t_tab_myc = "SELECT nid, COUNT(id) AS cnum FROM [mu_news_comment] WHERE aid=" & p_aid & " GROUP BY nid"
		''当前用户赞否
		Dim t_tab_myp : t_tab_myp = "SELECT nid, MAX(id) AS pid FROM [mu_news_praise] WHERE cancel=0 AND uid=" & p_aid & "  GROUP BY nid"

		Dim t_joins(6)
		t_joins(0) = "LEFT JOIN [mu_user_user] AS t_u ON t_u.user_id = t_n.aid"  ''文章作者
		t_joins(1) = "LEFT JOIN ("& t_tab_trl & ") AS t_r ON t_r.user_id = t_n.aid"  ''作者身份
		t_joins(2) = "LEFT JOIN [mu_news_cate] AS t_c ON t_c.id = t_n.cid"  ''文章分类
		t_joins(3) = "LEFT JOIN (" & t_tab_tnc & ") AS t_nc ON t_nc.nid = t_n.id"  ''总评论数
		t_joins(4) = "LEFT JOIN (" & t_tab_tnp & ") AS t_np ON t_np.nid = t_n.id"  ''总加赞数
		t_joins(5) = "LEFT JOIN (" & t_tab_myc & ") AS m_nc ON m_nc.nid = t_n.id"  ''个人评论数
		t_joins(6) = "LEFT JOIN (" & t_tab_myp & ") AS m_np ON m_np.nid = t_n.id"  ''个人点赞记录

		Dim t_field : t_field = ""
		t_field = t_field & "t_u.realname AS u_realname, t_u.headurl AS u_headurl, t_u.total_score AS u_score, "  ''用户信息
		t_field = t_field & "t_r.role_ident AS u_role_ident, t_r.role_name AS u_role, "  ''用户角色
		t_field = t_field & "t_n.id, t_n.cid, t_n.title, t_n.introduce, t_n.status, t_n.re_score, t_n.reward, t_n.pic, t_n.add_time, t_n.edt_time, t_n.is_top, t_n.is_elite, t_n.sort, t_n.aid, "  ''文章字段
		t_field = t_field & "t_c.name AS cate_name, "  ''文章分类
		t_field = t_field & "IIF(ISNULL(t_nc.cnum), 0, t_nc.cnum) AS total_cnum, IIF(ISNULL(t_np.pnum), 0, t_np.pnum) AS total_pnum, "  ''文章总评论和总加赞
		t_field = t_field & "IIF(ISNULL(m_nc.cnum), 0, m_nc.cnum) AS my_cnum, IIF(ISNULL(m_np.pid), 0, m_np.pid) AS my_pid"  ''当前用户对文章的评论和加赞

		Dim t_order : t_order = "t_n.is_top DESC, t_n.sort ASC, t_n.add_time DESC"
		Dim t_out : Set t_out = Sa.Dictionary
		t_out("list") = s_model.Alias(t_alias).Join(t_joins).Order(t_order).Field(t_field).Where(p_where).Size(p_size).Page(p_page).Select()
		t_out("rows") = s_model.Count()
		Set getPage = t_out
	End Function
End Class
%>