<%
'''新闻管理
Class News

	Private s_BaseAdmin

	'''构造
	Private Sub Class_Initialize()
		Sa.Use( ASHAPO_APP_PATH & "/common/controller/Admin" )
		Sa.Use( ASHAPO_APP_PATH & "/admin/model/NewsCate" )
		Sa.Use( ASHAPO_APP_PATH & "/admin/model/NewsNews" )
		
		Set s_BaseAdmin = New Admin
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_BaseAdmin = Nothing
	End Sub

	'''分类管理
	Public Sub cate()
		Dim t_news_cate : Set t_news_cate = New NewsCate
		If Sa.Request.IsPost() Then
			Dim t_act : t_act = Sa.Request.Post("act")
			Dim t_data
			Select Case t_act
			Case "add"
				Set t_data = Sa.Request.Post(Array("pid", "ident", "name", "sort"))
			Case "edt"
				Set t_data = Sa.Request.Post(Array("id", "pid", "ident", "name", "sort"))
			Case "del"
				Set t_data = Sa.Request.Post(Array("id", "name"))
			Case Else
				Dim t_dict : Set t_dict = Sa.Dictionary
				t_dict("errcode") = 110
				t_dict("errmsg") = "非法操作类型" & t_act
				Sa.AjaxReturn(t_dict)
			End Select
			Sa.AjaxReturn( t_news_cate.saveOne(t_data, t_act) )
		Else
			Sa.Dim("cates")
			cates = t_news_cate.getList()
			Sa.View.Display()
		End If
	End Sub

	'''新闻列表
	Public Sub index()
		Dim t_target : t_target = "/?m=Admin&c=News&a=index&p={p}"

		Dim t_where : t_where = ""
		Dim t_kwd : t_kwd = Sa.Request("kwd")
		If Has(t_kwd) Then
			t_target = t_target & "&kwd=" & t_kwd
			t_where = "t_n.title LIKE '%" & t_kwd & "%' OR t_n.keywords LIKE '%" & t_kwd & "%' OR t_n.description LIKE '%" & t_kwd & "%' OR t_n.content LIKE '%" & t_kwd & "%'"
		End If
		Dim t_news_news, t_data
		Set t_news_news = New NewsNews
		Set t_data = t_news_news.getPage(t_where, s_BaseAdmin.BaseCommon.PageNow, Sa.Config("PAGE_SIZE"), s_BaseAdmin.adminCookie("user_id"))

		Sa.Dim("v_newss") : v_newss = t_data("list")
		Sa.Dim("v_pager") : v_pager = s_BaseAdmin.BaseCommon.pager( t_target, t_data("rows"))
		Sa.Dim("v_kwd") : v_kwd = t_kwd
		Sa.View.Display()
	End Sub

	'''添加新闻
	Public Sub [add]()
		If Sa.Request.IsPost() Then
			Dim t_data
			Set t_data = Sa.Request.Post(Array("pid", "ident", "name", "sort"))
			t_data("aid") = Cint(s_BaseAdmin.adminCookie("user_id"))
			Sa.AjaxReturn( (New NewsNews).saveAdd(t_data) )
		Else
			Sa.Dim("cates")
			cates = (New NewsCate).getList()
			Sa.View.Display()
		End If
	End Sub
End Class
%>