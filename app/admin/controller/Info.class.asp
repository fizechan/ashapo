<%
'''后台基础页面
Class Info

	Private s_BaseAdmin

	'''构造
	Private Sub Class_Initialize()
		Sa.Use( ASHAPO_APP_PATH & "/common/controller/Admin" )
		Sa.Use(ASHAPO_APP_PATH & "/admin/model/CoreInfo")
		
		Set s_BaseAdmin = New Admin
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_BaseAdmin = Nothing
	End Sub

	'''站点信息
	Public Sub index()
		Sa.Dim("server_info")
		Dim t_core_info : Set t_core_info = New CoreInfo
		Set server_info = t_core_info.getSystemInfo()
		Sa.View.Display()
		Set server_info = Nothing
	End Sub
End Class
%>