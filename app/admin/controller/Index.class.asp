<%
'''后台基础页面
Class Index

	Private s_BaseCommon

	'''构造
	Private Sub Class_Initialize()
		Sa.Use(ASHAPO_APP_PATH & "/common/controller/Common")
		Sa.Use(ASHAPO_ROOT_PATH & "/extend/Captcha")
		Sa.Use(ASHAPO_APP_PATH & "/admin/model/UserUser")
		Sa.Use(ASHAPO_APP_PATH & "/common/model/CoreConfig")
		Set s_BaseCommon = New Common
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_BaseCommon = Nothing
	End Sub

	'''首页
	Public Sub index()
		Dim t_cookie
		'''Sa.Cookie可能返回Object，所以使用Assign方法接收
		Call Assign( t_cookie, Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") ) )
		If Has( t_cookie ) Then
			Response.Redirect("/?m=Admin&c=Info&a=index&mu=Index")
		Else
			Response.Redirect("/?m=Admin&c=Index&a=login")
		End If
	End Sub
	
	'''登录
	Public Sub login()
		If Sa.Request.IsPost() Then
			Dim t_json : Set t_json = Sa.Dictionary
			Dim t_captcha : Set t_captcha = New Captcha
			Dim t_user_user : Set t_user_user = New UserUser
			If Ucase(Session( t_captcha.SessionName )) <> Ucase(Sa.Request.Post("verify_code")) Then
				t_json("errcode") = 110
				t_json("errmsg") = "验证码错误啦，再输入吧"
			Else
				If Has( Sa.Request.Post("email") ) Then
					If Cstr( Sa.Request.Post("op_type") ) = "1" Then
						'''登录检测
						Set t_json = t_user_user.checkLogin( Sa.Request.Post("email"), Sa.Request.PostWithDefault("pwd", "") )
					Else
						'''找回密码
						Set t_json = t_user_user.sendFindPwdEDM( Sa.Request.Post("email") )
					End If
				Else
					t_json("errcode") = 110
					t_json("errmsg") = "邮箱地址必须输入"
				End If
			End If
			Sa.AjaxReturn( t_json )
		Else
			Sa.Dim("site")
			Set site = Sa.Dictionary
			Dim t_config : Set t_config = New CoreConfig
			site("BACK_NAME") = t_config.getOne("BACK_NAME")
			site("WEB_ICP") = t_config.getOne("WEB_ICP")
			Sa.View.Display()
			Set site = Nothing
		End If
	End Sub
	
	'''登录验证码
	Public Sub vcode()
		Dim t_captcha
		Set t_captcha = New Captcha
		t_captcha.CharWidth = 28
		t_captcha.CharHeight = 34
		t_captcha.SelectTextType("all")
		t_captcha.Write()
		Set t_captcha = Nothing
	End Sub

	'''重置密码
	Public Sub findpwd()
		If Sa.Request.IsPost() Then
			''do some thing
		Else
			''show view
		End If
	End Sub

	'''退出登录
	Public Sub logout()
		Sa.Cookie.Remove(Sa.Config("COOKIE_ADMIN_KEY"))
		Response.Redirect("/?m=Admin&c=Index&a=login")
	End Sub
	
	'''测试
	Public Sub test()
		Dim t_config
		Set t_config = Sa.Model("core_config")
		Dim t_configs
		t_configs = t_config.Select()
		For Each t_row In t_configs
			For Each t_key In t_row
				Response.Write(t_key & "：" & t_row(t_key) & "<br />")
			Next
		Next

		Dim s_model : Set s_model = Sa.Model("user_user")
		Dim t_info : Set t_info = s_model.Where("[email]='admin@deya.cn'").Find()

		Dim t_key_y
		For Each t_key_y In t_info
			Response.Write(t_key_y & "：" & t_info(t_key_y) & "<br />")
			''Response.Write(VarType(t_key_y) & "<br/>")
			Response.Write(VarType(t_info(t_key_y)) & "<br/>")
		Next

		'Set Sa.Cookie( Sa.Config("COOKIE_ADMIN_KEY") ) = t_info

		Dim t_gets : Set t_gets = Sa.Request.Gets
		For Each t_key In t_gets
			Response.Write(t_key & "：" & t_gets(t_key) & "<br />")
			''Response.Write(VarType(t_key_y) & "<br/>")
			Response.Write(VarType(t_gets(t_key)) & "<br/>")
		Next
	End Sub
End Class
%>