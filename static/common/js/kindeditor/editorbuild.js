function EDbuildById(dom_id){
    var editor;
    t_upjson = 'index.php?g=back&m=keditor&a=upload';
    t_mgjson = 'index.php?g=back&m=keditor&a=manager';
    KindEditor.ready(function(K) {
		editor=K.create('#'+dom_id, {
			cssPath : '/static/common/js/kindeditor/plugins/code/prettify.css',
			uploadJson : t_upjson,
			allowFileManager : true,
			urlType:'relative',
			fileManagerJson : t_mgjson,
			designMode:true
		});
    });
	return editor;
}
function EDbuildById2(dom_id){
	var editor;
	t_upjson = 'index.php?g=back&m=keditor&a=upload';
	t_mgjson = 'index.php?g=back&m=keditor&a=manager';
	KindEditor.ready(function(K) {
		editor=K.create('#'+dom_id, {
			cssPath : '/static/common/js/kindeditor/plugins/code/prettify.css',
            uploadJson : t_upjson,
            allowFileManager : true,
			urlType:'relative',
            fileManagerJson : t_mgjson,
            items:[
				'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
				'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
				'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
				'superscript', 'clearhtml', 'quickformat', 'selectall', '|', '/',
				'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
				'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
				'flash', 'emoticons', 'media', 'insertfile', 'table', 'hr', 'baidumap', 'pagebreak',
				'anchor', 'link', 'unlink', 'fullscreen'
            ]
		});
    });
	return editor;
}
function EDbuildByIdMini(dom_id){
    var editor;
    t_upjson = 'index.php?g=back&m=keditor&a=upload';
    t_mgjson = 'index.php?g=back&m=keditor&a=manager';
	editor=KindEditor.create('#'+dom_id, {
		minWidth : 480,
		cssPath : '/static/admin/css/editor.css',
		uploadJson : t_upjson,
		allowFileManager : true,
		urlType:'relative',
		fileManagerJson : t_mgjson,
		items:[
			'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'
		]
	});
	return editor;
}
function EDbuildByIdMiniWithFun(dom_id, fun_na){
    var editor;
    t_upjson = 'index.php?g=back&m=keditor&a=upload';
    t_mgjson = 'index.php?g=back&m=keditor&a=manager';
	editor=KindEditor.create('#'+dom_id, {
		minWidth : 480,
		cssPath : '/static/admin/css/editor.css',
		uploadJson : t_upjson,
		allowFileManager : true,
		urlType:'relative',
		fileManagerJson : t_mgjson,
		items:[
			'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'image', 'link'
		],
		afterChange : function() {
			fun_na(this.count());
			//KindEditor('#'+cot_id).val(this.count());
		}
	});
	return editor;
}
function EDbuildByIdMini2(dom_id){
    var editor;
    t_upjson = 'index.php?g=back&m=keditor&a=upload';
    t_mgjson = 'index.php?g=back&m=keditor&a=manager';
    KindEditor.ready(function(K) {
		editor=K.create('#'+dom_id, {
			minWidth : 480,
            cssPath : '/static/common/js/kindeditor/plugins/code/prettify.css',
            uploadJson : t_upjson,
            allowFileManager : true,
			urlType:'relative',
            fileManagerJson : t_mgjson,
            items:[
				'source', '|', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'image', 'link'
            ]
		});
    });
	return editor;
}