/**
 * 检测字符串是否是电子邮件地址格式
 * @param  {String} value 待检测字符串
 * @return {Boolean}
 */
function isEmail(value){
    var Reg =/^[0-9a-zA-Z]+(?:[\_\.\-][0-9a-zA-Z\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+$/;
    return Reg.test(value);
}

/**
 * 检测字符串是否是电子邮件地址数组格式
 * @param  {String} value 待检测字符串
 * @return {Boolean}
 */
function isEmailArray(value){
	var Reg =/^([0-9a-zA-Z]+(?:[\_\.\-][0-9a-zA-Z\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+){1}(?:,([0-9a-zA-Z]+(?:[\_\.\-][0-9a-zA-Z\-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\.[a-zA-Z]+))*$/;
    return Reg.test(value);
}

/**
 * 检测字符串是否只由数字字母和下划线组成
 * @param  {String} value 待检测字符串
 * @return {Boolean}
 */
function isSafeStr(value){
	var Reg =/^[A-Za-z0-9_]+$/;
    return Reg.test(value);
}

/**
 * 检测字符串是否是有效数字
 * @param  {String} value 待检测字符串
 * @return {Boolean}
 */
function isSafeInt(value){
	var Reg =/^[0-9]+$/;
    return Reg.test(value);
}

/**
 * 和PHP一样的时间戳格式化函数
 * @param  {string} format    格式
 * @param  {int}    timestamp 要格式化的时间 默认为当前时间
 * @return {string}           格式化的时间字符串
 */
function date(format, timestamp){
    var a, jsdate=((timestamp) ? new Date(timestamp*1000) : new Date());
    var pad = function(n, c){
        if((n = n + "").length < c){
            return new Array(++c - n.length).join("0") + n;
        } else {
            return n;
        }
    };
    var txt_weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var txt_ordin = {1:"st", 2:"nd", 3:"rd", 21:"st", 22:"nd", 23:"rd", 31:"st"};
    var txt_months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var f = {
        // Day
        d: function(){return pad(f.j(), 2)},
        D: function(){return f.l().substr(0,3)},
        j: function(){return jsdate.getDate()},
        l: function(){return txt_weekdays[f.w()]},
        N: function(){return f.w() + 1},
        S: function(){return txt_ordin[f.j()] ? txt_ordin[f.j()] : 'th'},
        w: function(){return jsdate.getDay()},
        z: function(){return (jsdate - new Date(jsdate.getFullYear() + "/1/1")) / 864e5 >> 0},
        // Week
        W: function(){
            var a = f.z(), b = 364 + f.L() - a;
            var nd2, nd = (new Date(jsdate.getFullYear() + "/1/1").getDay() || 7) - 1;
            if(b <= 2 && ((jsdate.getDay() || 7) - 1) <= 2 - b){
                return 1;
            } else{
                if(a <= 2 && nd >= 4 && a >= (6 - nd)){
                    nd2 = new Date(jsdate.getFullYear() - 1 + "/12/31");
                    return date("W", Math.round(nd2.getTime()/1000));
                } else{
                    return (1 + (nd <= 3 ? ((a + nd) / 7) : (a - (7 - nd)) / 7) >> 0);
                }
            }
        },
        // Month
        F: function(){return txt_months[f.n()]},
        m: function(){return pad(f.n(), 2)},
        M: function(){return f.F().substr(0,3)},
        n: function(){return jsdate.getMonth() + 1},
        t: function(){
            var n = jsdate.getMonth() + 1;
            if( n == 2 ){
                return 28 + f.L();
            } else{
                if( n & 1 && n < 8 || !(n & 1) && n > 7 ){
                    return 31;
                } else{
                    return 30;
                }
            }
        },
        // Year
        L: function(){
            var y = f.Y();
            return (!(y & 3) && (y % 1e2 || !(y % 4e2))) ? 1 : 0
        },
        Y: function(){return jsdate.getFullYear()},
        y: function(){return (jsdate.getFullYear() + "").slice(2)},
        // Time
        a: function(){return jsdate.getHours() > 11 ? "pm" : "am"},
        A: function(){return f.a().toUpperCase()},
        B: function(){
            var off = (jsdate.getTimezoneOffset() + 60)*60;
            var theSeconds = (jsdate.getHours() * 3600) + (jsdate.getMinutes() * 60) + jsdate.getSeconds() + off;
            var beat = Math.floor(theSeconds/86.4);
            if (beat > 1000) beat -= 1000;
            if (beat < 0) beat += 1000;
            if ((String(beat)).length == 1) beat = "00"+beat;
            if ((String(beat)).length == 2) beat = "0"+beat;
            return beat;
        },
        g: function(){return jsdate.getHours() % 12 || 12},
        G: function(){return jsdate.getHours()},
        h: function(){return pad(f.g(), 2)},
        H: function(){return pad(jsdate.getHours(), 2)},
        i: function(){return pad(jsdate.getMinutes(), 2)},
        s: function(){return pad(jsdate.getSeconds(), 2)},
        // Timezone
        O: function(){
            var t = pad(Math.abs(jsdate.getTimezoneOffset()/60*100), 4);
            if (jsdate.getTimezoneOffset() > 0) t = "-" + t; else t = "+" + t;
            return t;
        },
        P: function(){
            var O = f.O();
            return (O.substr(0, 3) + ":" + O.substr(3, 2))
        },
        // Full Date/Time
        c: function(){return f.Y() + "-" + f.m() + "-" + f.d() + "T" + f.h() + ":" + f.i() + ":" + f.s() + f.P()},
        U: function(){return Math.round(jsdate.getTime()/1000)}
    };
    return format.replace(/[\\]?([a-zA-Z])/g, function(t, s){
		var ret;
        if( t!=s ){
            // escaped
            ret = s;
        } else if( f[s] ){
            // a date function exists
            ret = f[s]();
        } else{
            // nothing special
            ret = s;
        }
        return ret;
    });
}

/*
 * 初始化窗口尺寸
 */
function autoSize(){
	//alert($(".commonBtnArea").length);
	var webBodyWidth = getBodySize("w");
	var webBodyHight = getBodySize("h");
	var h = (webBodyHight-150);
	h = h<300?300:h;
	$('#control').css('height',h+'px');
	$('#Left').css('height',(h+12)+'px');
	var btns = $(".commonBtnArea").length;
	var rh=btns>0?h-50:h;
	$('#Right').css({
		height:rh+'px',
		width:(webBodyWidth-230)+'px'
	});
	if(btns>0){
		$(".commonBtnArea").css({
			width:(webBodyWidth-210-40-16)+'px'
		});
    }
	var c_s = 0;
	$('#control').click(function(){
		if(c_s==1){
			if(btns>0){
				$(".commonBtnArea").animate({
					width:(webBodyWidth-210-40-16)+'px'
				}, "fast");
			}
			$("#Right").animate({
				width: (webBodyWidth-230)+'px'
			}, "fast");
			$("#Left").animate({
				marginLeft:"0px"
            }, "fast");
            $(this).removeClass("close");
            c_s=0;
        }else{
            if(btns>0){
                $(".commonBtnArea").animate({
                    width: (webBodyWidth-66)+'px'
                }, "fast");
            }
            $("#Right").animate({
                width: (webBodyWidth-26)+'px'
            }, "fast");
            $("#Left").animate({
                marginLeft:"-197px"
            }, "fast");
            $(this).addClass("close");
            c_s=1;
        }
    });
}

/**
 * 获取网页的宽度和高度
 * @param  {String} get    需要的宽（w）或高（h）
 * @return {Array|Int}	如果参数get存在，则返回相应宽或高，如果get没有写则返回数组
 */
function getBodySize(get) {
    var bodySize = [];
    bodySize['w']=($(document.body).width()>$(window).width())? $(document.body).width():$(window).width();
    bodySize['h']=($(document.body).height()>$(window).height())? $(document.body).height():$(window).height();
	var t_rst = get?bodySize[get]:bodySize;
    return t_rst;
}

/**
 * 通用AJAX提交
 * @param  {string} url    表单提交地址
 * @param  {string} formObj    待提交的表单对象或ID
 */
function commonAjaxSubmit(url,formObj){
    if(!formObj||formObj==''){
		formObj = "form";
    }
    if(!url||url==''){
		if($(formObj).attr('action')==''){
			url = document.URL;
		}else{
			url = $(formObj).attr('action');
		}
    }
	console.log(url);
	//打开遮罩
	$.jBox.tip("提交数据中...", 'loading');
    $(formObj).ajaxSubmit({
        url:url,
        type:"POST",
        success:function(data) {
            if(data.errcode==0){
				//取消beforeunload事件绑定
				$(window).unbind('beforeunload');
				if(data.errmsg && data.errmsg != ''){
					$.jBox.tip(data.errmsg, 'success');
				}
            }else{
				$.jBox.closeTip();
				$.jBox.error(data.errmsg, '发生错误');
            }
            if(data.url && data.url!=''){
				//取消beforeunload事件绑定
				$(window).unbind('beforeunload');
                setTimeout(function(){
                    top.window.location.href = data.url;
                },2000);
            }
            if(data.url==''||typeof(data.url)=='undefined'){
				if(data.errcode==0){
					//取消beforeunload事件绑定
					$(window).unbind('beforeunload');
					setTimeout(function(){
						top.window.location.reload(true);
					},1000);
				}
            }
        }
    });
    return false;
}

/**
 * 
 * @param {type} url	数据提交地址
 * @param {type} gdata	提交的get数据
 */
function commonAjaxGet(url,gdata){
    if(!url||url==''){
		url = document.URL;
    }
	//打开遮罩
	$.jBox.tip("操作中...", 'loading');
    $.get(
		url, gdata, function(data) {
            if(data.errcode==0){
				//取消beforeunload事件绑定
				$(window).unbind('beforeunload');
				if(data.errmsg && data.errmsg != ''){
					$.jBox.tip(data.errmsg, 'success');
				}
            }else{
				$.jBox.closeTip();
				$.jBox.error(data.errmsg, '发生错误');
            }
            if(data.url&&data.url!=''){
				//取消beforeunload事件绑定
				$(window).unbind('beforeunload');
                setTimeout(function(){
                    top.window.location.href=data.url;
                },2000);
            }
            if(data.url==''||typeof(data.url)=='undefined'){
				if(data.errcode==0){
					//取消beforeunload事件绑定
					$(window).unbind('beforeunload');
					setTimeout(function(){
						top.window.location.reload(true);
					},1000);
				}
            }
        },'json'
    );
    return false;
}