<%
'''分页类
Class Pager

	'''链接模型中代表分页数字的占位符, 跳转输入框ID
	Private s_placeholder, s_inputId

	'跳转目标URL, 每页记录, 当前页数, 总页数, 总记录数,
	Public s_target, s_pageSize, s_pageNow, s_pageCount, s_recordcount

	'分行显示分页列表,分行显示下拉列表,分页左偏移量,分页右偏移量,下拉框选项左/右修饰符,默认选中下拉框值, 分页风格
	Public LineList, LineSelect, LeftOffSet, RightOffSet, OptionLeft, OptionRight, OptionSelected, ListMode

	'第一页模型,最后页模型,上一页模型,下一页模型,当前页模型,其他页模型,跳转框模型,跳转按钮模型,下拉框模型,上一组模型,下一组模型
	Private s_first, s_last, s_prev, s_next, s_nmod, s_amod, s_jump, s_button, s_select, s_prevg, s_nextg

	'强制显示第一页/最后页/上一页/下一页/上一组/下一组
	Private s_forcedFirst, s_forcedLast, s_forcedPrev, s_forcedNext, s_forcedPrevG, s_forcedNextG

	'分页模版
	Private s_model
	
	'''构造
	Private Sub Class_Initialize()
		s_placeholder = "{p}"
		s_inputId = "page"

		LineList = True
		LineSelect = True
		LeftOffSet = 5
		RightOffSet = 5
		OptionLeft = ""
		OptionRight = ""
		OptionSelected = True
		ListMode = 3

		s_first = "<a href=""{url}"" class=""first"">{page}</a>-"
		s_last = "-<a href=""{url}"" class=""last"">{page}</a>"
		s_prev = "<a href=""{url}"" class=""prev"">&lt;</a>-"
		s_next = "=<a href=""{url}"" class=""prev"">&gt;</a>"
		s_prevg = "<a href=""{url}"" class=""prevg"">&lt;&lt;</a>--"
		s_nextg = "--<a href=""{url}"" class=""prevg"">&gt;&gt;</a>"
		s_nmod = "<a href=""{url}"" class=""now"">{page}</a>"
		s_amod = "<a href=""{url}"">{page}</a>"
		s_jump = "<input type=""text"" id=""{input}"" size=""2"" value=""{page}"" onkeydown=""{js}""/>"
		s_button = "<a href=""javascript:;"" onclick=""{js}"">GO</a>"
		s_select = "<select onchange=""{js}"">{option}</select>"

		s_forcedFirst = False
		s_forcedLast = False
		s_forcedPrev = False
		s_forcedNext = False
		s_forcedPrevG = False
		s_forcedNextG = False

		s_model = "{first}" & vbcrlf & "{prev}" & vbcrlf & "{list}" & vbcrlf& "{next}" & vbcrlf & "{last}" & vbcrlf & "--{jump}" & vbcrlf & "--{button}" & vbcrlf & "--{select}"
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''向上取整
	'p_value:要取整的值
	Private Function ceil_(Byval p_value)
	    Dim return, cei2
	    return = Int(p_value)
	    cei2 = p_value - return
	    if cei2 > 0 then
	        ceil_ = return + 1
	    else
	        ceil_ = p_value + 0 '就是Ceil=value多一个+0 强调返回值为数字型
	    End If
	End Function

	'''设置链接模型中代表分页数字的占位符,需要在Init方法前执行才有效
	Public Property Let Placeholder(Byval p_placeholder)
		s_placeholder = p_placeholder
	End Property

	'''初始化所有必需属性
	Public Sub Init(Byval p_target, Byval p_size, Byval p_now, Byval p_count)
		s_target = p_target
		s_pageSize = p_size
		s_pageNow = p_now
		s_pageCount = ceil_(p_count / p_size)
		s_recordcount = p_count
		If s_pageNow > s_pageCount Then
			s_pageNow = s_pageCount
		End If
	End Sub
	
	'''设置跳转输入框ID,用于模型参数{button}
	'p_id:输入框ID名字符串
	Public Property Let InputId(Byval p_id)
		s_inputId = p_id
	End Property
	
	'''通用的模型设置接口
	'p_name:模型名称
	'p_val:模型值
	Public Sub [Set](Byval p_name, Byval p_val)
		If InStr("first,last,prev,next,nmod,amod,jump,button,select,prevg,nextg", p_name)>0 And InStr(p_name, ",")=0 Then
			p_val = Replace(p_val, """", """""")
			Execute("s_" & p_name & " = """ & p_val & """")
		End If
	End Sub
	
	'''设置相关模版是否强制显示
	'p_name:模型名
	'p_bool:是否强制显示
	Public Property Let Forced(Byval p_name, Byval p_bool)
		If InStr("first,last,prev,next", Lcase(p_name))>0 And InStr(p_name, ",")=0 Then
			Execute("s_forced" & p_name & " = " & p_bool)
		End If
	End Property
	
	'''获取指定页的相应链接
	'p_page:指定页数
	Private Function hrefForPage_(Byval p_page)
		hrefForPage_ = Replace(s_target, s_placeholder, p_page)
	End Function
	
	'''设置分页模版
	'p_str:模板字符串
	Public Property Let Model(Byval p_str)
		s_model = p_str
	End Property
	
	'''最终分页输出
	Public Property Get OutPut()
		'构建list
		Dim t_o, t_s, t_t, t_i, t_b, t_e, t_l, t_r
		Select Case ListMode
		Case 1
			'居中风格,首补齐,尾不补齐
			t_b = s_pageNow - LeftOffSet
			t_e = s_pageNow + RightOffSet
			If s_pageNow-LeftOffSet<1 Then
				t_b = 1
				t_e = LeftOffSet + RightOffSet + 1
			End If
		Case 2
			'居中风格,首补齐,尾补齐
			t_b = s_pageNow - LeftOffSet
			t_e = s_pageNow + RightOffSet
			If s_pageNow-LeftOffSet<1 Then
				t_b = 1
				t_e = LeftOffSet + RightOffSet + 1
			ElseIf s_pageNow+RightOffSet>s_pageCount Then
				t_b = s_pageCount - LeftOffSet - RightOffSet
				t_e = s_pageCount
			End If
		Case 3
			'固定风格,不适合大于100的分页
			t_t = LeftOffSet + RightOffSet
			t_t = Int(s_pageNow/t_t) * t_t
			t_b = t_t
			t_e = t_t + LeftOffSet + RightOffSet
		Case 4
			'魔法风格,自动添加缩减
			t_t = 2*Len(Cstr(s_pageNow))-4
			t_l = LeftOffSet - t_t
			t_r = RightOffSet - t_t
			t_b = s_pageNow - t_l
			t_e = s_pageNow + t_r
			If s_pageNow-t_l<1 Then
				t_b = 1
				t_e = LeftOffSet + RightOffSet + 1
			End If
		Case Else
			'固定风格,坚持按实际参数执行
			t_b = s_pageNow - LeftOffSet
			t_e = s_pageNow + RightOffSet
		End Select
		t_s = ""
		For t_i = t_b To t_e
			If t_i>0 And t_i<=s_pageCount Then
				If t_i = s_pageNow Then
					t_s = t_s & Replace(Replace(s_nmod, "{url}", hrefForPage_(t_i)), "{page}", t_i)
				Else
					t_s = t_s & Replace(Replace(s_amod, "{url}", hrefForPage_(t_i)), "{page}", t_i)
				End If
				If LineList And t_i<>t_e Then
					t_s = t_s & vbcrlf
				End If
			End If
		Next
		t_o = Replace(s_model, "{list}", t_s)
		'第一页
		If s_forcedFirst Or t_b>1 Then
			If InStr(s_model, "{first}")>0 Then
				t_o = Replace(t_o, "{first}", Replace(Replace(s_first, "{url}", hrefForPage_(1)), "{page}", 1))
			Else
				t_o = Replace(t_o, "{first}", "")
			End If
		Else
			t_o = Replace(t_o, "{first}", "")
		End If
		'最后页
		If s_forcedLast Or t_e<s_pageCount Then
			If InStr(s_model, "{last}")>0 Then
				t_o = Replace(t_o, "{last}", Replace(Replace(s_last, "{url}", hrefForPage_(s_pageCount)), "{page}", s_pageCount))
			Else
				t_o = Replace(t_o, "{last}", "")
			End If
		Else
			t_o = Replace(t_o, "{last}", "")
		End If
		'上一页
		If s_forcedPrev Or s_pageNow>1 Then
			If InStr(s_model, "{prev}")>0 Then
				t_o = Replace(t_o, "{prev}", Replace(Replace(s_prev, "{url}", hrefForPage_(s_pageNow-1)), "{page}", Cstr(s_pageNow-1)))
			Else
				t_o = Replace(t_o, "{prev}", "")
			End If
		Else
			t_o = Replace(t_o, "{prev}", "")
		End If
		'下一页
		If s_forcedNext Or s_pageNow<s_pageCount Then
			If InStr(s_model, "{next}")>0 Then
				t_o = Replace(t_o, "{next}", Replace(Replace(s_next, "{url}", hrefForPage_(s_pageNow+1)), "{page}", Cstr(s_pageNow+1)))
			Else
				t_o = Replace(t_o, "{next}", "")
			End If
		Else
			t_o = Replace(t_o, "{next}", "")
		End If
		'上一组
		If s_forcedPrevG Or s_pageNow-LeftOffSet>0 Then
			If InStr(s_model, "{prevg}")>0 Then
				t_o = Replace(t_o, "{prevg}", Replace(Replace(s_prevg, "{url}", hrefForPage_(s_pageNow-LeftOffSet)), "{page}", Cstr(s_pageNow-LeftOffSet)))
			Else
				t_o = Replace(t_o, "{prevg}", "")
			End If
		Else
			t_o = Replace(t_o, "{prevg}", "")
		End If
		'下一组
		If s_forcedNextG Or s_pageNow+RightOffSet<=s_pageCount Then
			If InStr(s_model, "{nextg}")>0 Then
				t_o = Replace(t_o, "{nextg}", Replace(Replace(s_nextg, "{url}", hrefForPage_(s_pageNow+RightOffSet)), "{page}", Cstr(s_pageNow+RightOffSet)))
			Else
				t_o = Replace(t_o, "{nextg}", "")
			End If
		Else
			t_o = Replace(t_o, "{nextg}", "")
		End If
		'跳转框
		If InStr(s_model, "{jump}")>0 Then
			t_s = "javascript:if(event.keyCode==13){var url='" & s_target & "';location=url.replace('" & s_placeholder & "',this.value);return false;}"
			t_o = Replace(t_o, "{jump}", Replace(Replace(Replace(s_jump, "{input}", s_inputId), "{page}", s_pageNow), "{js}", t_s))
		End If
		'跳转按钮
		If InStr(s_model, "{button}")>0 Then
			t_s = "javascript:var url='" & s_target & "';location=url.replace('" & s_placeholder & "',document.getElementById('" & s_inputId & "').value);return false;"
			t_o = Replace(t_o, "{button}", Replace(Replace(Replace(s_button, "{input}", s_inputId), "{page}", s_pageNow), "{js}", t_s))
		End If
		'下拉框
		If InStr(s_model, "{select}")>0 Then
			t_t = "javascript:var url='" & s_target & "';location=url.replace('" & s_placeholder & "',this.value);return false;"
			t_s = Replace(s_select, "{js}", t_t)
			'处理option
			t_t = ""
			For t_i = 1 To s_pageCount
				If t_i = s_pageNow Then
					If OptionSelected Then
						t_t = t_t & "<option selected=""selected"" value=""" & t_i & """>" & OptionLeft & t_i & OptionRight & "</option>"
					Else
						t_t = t_t & "<option value=""" & t_i & """>" & OptionLeft & t_i & OptionRight & "</option>"
					End If
				Else
					t_t = t_t & "<option value=""" & t_i & """>" & OptionLeft & t_i & OptionRight & "</option>"
				End If
				If LineSelect Then
					t_t = t_t & vbcrlf
				End If
			Next
			t_s = Replace(t_s, "{option}", vbcrlf & t_t)
			t_o = Replace(t_o, "{select}", t_s)
		End If
		OutPut = t_o
	End Property
End Class
%>