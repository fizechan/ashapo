<%
Class Fso
	'''FSO的错误代码段暂时定为001-020
	'设置服务器是否虚拟主机
	Public IsVirtualHost
	'内部使用Fso名称
	Private s_fso
	'是否删除只读文件、是否覆盖原文件
	Private s_force, s_overwrite
	'FSO组件名、 编码、 设置文件大小显示格式
	Private s_fsoName, s_charset, s_sizeformat
	'星号替换字符、问号替换字符(这两个字符串要足够复杂防止出现路径被替换), BOM头
	Public XingStr, WenStr, FileBOM
	
	'''构造
	Private Sub Class_Initialize()
		s_fsoName 	= "Scripting.FileSystemObject"
		s_charset	= "UTF-8"
		Set s_fso 	= Server.CreateObject(s_fsoName)
		IsVirtualHost = True
		s_force		= True
		s_overwrite	= True
		XingStr = "[.$.[a.s.h.a.p.o.s.t.a.r].#.]"
		WenStr = "[.$.[a.s.h.a.p.o.q.u.e.s].#.]"
		FileBOM = "keep"
		s_sizeformat= "K"
		'Easp.Error(52) = "写入文件错误！"
		'Easp.Error(53) = "创建文件夹错误！"
		'Easp.Error(54) = "读取文件列表失败！"
		'Easp.Error(55) = "设置属性失败，文件不存在！"
		'Easp.Error(56) = "设置属性失败！"
		'Easp.Error(57) = "获取属性失败，文件不存在！"
		'Easp.Error(58) = "复制失败，源文件不存在！"
		'Easp.Error(59) = "移动失败，源文件不存在！"
		'Easp.Error(60) = "删除失败，文件不存在！"
		'Easp.Error(61) = "重命名失败，源文件不存在！"
		'Easp.Error(62) = "重命名失败，已存在同名文件！"
		'Easp.Error(63) = "文件或文件夹操作错误！"
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_fso = Nothing
	End Sub
	
	'''设置FSO组件名称
	'p_s:FSO组件名
	Public Property Let FsoName(Byval p_s)
		s_fsoName = p_s
		Set s_fso = Server.CreateObject(s_fsoName)
	End Property
	
	'''设置服务器端操作文件的文件编码
	'p_s:编码名
	Public Property Let CharSet(Byval p_s)
		s_charset = Ucase(p_s)
	End Property
	
	'''设置是否删除服务器上的只读文件
	'p_b:bool值
	Public Property Let Force(Byval p_b)
		s_force = p_b
	End Property
	
	'''设置是否覆盖服务器端原有文件
	'p_b:bool值
	Public Property Let OverWrite(Byval p_b)
		s_overwrite = p_b
	End Property
	
	'''设置文件大小显示格式
	'p_s可选值有：G,M,K,b,auto/NUM，默认值为 K。
	Public Property Let SizeFormat(Byval p_s)
		s_sizeformat = p_s
	End Property
	
	'''判断路径是否带有?/*等通配符
	'p_p:路径
	Private Function isWildcards_(Byval p_p)
		isWildcards_ = False
		If Instr(p_p,"*")>0 Or Instr(p_p,"?")>0 Then
			isWildcards_ = True
		End If
	End Function
	
	'''获取实际路径MapPath
	'p_p:路径
	Private Function absPath_(Byval p_p)
		If CStr(p_p) = "" Then
			absPath_ = "" : Exit Function
		End If
		If Mid(p_p,2,1)<>":" Then
			If isWildcards_(p_p) Then
				p_p = Replace(p_p,"*",XingStr)
				p_p = Replace(p_p,"?",WenStr)
				p_p = Server.MapPath(p_p)
				p_p = Replace(p_p,WenStr,"?")
				p_p = Replace(p_p,XingStr,"*")
			Else
				p_p = Server.MapPath(p_p)
			End If
		End If
		If Right(p_p,1) = "\" Then
			p_p = Left(p_p,Len(p_p)-1)
		End If
		absPath_ = p_p
	End Function
	
	'''获取文件或文件夹的物理绝对路径
	'p_p:路径
	Public Function MapPath(Byval p_p)
		MapPath = absPath_(p_p)
	End Function
	
	'''判断文件是否存在
	'p_p:文件路径
	Public Function IsFile(Byval p_p)
		p_p = absPath_(p_p)
		IsFile = False
		If s_fso.FileExists(p_p) Then
			IsFile = True
		End If
	End Function
	
	'''判断服务器上的文件夹是否存在
	'p_p:文件夹路径
	Public Function IsFolder(Byval p_p)
		p_p = absPath_(p_p)
		IsFolder = False
		If s_fso.FolderExists(p_p) Then
			IsFolder = True
		End If
	End Function
	
	'''判断文件或文件夹是否存在
	'p_p:路径
	Public Function IsExists(Byval p_p)
		IsExists = False
		If IsFile(p_p) or IsFolder(p_p) Then
			IsExists = True
		End If
	End Function
	
	'''读取服务器端文件内容,可指定编码
	'p_p:文件路径[>读取编码形式]
	Public Function Read(Byval p_p)
		Dim t_p, t_f, t_s, t_t, t_c
		t_c = s_charset
		If Instr(p_p,">")>0 Then
			t_c = UCase(Trim(CRight(p_p,">")))
			p_p = Trim(CLeft(p_p,">"))
		End If
		t_p = absPath_(p_p)
		If IsFile(t_p) Then
			Set t_s = Server.CreateObject("ADODB.Stream")
			With t_s
				.Type = 2
				.Mode = 3
				.Open
				.LoadFromFile t_p
				.Charset = t_c
				.Position = 2
				t_t = .ReadText
				.Close
			End With
			Set t_s = Nothing
			If t_c = "UTF-8" Then
				Select Case FileBOM
					Case "keep"
						'Do Nothing
					Case "remove"
						If RegTest(t_t, "^\uFEFF") Then
							t_t = RegReplace(t_t, "^\uFEFF", "")
						End If
					Case "add"
						If Not RegTest(t_t, "^\uFEFF") Then
							t_t = Chrw(&hFEFF) & t_t
						End If
				End Select
			End If
		Else
			t_t = ""
			'收集错误,Log信息收集
			Sa.E.Raise(1)
		End If
		Read = t_t
	End Function
	
	'''在服务器上创建文件夹
	'p_p:路径
	Public Function CreateFolder(Byval p_p)
		'On Error Resume Next
		Dim t_p, t_a, t_i : CreateFolder = True
		t_p = absPath_(p_p)
		t_a = Split(t_p,"\") : t_p = ""
		For t_i = 0 To Ubound(t_a)
			t_p = t_p & t_a(t_i) & "\"
			If IsVirtualHost Then
				If Instr(t_p, absPath_("/") & "\")>0 Then
					If Not IsFolder(t_p) And t_i>0 Then
						s_fso.CreateFolder(t_p)
					End If
				End If
			Else
				If Not IsFolder(t_p) And t_i>0 Then
					s_fso.CreateFolder(t_p)
				End If
			End If
		Next
		If Err.Number<>0 Then
			CreateFolder = False
			'收集错误,Log信息收集
			Sa.E.Raise(2)
		End If
		Err.Clear()
	End Function
	
	'''在服务器上创建文件夹
	'CreateFolder的别名
	'p_p:路径
	Public Function MD(Byval p_p)
		MD = CreateFolder(p_p)
	End Function
	
	'''以ADODB.Stream保存内容至指定文件
	'p_p:路径
	'p_c:二进制流(文件写入内容)
	Public Function SaveAs(Byval p_p, Byval p_c)
		'On Error Resume Next
		Dim  t_p, t_s
		t_p = absPath_(p_p)
		SaveAs = MD(Left(t_p, InstrRev(t_p,"\")-1))
		If SaveAs Then
			Set t_s = Server.CreateObject("ADODB.Stream")
			With t_s
				.Type = 1
				.Open
				.Write p_c
				.SaveToFile t_p, IIF(s_overwrite,2,1)
				.Close
			End With
			Set t_s = Nothing
		End If
		If Err.Number<>0 Then
			SaveAs = False
			'收集错误,Log信息收集
			Sa.E.Raise(3)
		End If
		Err.Clear()
	End Function
	
	'''在服务器端创建文件并写入内容
	'p_p:路径
	'p_c:文件写入内容字符串
	Public Function CreateFile(Byval p_p, Byval p_c)
		On Error Resume Next
		Dim t_p, t_c, t_s
		t_c = s_charset
		t_p = absPath_(p_p)
		CreateFile = MD(Left(t_p,InstrRev(t_p,"\")-1))
		If CreateFile Then
			Set t_s = Server.CreateObject("ADODB.Stream")
			If Lcase(FileBOM) = "remove" And t_c = "UTF-8" Then
				With t_s
					.Type = 2
					.Mode = 3
					.Charset = t_c
					.Open
					.Writetext(p_c)
					.Position = 3
				End With
				Dim t_n : Set t_n = Server.CreateObject("ADODB.Stream")   
				With t_n   
					.Mode = 3   
					.Type = 1   
					.Open()   
				End With
				t_s.CopyTo(t_n)
				t_n.SaveToFile t_p, IIF(s_overwrite,2,1)
				t_s.Flush
				t_s.Close
				Set t_n = Nothing
			Else
				With t_s
					.Type = 2
					.Open
					.Charset = t_c
					.Position = t_s.Size
					.WriteText = p_c
					.SaveToFile t_p,IIF(s_overwrite,2,1)
					.Close
				End With
			End If
			Set t_s = Nothing
		End If
		If Err.Number<>0 Then
			CreateFile = False
			'收集错误,Log信息收集
			Sa.E.Raise(4)
		End If
		Err.Clear()
	End Function
	
	'''按正则表达式更新文件内容
	'p_p:文件路径
	'p_r:要替换的字符串正则
	'p_s:要替换的字符串,支持正则捕获
	Public Function UpdateFile(Byval p_p, Byval p_r, Byval p_s)
		Dim t_t : p_p = absPath_(p_p)
		t_t = RegReplace(Read(p_p), p_r, p_s)
		UpdateFile = CreateFile(p_p, t_t)
	End Function
	
	'''追加文件的内容
	'p_p:路径
	'p_c:文件追加内容字符串
	Public Function AppendFile(Byval p_p, Byval p_c)
		Dim t_t : p_p = absPath_(p_p)
		t_t = Read(p_p) & p_c
		AppendFile = CreateFile(p_p, t_t)
	End Function
		
	'''格式化输出的文件大小
	'p_s:文件大小(数字)
	'p_l:格式化格式,可选值有：G,M,K,b,auto/NUM
	Public Function FormatSize(Byval p_s, Byval p_l)
		Dim t_s : t_s = Int(p_s) : p_l = UCase(p_l)
		If p_l = "NUM" Then
			FormatSize = p_s
			Exit Function
		End If
		FormatSize = IIF(t_s/(1073741824)>0.01,FormatNumber(t_s/(1073741824),2,-1,0,-1),"0.01") & " GB"
		If t_s = 0 Then
			FormatSize = "0 GB"
		End If
		If p_l = "G" Or (p_l="AUTO" And t_s>1073741824) Then
			Exit Function
		End If
		FormatSize = IIF(t_s/(1048576)>0.1,FormatNumber(t_s/(1048576),1,-1,0,-1),"0.1") & " MB"
		If t_s = 0 Then
			FormatSize = "0 MB"
		End If
		If p_l = "M" Or (p_l="AUTO" And t_s>1048576) Then
			Exit Function
		End If
		FormatSize = IIF((t_s/1024)>1,Int(t_s/1024),1) & " KB"
		If t_s = 0 Then
			FormatSize = "0 KB"
		End If
		If p_l = "K" Or (p_l="AUTO" And t_s>1024) Then
			Exit Function
		End If
		If p_l = "B" Or p_l = "AUTO" Then
			FormatSize = t_s & " bytes"
		Else
			FormatSize = t_s
		End If
	End Function
	
	'''按数字转化成文件属性
	'p_n:文件属性(数字值)
	Private Function attr2Str_(Byval p_n)
		Dim t_a, t_s : t_a = Int(p_n)
		If t_a >= 2048 Then
			t_a = t_a - 2048
		End If
		If t_a >= 1024 Then
			t_a = t_a - 1024
		End If
		If t_a >= 32 Then
			t_s = "A"
			t_a = t_a - 32
		End If
		If t_a >= 16 Then
			t_a = t_a- 16
		End If
		If t_a >= 8 Then
			t_a = t_a - 8
		End If
		If t_a >= 4 Then
			t_s = "S" & t_s
			t_a = t_a - 4
		End If
		If t_a >= 2 Then
			t_s = "H" & t_s
			t_a = t_a - 2
		End If
		If t_a >= 1 Then
			t_s = "R" & t_s
			t_a = t_a - 1
		End If
		attr2Str_ = t_s
	End Function
	
	'''列出服务器上指定目录下所有的文件或文件夹
	'p_p:文件夹路径
	'p_t:输出类型，可选值有：0 或 ""，1 或 "file"，2 或 "folder"
	Public Function [List](Byval p_p, Byval p_t)
		'On Error Resume Next
		Dim t_f, t_s, t_k, t_a(), t_i, t_l
		p_p = absPath_(p_p) : t_i = 0
		Select Case LCase(p_t)
			Case "1","file" t_l = 1
			Case "2","folder" t_l = 2
			Case Else t_l = 0
		End Select
		Set t_f = s_fso.GetFolder(p_p)
		If t_l = 0 Or t_l = 2 Then
			Set t_s = t_f.SubFolders
			ReDim Preserve t_a(4, t_s.Count-1)
			For Each t_k In t_s
				t_a(0,t_i) = t_k.Name
				t_a(1,t_i) = FormatSize(t_k.Size,s_sizeformat)
				t_a(2,t_i) = t_k.DateLastModified
				t_a(3,t_i) = attr2Str_(t_k.Attributes)
				t_a(4,t_i) = t_k.Type
				t_i = t_i + 1
			Next
		End If
		If t_l = 0 Or t_l = 1 Then
			Set t_s = t_f.Files
			ReDim Preserve t_a(4, t_s.Count+t_i-1)
			For Each t_k In t_s
				t_a(0, t_i) = t_k.Name
				t_a(1, t_i) = FormatSize(t_k.Size, s_sizeformat)
				t_a(2, t_i) = t_k.DateLastModified
				t_a(3, t_i) = attr2Str_(t_k.Attributes)
				t_a(4, t_i) = t_k.Type
				t_i = t_i + 1
			Next
		End If
		Set t_s = Nothing
		Set t_f = Nothing
		[List] = t_a
		If Err.Number<>0 Then
			'收集错误,Log信息收集
			Sa.E.Raise(5)
		End If
		Err.Clear()
	End Function
	
	'''列出服务器上指定目录下所有的文件和文件夹
	'p_p:文件夹路径
	Public Function Dir(Byval p_p)
		Dir = List(p_p,0)
	End Function
	
	'''获取文件名或扩展名(后缀)
	'p_f:文件名(可带路径一起)
	'p_f:0获取文件名,1获取扩展名后缀
	Private Function getNameOf_(Byval p_f, Byval p_t)
		Dim t_r, t_n, t_e
		If IsN(p_f) Then
			getNameOf_ = "" : Exit Function
		End If
		p_f = Replace(p_f,"\", "/")
		If Right(p_f,1) = "/" Then
			t_r = Split(p_f, "/")
			getNameOf_ = IIF(p_t=0, t_r(Ubound(t_r)-1),"")
			Exit Function
		ElseIf Instr(p_f,"/")>0 Then
			t_r = Split(p_f,"/")(Ubound(Split(p_f,"/")))
		Else
			t_r = p_f
		End If
		If Instr(t_r,".")>0 Then
			t_n = Left(t_r,InstrRev(t_r,".")-1)
			t_e = Mid(t_r,InstrRev(t_r,"."))
		Else
			t_n = t_r
			t_e = ""
		End If
		If p_t = 0 Then
			getNameOf_ = t_n
		ElseIf p_t = 1 Then
			getNameOf_ = t_e
		End If
	End Function
	
	'''获取文件名
	'p_f:文件名(可带路径一起)
	Public Function NameOf(Byval p_f)
		NameOf = getNameOf_(p_f, 0)
	End Function
	
	'''取文件扩展名(后缀名)
	'p_f:文件名(可带路径一起)
	Public Function ExtOf(Byval p_f)
		ExtOf = getNameOf_(p_f, 1)
	End Function
	
	'''设置文件或文件夹属性
	'p_p:路径
	'p_t:R只读/A存档/S系统/H隐藏(+附件,-去除,以" "或者","进行分割)
	Public Function SetAttr(Byval p_p, Byval p_t)
		'On Error Resume Next
		Dim t_p, t_a, t_i, t_n, t_f, t_t
		t_p = absPath_(p_p) : t_n = 0 : SetAttr = True
		If not IsExists(t_p) Then
			SetAttr = False
			'收集错误,Log信息收集
			Sa.E.Raise(6)
			Exit Function
		End If
		If IsFile(t_p) Then
			Set t_f = s_fso.GetFile(t_p)
		ElseIf IsFolder(t_p) Then
			Set t_f = s_fso.GetFolder(t_p)
		End If
		t_t = t_f.Attributes
		t_a = UCase(p_t)
		If Instr(t_a,"+")>0 Or Instr(t_a,"-")>0 Then
			t_a = IIF(Instr(t_a," ")>0,Split(t_a," "),Split(t_a,","))
			For t_i = 0 To Ubound(t_a)
				Select Case t_a(t_i)
				Case "+R"
					t_t = IIF(t_t And 1,t_t,t_t+1)
				Case "-R"
					t_t = IIF(t_t And 1,t_t-1,t_t)
				Case "+H"
					t_t = IIF(t_t And 2,t_t,t_t+2)
				Case "-H"
					t_t = IIF(t_t And 2,t_t-2,t_t)
				Case "+S"
					t_t = IIF(t_t And 4,t_t,t_t+4)
				Case "-S"
					t_t = IIF(t_t And 4,t_t-4,t_t)
				Case "+A"
					t_t = IIF(t_t And 32,t_t,t_t+32)
				Case "-A"
					t_t = IIF(t_t And 32,t_t-32,t_t)
				End Select
			Next
			t_f.Attributes = t_t
		Else
			For t_i = 1 To Len(t_a)
				Select Case Mid(t_a,t_i,1)
				Case "R"
					t_n = t_n + 1
				Case "H"
					t_n = t_n + 2
				Case "S"
					t_n = t_n + 4
				End Select
			Next
			t_f.Attributes = IIF(t_t And 32,t_n+32,t_n)
		End If
		Set t_f = Nothing
		If Err.Number<>0 Then
			SetAttr = False
			'收集错误,Log信息收集
			Sa.E.Raise(6)
		End If
		Err.Clear()
	End Function
	
	'''获取文件或文件夹属性
	'p_p:路径
	'p_t:属性名称或者对应数字
	Public Function GetAttr(Byval p_p, Byval p_t)
		Dim t_f, t_s, t_p : t_p = absPath_(p_p)
		If IsFile(t_p) Then
			Set t_f = s_fso.GetFile(t_p)
		ElseIf IsFolder(t_p) Then
			Set t_f = s_fso.GetFolder(t_p)
		Else
			GetAttr = ""
			'收集错误,Log信息收集
			Sa.E.Raise(7)
			Exit Function
		End If
		Select Case LCase(p_t)
		Case "0","name"
			t_s = t_f.Name
		Case "1","date", "datemodified"
			t_s = t_f.DateLastModified
		Case "2","datecreated"
			t_s = t_f.DateCreated
		Case "3","dateaccessed"
			t_s = t_f.DateLastAccessed
		Case "4","size"
			t_s = FormatSize(t_f.Size,s_sizeformat)
		Case "5","attr"
			t_s = attr2Str_(t_f.Attributes)
		Case "6","type"
			t_s = t_f.Type
		Case Else
			t_s = ""
		End Select
		Set t_f = Nothing
		GetAttr = t_s
	End Function
	
	'''FSO核心操作
	'p_f：操作路径
	'p_t：目标路径
	'p_o：对象类型(文件或文件夹)
	'p_m：操作类型
	Private Function fofo_(Byval p_f, Byval p_t, Byval p_o, Byval p_m)
		'On Error Resume Next
		fofo_ = True
		Dim t_ff, t_tf, t_oc, t_of, t_oi, t_ot, t_os
		t_ff = absPath_(p_f) : t_tf = absPath_(p_t)
		If p_o = 0 Then
			t_oc = IsFile(t_ff) : t_of = "File" : t_oi = "文件"
		ElseIf p_o = 1 Then
			t_oc = IsFolder(t_ff) : t_of = "Folder" : t_oi = "文件夹"
		End If
		If p_m = 0 Then
			t_ot = "Copy" : t_os = "复制"
		ElseIf p_m = 1 Then
			t_ot = "Move" : t_os = "移动"
		ElseIf p_m = 2 Then
			t_ot = "Delete" : t_os = "删除"
		End If
		If t_oc Then
			If p_m <> 2 Then
				If p_o = 0 Then
					If Right(p_t,1)="/" Or Right(p_t,1)="\" Then
						fofo_ = MD(t_tf) : t_tf = t_tf & "\"
					Else
						fofo_ = MD(Left(t_tf,InstrRev(t_tf,"\")-1))
					End If
				ElseIf p_o = 1 Then
					t_tf = t_tf & "\"
					fofo_ = MD(t_tf)
				End If
				'该注释语句无法强制移动
				'Execute("s_fso." & t_ot & t_of & " t_ff,t_tf" & IfThen(p_m=0,",s_overwrite"))
				If p_m = 1 Then
					'强行移动(先复制后删除)
					Execute("s_fso.Copy" & t_of & " t_ff,t_tf,s_overwrite")
					Execute("s_fso.Delete" & t_of & " t_ff,s_force")
				Else
					Execute("s_fso." & t_ot & t_of & " t_ff,t_tf,s_overwrite")
				End If
			Else
				Execute("s_fso." & t_ot & t_of & " t_ff,s_force")
			End If
			If Err.Number<>0 Then
				fofo_ = False
				'出错处理
				'Easp.Error.Msg = "<br />" & os & oi & "失败！" & "( "&frompath&" "&Easp.IIF(p_m=2,"",os&"到 "&toPath)&" )"
				Sa.E.Raise(8)
			End If
		ElseIf isWildcards_(t_ff) Then
			If p_m<>2 Then
				fofo_ = MD(t_tf)
				'该注释语句无法强制移动
				'Execute("s_fso." & t_ot & t_of & " t_ff,t_tf" & IIF(p_m=0,",s_overwrite",""))
				If p_m = 1 Then
					'强行移动(先复制后删除)
					Execute("s_fso.Copy" & t_of & " t_ff,t_tf,s_overwrite")
					Execute("s_fso.Delete" & t_of & " t_ff,s_force")
				Else
					Execute("s_fso." & t_ot & t_of & " t_ff,t_tf,s_overwrite")
				End If
			Else
				Execute("s_fso." & t_ot & t_of & " t_ff,s_force")
			End If
			If Err.Number<>0 Then
				fofo_ = False
				'出错处理
				'Easp.Error.Msg = "<br />" & os & oi & "失败！" & "( "&frompath&" "&Easp.IIF(p_m=2,"",os&"到 "&toPath)&" )"
				Sa.E.Raise(8)
			End If
		Else
			fofo_ = False
			'出错处理
			'Easp.Error.Msg = "<br />" & os & oi & "失败！" & Easp.IIF(p_m=2,"","源")&oi&"不存在( "&frompath&" )"
			Sa.E.Raise(8)
		End If
		Err.Clear()
	End Function
	
	'''复制文件(支持通配符 * 和 ? )
	'p_f：操作文件路径
	'p_t：目标文件路径
	Public Function CopyFile(Byval p_f, Byval p_t)
		CopyFile = fofo_(p_f,p_t,0,0)
	End Function
	
	'''复制文件夹(支持通配符 * 和 ? )
	'p_f：操作文件夹路径
	'p_t：目标文件夹路径
	Public Function CopyFolder(Byval p_f, Byval p_t)
		CopyFolder = fofo_(p_f,p_t,1,0)
	End Function
	
	'''复制文件或文件夹(支持通配符 * 和 ? )
	'p_f：操作路径
	'p_t：目标路径
	Public Function Copy(Byval p_f, Byval p_t)
		Dim t_ff,t_tf : t_ff = absPath_(p_f) : t_tf = absPath_(p_t)
		If IsFile(t_ff) Then
			Copy = CopyFile(p_f,p_t)
		ElseIf IsFolder(t_ff) Then
			Copy = CopyFolder(p_f,p_t)
		Else
			Copy = False
			'出错处理
			Sa.E.Raise(9)
		End If
	End Function
	
	'''移动文件(支持通配符 * 和 ? )
	'p_f：操作文件路径
	'p_t：目标文件路径
	Public Function MoveFile(Byval p_f, Byval p_t)
		MoveFile = fofo_(p_f,p_t,0,1)
	End Function
	
	'''移动文件夹(支持通配符 * 和 ? )
	'p_f：操作文件夹路径
	'p_t：目标文件夹路径
	Public Function MoveFolder(Byval p_f, Byval p_t)
		MoveFolder = fofo_(p_f,p_t,1,1)
	End Function
	
	'''移动文件或文件夹(注意:不支持通配符 * 和 ? )
	'p_f：操作路径
	'p_t：目标路径
	Public Function Move(Byval p_f, Byval p_t)
		Dim t_ff,t_tf : t_ff = absPath_(p_f) : t_tf = absPath_(p_t)
		If IsFile(t_ff) Then
			Move = MoveFile(p_f,p_t)
		ElseIf IsFolder(t_ff) Then
			Move = MoveFolder(p_f,p_t)
		Else
			Move = False
			'出错处理
			'Easp.Error.Msg = "(" & fromPath & ")"
			Sa.E.Raise(10)
		End If
	End Function
	
	'''删除文件(支持通配符 * 和 ? )
	'p_p：操作文件路径(支持通配符 * 和 ? )
	Public Function DelFile(Byval p_p)
		DelFile = fofo_(p_p,"",0,2)
	End Function
	
	'''删除文件夹(支持通配符 * 和 ? )
	'p_p：操作文件夹路径(支持通配符 * 和 ? )
	Public Function DelFolder(Byval p_p)
		DelFolder = fofo_(p_p,"",1,2)
	End Function
	
	'''删除文件夹(支持通配符 * 和 ? )
	'DelFolder的别名
	'p_p：操作文件夹路径(支持通配符 * 和 ? )
	Public Function RD(Byval p_p)
		RD = DelFolder(p_p)
	End Function
	
	'''删除文件或文件夹(支持通配符 * 和 ? )
	'p_p：操作路径(支持通配符 * 和 ? )
	Public Function Del(Byval p_p)
		Dim t_p : t_p = absPath_(p_p)
		If IsFile(t_p) Then
			Del = DelFile(p_p)
		ElseIf IsFolder(t_p) Then
			Del = DelFolder(p_p)
		Else
			Del = False
			'出错处理
			Sa.E.Raise(11)
		End If
	End Function
	
	'''重命名文件或文件夹
	'p_p：操作路径
	'p_n:新文件夹名或新文件名
	Public Function Rename(Byval p_p, Byval p_n)
		'On Error Resume Next
		Dim t_s,t_t : t_t = 1	'操作方式
		Dim t_p,t_n : t_p = absPath_(p_p) : Rename = True
		t_n = Left(t_p,InstrRev(t_p,"\")) & p_n
		If Not IsExists(t_p) Then
			Rename = False
			'出错处理
			Sa.E.Raise(12)
			Exit Function
		End If
		If IsExists(t_n) Then
			Rename = False
			'出错处理
			Sa.E.Raise(13)
			Exit Function
		End If
		If IsFolder(t_p) Then
			If t_t = 1 Then
				Set t_s = s_fso.GetFolder(t_p)
				t_s.Name = p_n
			Else
				'这里是可能捕获的出错地方
				s_fso.MoveFolder t_p,t_n
			End If
		ElseIf IsFile(t_p) Then
			If t_t = 1 Then
				Set t_s = s_fso.GetFile(t_p)
				t_s.Name = p_n
			Else
				'这里是可能捕获的出错地方
				Rename = Copy(t_p, t_n)
				Rename = Del(t_p)
			End If
		End If
		Set t_s = Nothing
		If Err.Number<>0 Then
			Sa.E.Raise(14)
			Rename = False
		End If
		Err.Clear()
	End Function
	
	'''重命名文件或文件夹
	'Rename的别名
	Public Function Ren(Byval p_p, Byval p_n)
		Ren = Rename(p_p, p_n)
	End Function
	
End Class
%>