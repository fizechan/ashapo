<%
'''SHA1,暂时不支持中文
Class Sha1
	'编码
	Public CharSet
	Private s_lOnBits(30)
	Private s_l2Power(30)
	
	'''构造
	Private Sub Class_Initialize()
		CharSet = "UTF-8"
		s_lOnBits(0) = CLng(1)
		s_lOnBits(1) = CLng(3)
		s_lOnBits(2) = CLng(7)
		s_lOnBits(3) = CLng(15)
		s_lOnBits(4) = CLng(31)
		s_lOnBits(5) = CLng(63)
		s_lOnBits(6) = CLng(127)
		s_lOnBits(7) = CLng(255)
		s_lOnBits(8) = CLng(511)
		s_lOnBits(9) = CLng(1023)
		s_lOnBits(10) = CLng(2047)
		s_lOnBits(11) = CLng(4095)
		s_lOnBits(12) = CLng(8191)
		s_lOnBits(13) = CLng(16383)
		s_lOnBits(14) = CLng(32767)
		s_lOnBits(15) = CLng(65535)
		s_lOnBits(16) = CLng(131071)
		s_lOnBits(17) = CLng(262143)
		s_lOnBits(18) = CLng(524287)
		s_lOnBits(19) = CLng(1048575)
		s_lOnBits(20) = CLng(2097151)
		s_lOnBits(21) = CLng(4194303)
		s_lOnBits(22) = CLng(8388607)
		s_lOnBits(23) = CLng(16777215)
		s_lOnBits(24) = CLng(33554431)
		s_lOnBits(25) = CLng(67108863)
		s_lOnBits(26) = CLng(134217727)
		s_lOnBits(27) = CLng(268435455)
		s_lOnBits(28) = CLng(536870911)
		s_lOnBits(29) = CLng(1073741823)
		s_lOnBits(30) = CLng(2147483647)
		'
		s_l2Power(0) = CLng(1)
		s_l2Power(1) = CLng(2)
		s_l2Power(2) = CLng(4)
		s_l2Power(3) = CLng(8)
		s_l2Power(4) = CLng(16)
		s_l2Power(5) = CLng(32)
		s_l2Power(6) = CLng(64)
		s_l2Power(7) = CLng(128)
		s_l2Power(8) = CLng(256)
		s_l2Power(9) = CLng(512)
		s_l2Power(10) = CLng(1024)
		s_l2Power(11) = CLng(2048)
		s_l2Power(12) = CLng(4096)
		s_l2Power(13) = CLng(8192)
		s_l2Power(14) = CLng(16384)
		s_l2Power(15) = CLng(32768)
		s_l2Power(16) = CLng(65536)
		s_l2Power(17) = CLng(131072)
		s_l2Power(18) = CLng(262144)
		s_l2Power(19) = CLng(524288)
		s_l2Power(20) = CLng(1048576)
		s_l2Power(21) = CLng(2097152)
		s_l2Power(22) = CLng(4194304)
		s_l2Power(23) = CLng(8388608)
		s_l2Power(24) = CLng(16777216)
		s_l2Power(25) = CLng(33554432)
		s_l2Power(26) = CLng(67108864)
		s_l2Power(27) = CLng(134217728)
		s_l2Power(28) = CLng(268435456)
		s_l2Power(29) = CLng(536870912)
		s_l2Power(30) = CLng(1073741824)
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''
	Private Function cByteW_(Byval p_s)
		'cByteW_ = CByte(p_s)
		cByteW_ = Cint(p_s)
	End Function
	
	'''
	Private Function andW_(Byref p_o, Byref p_t)
        Dim t_a(3)
        Dim t_i
        For t_i = 0 To 3
            t_a(t_i) = cByteW_(p_o(t_i) And p_t(t_i))
        Next
        andW_ = t_a
    End Function
	
	'''
	Private Function orW_(Byref p_o, Byref p_t)
        Dim t_a(3)
        Dim t_i
        For t_i = 0 To 3
            t_a(t_i) = cByteW_(p_o(t_i) Or p_t(t_i))
        Next
        orW_ = t_a
    End Function
	
	'''
	Private Function xorW_(Byref p_o, Byref p_t)
        Dim t_a(3)
        Dim t_i
        For t_i = 0 To 3
            t_a(t_i) = cByteW_(p_o(t_i) Xor p_t(t_i))
        Next
        xorW_ = t_a
    End Function
	
	'''
	Private Function notW_(Byref p_o)
        Dim t_a(3)
        Dim t_i
        For t_i = 0 To 3
            t_a(t_i) = Not cByteW_(p_o(t_i))
        Next
        notW_ = t_a
    End Function
	
	'''
	Private Function addW_(Byref p_o, Byref p_t)
        Dim t_i
        Dim t_t
        Dim t_a(3)
        For t_i = 3 To 0 Step -1
            If t_i = 3 Then
                t_t = CInt(p_o(t_i)) + p_t(t_i)
                t_a(t_i) = t_t Mod 256
            Else
                t_t = CInt(p_o(t_i)) + p_t(t_i) + (t_t \ 256)
                t_a(t_i) = t_t Mod 256
            End If
        Next
        addW_ = t_a
    End Function
	
	'''
	Private Function dMod_(Byref p_v, Byref p_s)
        Dim t_s
        t_s = CDbl(CDbl(p_v) - (Int(CDbl(p_v) / CDbl(p_s)) * CDbl(p_s)))
        If t_s < 0 Then
            t_s = CDbl(t_s + p_s)
        End If
        dMod_ = t_s
    End Function
	
	'''
	Private Function doubleToWord_(Byref p_v)
        Dim t_a(3)
        t_a(0) = Int(dMod_(p_v, 2 ^ 32) / (2 ^ 24))
        t_a(1) = Int(dMod_(p_v, 2 ^ 24) / (2 ^ 16))
        t_a(2) = Int(dMod_(p_v, 2 ^ 16) / (2 ^ 8))
        t_a(3) = Int(dMod_(p_v, 2 ^ 8))
        doubleToWord_ = t_a
    End Function
	
	'''
	Private Function wordToDouble_(Byref p_v)
        wordToDouble_ = CDbl((p_v(0) * (2 ^ 24)) + (p_v(1) * (2 ^ 16)) + (p_v(2) * (2 ^ 8)) + p_v(3))
    End Function
	
	'''
	Private Function cShiftLeftW_(Byref p_v, Byref p_s)
        Dim t_o, t_t
        t_o = wordToDouble_(p_v)
        t_t = t_o
        t_o = CDbl(t_o * (2 ^ p_s))
        t_t = CDbl(t_t / (2 ^ (32 - p_s)))
        cShiftLeftW_ = orW_(doubleToWord_(t_o), doubleToWord_(t_t))
    End Function
	
	'''
	Private Function wordToHex_(Byref p_v)
        Dim t_i
        For t_i = 0 To 3
            wordToHex_ = wordToHex_ & Right("0" & Hex(p_v(t_i)), 2)
        Next
    End Function
	
	'''
	Private Function hexToWord_(Byref p_h)
        hexToWord_ = doubleToWord_(CDbl("&h" & p_h))
    End Function
	
	'''
	Private Function ff_(Byref p_t, Byref p_b, Byref p_c, Byref p_d)
        If p_t <= 19 Then
            ff_ = orW_(andW_(p_b, p_c), andW_((notW_(p_b)), p_d))
        ElseIf p_t <= 39 Then
            ff_ = xorW_(xorW_(p_b, p_c), p_d)
        ElseIf p_t <= 59 Then
            ff_ = orW_(orW_(andW_(p_b, p_c), andW_(p_b, p_d)), andW_(p_c, p_d))
        Else
            ff_ = xorW_(xorW_(p_b, p_c), p_d)
        End If
    End Function
	
	'''
	Private Function toUnicode_(Byval p_s)
		Dim t_o : Set t_o = Server.CreateObject("ADODB.Stream")
		With t_o
			.Charset = CharSet
			.Type = 2
			.Open
			.WriteText p_s
			.Position = 0
			.Charset = "Unicode"
			.Type = 1
			toUnicode_ = MidB(.Read, 1)
		End With
		Set t_o = Nothing
	End Function

	'Unicode
	Private Function strConv_(Byval p_s)
		Dim rs, stm, bytAry, intLen
		If Len(p_s & "") > 0 Then
			p_s = MidB(p_s, 1)
			intLen = LenB(p_s)
			Set rs = Server.CreateObject("ADODB.Recordset")
			Set stm = Server.CreateObject("ADODB.Stream")
			With rs
				.Fields.Append "X", 205, intLen
				.Open
				.AddNew
				rs(0).AppendChunk p_s & ChrB(0)
				.Update
				bytAry = rs(0).GetChunk(intLen)
			End With
			With stm
				.Type = 1
				.Open
				.Write bytAry
				.Position = 0
				.Type = 2
				.Charset = CharSet
				strConv_ = .ReadText
			End With
		End If
		stm.Close
		Set stm = Nothing
		rs.Close
		Set rs = Nothing
	End Function
	
	'''
	Public Function strLen_(Byval p_s)
		Dim t_i, t_n : t_n = 0
		For t_i = 1 To Len(p_s)
			If Abs(Ascw(Mid(p_s,t_i,1)))>255 Then
				t_n = t_n + 2
			Else
				t_n = t_n + 1
			End If
		Next
		strLen_ = t_n
	End Function
	
	'''使用SHA1加密算法加密处理字符串
	'p_s:要加密的字符串
	Public Function SecureHash(Byval p_s)
		'p_s = strConv_(toUnicode_(p_s))
		'p_s = strConv_(p_s)
        Dim t_l, t_lw, t_sm, t_nb, t_wa(79), t_bt, t_wt, t_b, t_i, t_ta, t_vwa(3)
        Dim t_h0a, t_h1a, t_h2a, t_h3a, t_h4a, t_waa, t_wba, t_wca, t_wda, t_wea, t_wfa
		't_l = Len(p_s)
		t_l = strLen_(p_s)
		t_lw = doubleToWord_(CDbl(t_l) * 8)
		t_sm = p_s & ChrW(128) & String((128 - (t_l Mod 64) - 9) Mod 64, ChrW(0))
		t_sm = t_sm & String(4, ChrW(0)) & ChrW(t_lw(0)) & ChrW(t_lw(1)) & ChrW(t_lw(2)) & ChrW(t_lw(3))
		t_nb = Len(t_sm) / 64
		't_nb = strLen_(t_sm) / 64
		t_vwa(0) = hexToWord_("5A827999")
		t_vwa(1) = hexToWord_("6ED9EBA1")
		t_vwa(2) = hexToWord_("8F1BBCDC")
		t_vwa(3) = hexToWord_("CA62C1D6")
		t_h0a = hexToWord_("67452301")
		t_h1a = hexToWord_("EFCDAB89")
		t_h2a = hexToWord_("98BADCFE")
		t_h3a = hexToWord_("10325476")
		t_h4a = hexToWord_("C3D2E1F0")
		For t_b = 0 To t_nb - 1
			t_bt = Mid(t_sm, (t_b * 64) + 1, 64)
			For t_i = 0 To 15
				t_wt = Mid(t_bt, (t_i * 4) + 1, 4)
				t_wa(t_i) = Array(AscB(Mid(t_wt, 1, 1)), AscB(Mid(t_wt, 2, 1)), AscB(Mid(t_wt, 3, 1)), AscB(Mid(t_wt, 4, 1)))
			Next
			For t_i = 16 To 79
				t_wa(t_i) = cShiftLeftW_(xorW_(xorW_(xorW_(t_wa(t_i - 3), t_wa(t_i - 8)), t_wa(t_i - 14)), t_wa(t_i - 16)), 1)
			Next
			t_waa = t_h0a
			t_wba = t_h1a
			t_wca = t_h2a
			t_wda = t_h3a
			t_wea = t_h4a
			For t_i = 0 To 79
				t_wfa = ff_(t_i, t_wba, t_wca, t_wda)
				t_ta = addW_(addW_(addW_(addW_(cShiftLeftW_(t_waa, 5), t_wfa), t_wea), t_wa(t_i)), t_vwa(t_i \ 20))
				t_wea = t_wda
				t_wda = t_wca
				t_wca = cShiftLeftW_(t_wba, 30)
				t_wba = t_waa
				t_waa = t_ta
			Next
			t_h0a = addW_(t_h0a, t_waa)
			t_h1a = addW_(t_h1a, t_wba)
			t_h2a = addW_(t_h2a, t_wca)
			t_h3a = addW_(t_h3a, t_wda)
			t_h4a = addW_(t_h4a, t_wea)
		Next
		SecureHash = wordToHex_(t_h0a) & wordToHex_(t_h1a) & wordToHex_(t_h2a) & wordToHex_(t_h3a) & wordToHex_(t_h4a)
	End Function
	
	'''使用SHA1加密算法加密处理字符串
	'p_s:要加密的字符串
	Public Default Function E(Byval p_s)
		E = LCase(SecureHash(p_s))
	End Function
End Class
%>