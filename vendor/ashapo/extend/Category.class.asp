<%
'''无限级分类管理类
Class Category

	'''分类的数据表名
	Private s_model

	'''格式化的字符
	Private s_icon

	'''字段映射，分类id，上级分类fid,分类名称name,格式化后分类名称fullname
	Private s_fields

	'''原始的分类数据
	Private s_rawList

	'''格式化后的分类
	Private s_formatList

	'''构造
	Private Sub Class_Initialize()
		Set s_fields = Sa.Dictionary
		s_fields("cid") = "cid"
		s_fields("fid") = "fid"
		s_fields("name") = "name"
		s_fields("fullname") = "fullname"

		s_icon = Array("&nbsp;&nbsp;│", "&nbsp;&nbsp;├ ", "&nbsp;&nbsp;└ ")
		s_formatList = Array()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''初始化
	Public Sub Init(Byval p_table, Byval p_fields)
		Set s_model = Sa.Model(p_table)
		Dim t_key
		For Each t_key In p_fields
			If s_fields.Exists(t_key) Then
				s_fields(t_key) = p_fields(t_key)
			End If
		Next
	End Sub

	'''获取分类信息数据
	Private Sub findAllCat_()
		s_rawList = s_model.Select()
	End Sub

	'''返回给定上级分类$fid的所有同一级子分类
	'p_fid:要查询的fid
	Private Function getChild_(Byval p_fid)
		Dim t_childs, t_row, t_i : t_i = 0
		ReDim t_childs(-1)
		For Each t_row In s_rawList
			If t_row(s_fields("fid")) = p_fid Then
				ReDim Preserve t_childs(t_i)
				Set t_childs(t_i) = t_row
				t_i = t_i + 1
			End If
		Next
		getChild_ = t_childs
	End Function

	'''递归格式化分类前的字符
	'p_cid:分类cid
	'p_space:前格式化字符串
	Private Sub searchList_(Byval p_cid, Byval p_space)
		Dim t_childs : t_childs = getChild_(p_cid)
		'Die(UBound(t_childs))
		Dim t_n : t_n = UBound(t_childs)
		If t_n < 0 Then
			Exit Sub
		End If
		Dim t_m, t_i : t_m = 1
		Dim t_pre, t_pad, t_sub
		For t_i = 0 To t_n
			t_pre = ""
			t_pad = ""
			If t_n = t_m Then
				t_pre = s_icon(2)
				t_pad = IIF(Has(p_space), "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "")
			Else
				t_pre = s_icon(1)
				t_pad = IIF(Has(p_space), s_icon(0), "")
			End If
			t_childs(t_i)(s_fields("fullname")) = IIF(Has(p_space), p_space & t_pre, "") & t_childs(t_i)(s_fields("name"))
			t_sub = UBound(s_formatList) + 1
			ReDim Preserve s_formatList(t_sub)
			Set s_formatList(t_sub) = t_childs(t_i)
			'''递归下一级分类
			Call searchList_(t_childs(t_i)(s_fields("cid")), p_space & t_pad & "&nbsp;&nbsp;")
			t_m = t_m + 1
		Next
	End Sub

	'''得到递归格式化分类
	Public Function getList()
		'''unset
		findAllCat_()
		Call searchList_(0, "")
		getList = s_formatList
	End Function
End Class
%>