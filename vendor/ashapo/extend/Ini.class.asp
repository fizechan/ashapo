<%
'''002
'''底层ini文件读写类
'''使用了configAshapo类的Sc实例
'''使用了fsoAshapo
'''本类用于语言包生成，仅包含框架所需功能，如果需要使用完整功能，请使用iniClass
Class Ini

	'内部使用fsoAshapo
	Private s_fso

	'要打开的ini文件地址,支持绝对路径和相对路径
	Private s_path
	
	'''构造
	Private Sub Class_Initialize()
		Set s_fso = New fsoAshapo
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_fso = Nothing
	End Sub
	
	'''设置ini的路径
	'p_path:LOG文件路径
	Public Default Function SetPath(Byval p_path)
		s_path = p_path
		Set SetPath = Me
	End Function
	
	'''从文本中获取p_start到p_end中间的内容
	'p_content:要读取的字符串
	'p_start:开始截取的字符串
	'p_end:结束截取的字符串
	Private Function separate_(Byval p_content, Byval p_start, Byval p_end)
		Dim t_b : t_b = InStr(1, p_content, p_start, 1)
		If t_b > 0 Then
			t_b = t_b + Len(p_start)
			Dim t_e : t_e = InStr(t_b, p_content, p_end, 1)
			If t_e = 0 Then
				t_e = InStr(t_b, p_content, vbCrLf, 1)
			End If
			If t_e = 0 Then
				t_e = Len(p_content) + 1
			End If
			separate_ = Mid(p_content, t_b, t_e - t_b)
		End If
	End Function
	
	'''获取INI键值
	'p_section:节点
	'p_key:键名
	'p_default:获取不到时的默认值
	Public Function [Get](Byval p_section, Byval p_key, Byval p_default)
		Dim t_icontent, t_sstart, t_send, t_scontent, t_value, t_t_found
		t_icontent = s_fso.ReadFile(s_path)
		'寻找节点
		t_sstart = InStr(1, t_icontent, "[" & p_section & "]", 1)
		If t_sstart>0 Then
			'找到节点，寻找节点结束点
			t_send = InStr(t_sstart, t_icontent, vbCrLf & "[")
			'最后一个节点的情况
			If t_send = 0 Then
				t_send = Len(t_icontent) + 1
			End If
			'节点下的所有键值对内容
			t_scontent = Mid(t_icontent, t_sstart, t_send - t_sstart)
			If InStr(1, t_scontent, vbCrLf & p_key & "=", 1)>0 Then
				t_t_found = True
				'获得值
				t_value = separate_(t_scontent, vbCrLf & p_key & "=", vbCrLf)
			End If
		End If
		If IsEmpty(t_t_found) Then
			t_value = p_default
		End If
		[Get] = t_value
	End Function
	
	'''获取该INI文件下的所有键名键值对
	Public Function GetAll(p_path)
		If p_path = "" Then
			p_path = s_path
		End If
		Dim t_fc : t_fc = s_fso.ReadFile(p_path)
		Dim t_fl : t_fl = Split(t_fc, vbcrlf)
		Dim t_c : t_c = ""
		Dim t_a, t_x
		Dim t_dn : t_dn = Sc.C("DICT_NAME")
		Dim t_dct : Set t_dct = Server.CreateObject(t_dn)
		For Each t_x In t_fl
			'检测节点
			If Has(t_x) And InStr(t_x, "[")<>1 Then
				t_a = Split(t_x, "=")
				t_dct(t_a(0)) = Replace(t_x, t_a(0)&"=", "")
			End If
		Next
		Set GetAll = t_dct
	End Function

	'''设置ini键值
	'p_section:节点
	'p_key:键名
	'p_value:键值
	Public Sub [Set](Byval p_section, Byval p_key, Byval p_value)
		Dim t_icontent, t_sstart, t_send
		t_icontent = s_fso.ReadFile(s_path)
		'寻找节点
		t_sstart = InStr(1, t_icontent, "[" & p_section & "]", 1)
		If t_sstart>0 Then
			'找到节点，寻找节点结束点
			t_send = InStr(t_sstart, t_icontent, vbCrLf & "[")
			'最后一个节点的情况
			If t_send = 0 Then
				t_send = Len(t_icontent) + 1
			End If
			'节点下的所有键值对内容
			Dim t_old, t_new, t_line
			Dim t_key, t_found
			t_old = Mid(t_icontent, t_sstart, t_send - t_sstart)
			t_old = Split(t_old, vbCrLf)
			t_key = LCase(p_key & "=")
			'改写每一行
			For Each t_line In t_old
				If LCase(Left(t_line, Len(t_key))) = t_key Then
					t_line = p_key & "=" & p_value
					t_found = True
				End If
				t_new = t_new & t_line & vbCrLf
			Next
			If IsEmpty(t_found) Then
				'如果没有找到键值则在后面追加
				t_new = t_new & p_key & "=" & p_value
			Else
				'处理最后一个vbCrLf
				t_new = Left(t_new, Len(t_new) - 2)
			End If
			'组装文本
			t_icontent = Left(t_icontent, t_sstart-1) & t_new & Mid(t_icontent, t_send)
		Else
			'如果没找到节点则直接在文本后面追加
			If Right(t_icontent, 2) <> vbCrLf And Len(t_icontent)>0 Then 
				t_icontent = t_icontent & vbCrLf 
			End If
			t_icontent = t_icontent & "[" & p_section & "]" & vbCrLf & p_key & "=" & p_value
		End If
		Call s_fso.CreateFile(s_path, t_icontent, False)
	End Sub
End Class
%>