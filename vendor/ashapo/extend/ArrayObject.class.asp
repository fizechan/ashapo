<%
'''底层数组类,
'''使用了configAshapo类的Sc实例
'扩展了asp数组
Class ArrayObject

	Private s_dict

	'''构造
	Private Sub Class_Initialize()
		Set s_dict = Server.CreateObject(Sc.C("DICT_NAME"))
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_dict = Nothing
	End Sub
	
	'''获取数组字典信息,默认使用
	'p_k:键名
	Public Default Property Get [Value](Byval p_k)
		If s_dict.Exists(p_k) Then
			[Value] = s_dict(p_k)
		Else
			[Value] = ""
		End If
	End Property
	
	'''设置数组字典
	'p_k:键名
	'p_v:键值
	Public Property Let [Value](Byval p_k, Byval p_v)
		s_dict(p_k) = p_v
	End Property
	
	'''返回数组原型
	Public Function [Array]()
		''[Array] = s_dict.Items
		Set [Array] = s_dict
	End Function
	
	'''返回所有键值
	Public Function [Keys]()
		[Keys] = s_dict.Keys
	End Function
	
	'''获取所有值
	Public Function Values()
		Values = s_dict.Items
	End Function
	
	'''遍历
	Public Sub [ForEach](Byref p_evt)
	End Sub
End Class
%>