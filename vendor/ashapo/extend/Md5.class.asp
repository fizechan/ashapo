<%
Class Md5
	'编码
	Public CharSet
	Private s_lOnBits(30)
	Private s_l2Power(30)
	
	Private Sub Class_Initialize()
		CharSet = "UTF-8"
	End Sub
	
	'''
	Private Function lShift_(Byval p_v, Byval p_b)
		If p_b = 0 Then
			lShift_ = p_v
			Exit Function
		ElseIf p_b = 31 Then
			If p_v And 1 Then
				lShift_ = &H80000000
			Else
				lShift_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 31 Then
			Errc.Raise(6)
		End If
		If (p_v And s_l2Power(31 - p_b)) Then
			lShift_ = ((p_v And s_lOnBits(31 - (p_b + 1))) * s_l2Power(p_b)) Or &H80000000
		Else
			lShift_ = ((p_v And s_lOnBits(31 - p_b)) * s_l2Power(p_b))
		End If
	End Function
	
	'''
	Private Function rShift_(Byval p_v, Byval p_b)
		If p_b = 0 Then
			rShift_ = p_v
			Exit Function
		ElseIf p_b = 31 Then
			If p_v And &H80000000 Then
				rShift_ = 1
			Else
				rShift_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 31 Then
			Errc.Raise(6)
		End If
		rShift_ = (p_v And &H7FFFFFFE) \ s_l2Power(p_b)
		If (p_v And &H80000000) Then
			rShift_ = (rShift_ Or (&H40000000 \ s_l2Power(p_b - 1)))
		End If
	End Function
	
	'''
	Private Function rotateLeft_(Byval p_v, Byval p_b)
		rotateLeft_ = lShift_(p_v, p_b) Or rShift_(p_v, (32 - p_b))
	End Function
	
	'''
	Private Function addUnsigned_(Byval p_x, Byval p_y)
		Dim t_x4, t_y4, t_x8, t_y8, t_s
		t_x8 = p_x And &H80000000
		t_y8 = p_y And &H80000000
		t_x4 = p_x And &H40000000
		t_y4 = p_y And &H40000000
		t_s = (p_x And &H3FFFFFFF) + (p_y And &H3FFFFFFF)
		If t_x4 And t_y4 Then
			t_s = t_s Xor &H80000000 Xor t_x8 Xor t_y8
		ElseIf t_x4 Or t_y4 Then
			If t_s And &H40000000 Then
				t_s = t_s Xor &HC0000000 Xor t_x8 Xor t_y8
			Else
				t_s = t_s Xor &H40000000 Xor t_x8 Xor t_y8
			End If
		Else
			t_s = t_s Xor t_x8 Xor t_y8
		End If
		addUnsigned_ = t_s
	End Function
	
	'''
	Private Function md5F_(Byval p_x, Byval p_y, Byval p_z)
		md5F_ = (p_x And p_y) Or ((Not p_x) And p_z)
	End Function
	
	'''
	Private Function md5G_(Byval p_x, Byval p_y, Byval p_z)
		md5G_ = (p_x And p_z) Or (p_y And (Not p_z))
	End Function
	
	'''
	Private Function md5H_(Byval p_x, Byval p_y, Byval p_z)
		md5H_ = (p_x Xor p_y Xor p_z)
	End Function
	
	'''
	Private Function md5I_(Byval p_x, Byval p_y, Byval p_z)
		md5I_ = (p_y Xor (p_x Or (Not p_z)))
	End Function
	
	'''
	Private Sub md5FF_(Byref p_a, Byval p_b, Byval p_c, Byval p_d, Byval p_x, Byval p_s, Byval p_t)
		p_a = addUnsigned_(p_a, addUnsigned_(addUnsigned_(md5F_(p_b, p_c, p_d), p_x), p_t))
		p_a = rotateLeft_(p_a, p_s)
		p_a = addUnsigned_(p_a, p_b)
	End Sub
	
	'''
	Private Sub md5GG_(Byref p_a, Byval p_b, Byval p_c, Byval p_d, Byval p_x, Byval p_s, Byval p_t)
		p_a = addUnsigned_(p_a, addUnsigned_(addUnsigned_(md5G_(p_b, p_c, p_d), p_x), p_t))
		p_a = rotateLeft_(p_a, p_s)
		p_a = addUnsigned_(p_a, p_b)
	End Sub
	
	'''
	Private Sub md5HH_(Byref p_a, Byval p_b, Byval p_c, Byval p_d, Byval p_x, Byval p_s, Byval p_t)
		p_a = addUnsigned_(p_a, addUnsigned_(addUnsigned_(md5H_(p_b, p_c, p_d), p_x), p_t))
		p_a = rotateLeft_(p_a, p_s)
		p_a = addUnsigned_(p_a, p_b)
	End Sub
	
	'''
	Private Sub md5II_(Byref p_a, Byval p_b, Byval p_c, Byval p_d, Byval p_x, Byval p_s, Byval p_t)
		p_a = addUnsigned_(p_a, addUnsigned_(addUnsigned_(md5I_(p_b, p_c, p_d), p_x), p_t))
		p_a = rotateLeft_(p_a, p_s)
		p_a = addUnsigned_(p_a, p_b)
	End Sub
	
	'''
	Private Function convert2Array_(Byval p_s)
		Dim t_ml,t_wn,t_wa(),t_bp,t_bc,t_wc
		Const B_BT2BY = 8
		Const B_BY2WD = 4
		Const B_BT2WD = 32
		Const B_MDB = 512
		Const B_CRB = 448
		t_ml = LenB(p_s)
		t_wn = (((t_ml + ((B_MDB - B_CRB) \ B_BT2BY)) \ (B_MDB \ B_BT2BY)) + 1) * (B_MDB \ B_BT2WD)
		ReDim t_wa(t_wn - 1)
		t_bp = 0
		t_bc = 0
		Do Until t_bc >= t_ml
			t_wc = t_bc \ B_BY2WD
			t_bp = (t_bc Mod B_BY2WD) * B_BT2BY
			t_wa(t_wc) = t_wa(t_wc) Or lShift_(AscB(MidB(p_s, t_bc + 1, 1)), t_bp)
			t_bc = t_bc + 1
		Loop
		t_wc = t_bc \ B_BY2WD
		t_bp = (t_bc Mod B_BY2WD) * B_BT2BY
		t_wa(t_wc) = t_wa(t_wc) Or lShift_(&H80, t_bp)
		t_wa(t_wn - 2) = lShift_(t_ml, 3)
		t_wa(t_wn - 1) = rShift_(t_ml, 29)
		convert2Array_ = t_wa
	End Function
	
	'''
	Private Function wordToHex_(Byval p_s)
		Const B_BT2BY = 8
		Dim t_b,t_c
		For t_c = 0 To 3
			t_b = rShift_(p_s, t_c * B_BT2BY) And s_lOnBits(B_BT2BY - 1)
			wordToHex_ = wordToHex_ & Right("0" & Hex(t_b), 2)
		Next
	End Function
	
	'''GBK编码转成二进制流
	'p_s:GBK编码字符串
	Private Function gbk2Bin_(p_s) 
		Dim t_c,t_i,t_r,t_w,t_h
		gbk2Bin_="" 
		For t_i=1 To Len(p_s) 
			t_r = Mid(p_s,t_i,1) 
			t_c = Asc(t_r)
			If t_c<0 Then 
				t_c = t_c + 65535 
			End If
			If t_c>255 Then 
				t_w = Left(Hex(Asc(t_r)),2) 
				t_h = Right(Hex(Asc(t_r)),2) 
				gbk2Bin_ = gbk2Bin_ & ChrB("&H" & t_w) & ChrB("&H" & t_h) 
			Else
				gbk2Bin_ = gbk2Bin_ & ChrB(AscB(t_r)) 
			End If
		Next
	End Function
	
	'''UTF-8编码转成二进制流
	'p_s:UTF-8编码字符串
	Private Function utf2Bin_(p_s)
		Dim t_r, t_c, t_a, t_j, t_i
		utf2Bin_ = ""
		For t_i=1 To Len(p_s)
			t_r = Mid(p_s,t_i,1)
			t_c = Server.UrlEncode(t_r)
			If t_c = "+" Then
				t_c = "%20"
			End If
			If Len(t_c) = 1 Then
				utf2Bin_ = utf2Bin_ & ChrB(AscB(t_c))
			Else
				t_a = Split(t_c,"%")
				For t_j = 1 to UBound(t_a)
					utf2Bin_ = utf2Bin_ & ChrB("&H" & t_a(t_j))
				Next
			End If
		Next
	End Function
	
	'''
	Private Function MD5(Byval p_s, Byval p_b)
		s_lOnBits(0) = CLng(1)
		s_lOnBits(1) = CLng(3)
		s_lOnBits(2) = CLng(7)
		s_lOnBits(3) = CLng(15)
		s_lOnBits(4) = CLng(31)
		s_lOnBits(5) = CLng(63)
		s_lOnBits(6) = CLng(127)
		s_lOnBits(7) = CLng(255)
		s_lOnBits(8) = CLng(511)
		s_lOnBits(9) = CLng(1023)
		s_lOnBits(10) = CLng(2047)
		s_lOnBits(11) = CLng(4095)
		s_lOnBits(12) = CLng(8191)
		s_lOnBits(13) = CLng(16383)
		s_lOnBits(14) = CLng(32767)
		s_lOnBits(15) = CLng(65535)
		s_lOnBits(16) = CLng(131071)
		s_lOnBits(17) = CLng(262143)
		s_lOnBits(18) = CLng(524287)
		s_lOnBits(19) = CLng(1048575)
		s_lOnBits(20) = CLng(2097151)
		s_lOnBits(21) = CLng(4194303)
		s_lOnBits(22) = CLng(8388607)
		s_lOnBits(23) = CLng(16777215)
		s_lOnBits(24) = CLng(33554431)
		s_lOnBits(25) = CLng(67108863)
		s_lOnBits(26) = CLng(134217727)
		s_lOnBits(27) = CLng(268435455)
		s_lOnBits(28) = CLng(536870911)
		s_lOnBits(29) = CLng(1073741823)
		s_lOnBits(30) = CLng(2147483647)
		'
		s_l2Power(0) = CLng(1)
		s_l2Power(1) = CLng(2)
		s_l2Power(2) = CLng(4)
		s_l2Power(3) = CLng(8)
		s_l2Power(4) = CLng(16)
		s_l2Power(5) = CLng(32)
		s_l2Power(6) = CLng(64)
		s_l2Power(7) = CLng(128)
		s_l2Power(8) = CLng(256)
		s_l2Power(9) = CLng(512)
		s_l2Power(10) = CLng(1024)
		s_l2Power(11) = CLng(2048)
		s_l2Power(12) = CLng(4096)
		s_l2Power(13) = CLng(8192)
		s_l2Power(14) = CLng(16384)
		s_l2Power(15) = CLng(32768)
		s_l2Power(16) = CLng(65536)
		s_l2Power(17) = CLng(131072)
		s_l2Power(18) = CLng(262144)
		s_l2Power(19) = CLng(524288)
		s_l2Power(20) = CLng(1048576)
		s_l2Power(21) = CLng(2097152)
		s_l2Power(22) = CLng(4194304)
		s_l2Power(23) = CLng(8388608)
		s_l2Power(24) = CLng(16777216)
		s_l2Power(25) = CLng(33554432)
		s_l2Power(26) = CLng(67108864)
		s_l2Power(27) = CLng(134217728)
		s_l2Power(28) = CLng(268435456)
		s_l2Power(29) = CLng(536870912)
		s_l2Power(30) = CLng(1073741824)
		'
		Dim t_x,t_k,t_aa,t_bb,t_cc,t_dd,t_a,t_b,t_c,t_d
		Const B_S11 = 7
		Const B_S12 = 12
		Const B_S13 = 17
		Const B_S14 = 22
		Const B_S21 = 5
		Const B_S22 = 9
		Const B_S23 = 14
		Const B_S24 = 20
		Const B_S31 = 4
		Const B_S32 = 11
		Const B_S33 = 16
		Const B_S34 = 23
		Const B_S41 = 6
		Const B_S42 = 10
		Const B_S43 = 15
		Const B_S44 = 21
		Select Case Lcase(CharSet)
		Case "utf-8"
			t_x = convert2Array_(utf2Bin_(p_s))
		Case "gbk","gb2312"
			t_x = convert2Array_(gbk2Bin_(p_s))
		Case Else
			t_x = convert2Array_(utf2Bin_(p_s))
		End Select
		t_a = &H67452301
		t_b = &HEFCDAB89
		t_c = &H98BADCFE
		t_d = &H10325476
		For t_k = 0 To UBound(t_x) Step 16
			t_aa = t_a
			t_bb = t_b
			t_cc = t_c
			t_dd = t_d
			'
			md5FF_ t_a, t_b, t_c, t_d, t_x(t_k + 0), B_S11, &HD76AA478
			md5FF_ t_d, t_a, t_b, t_c, t_x(t_k + 1), B_S12, &HE8C7B756
			md5FF_ t_c, t_d, t_a, t_b, t_x(t_k + 2), B_S13, &H242070DB
			md5FF_ t_b, t_c, t_d, t_a, t_x(t_k + 3), B_S14, &HC1BDCEEE
			md5FF_ t_a, t_b, t_c, t_d, t_x(t_k + 4), B_S11, &HF57C0FAF
			md5FF_ t_d, t_a, t_b, t_c, t_x(t_k + 5), B_S12, &H4787C62A
			md5FF_ t_c, t_d, t_a, t_b, t_x(t_k + 6), B_S13, &HA8304613
			md5FF_ t_b, t_c, t_d, t_a, t_x(t_k + 7), B_S14, &HFD469501
			md5FF_ t_a, t_b, t_c, t_d, t_x(t_k + 8), B_S11, &H698098D8
			md5FF_ t_d, t_a, t_b, t_c, t_x(t_k + 9), B_S12, &H8B44F7AF
			md5FF_ t_c, t_d, t_a, t_b, t_x(t_k + 10), B_S13, &HFFFF5BB1
			md5FF_ t_b, t_c, t_d, t_a, t_x(t_k + 11), B_S14, &H895CD7BE
			md5FF_ t_a, t_b, t_c, t_d, t_x(t_k + 12), B_S11, &H6B901122
			md5FF_ t_d, t_a, t_b, t_c, t_x(t_k + 13), B_S12, &HFD987193
			md5FF_ t_c, t_d, t_a, t_b, t_x(t_k + 14), B_S13, &HA679438E
			md5FF_ t_b, t_c, t_d, t_a, t_x(t_k + 15), B_S14, &H49B40821
			'
			md5GG_ t_a, t_b, t_c, t_d, t_x(t_k + 1), B_S21, &HF61E2562
			md5GG_ t_d, t_a, t_b, t_c, t_x(t_k + 6), B_S22, &HC040B340
			md5GG_ t_c, t_d, t_a, t_b, t_x(t_k + 11), B_S23, &H265E5A51
			md5GG_ t_b, t_c, t_d, t_a, t_x(t_k + 0), B_S24, &HE9B6C7AA
			md5GG_ t_a, t_b, t_c, t_d, t_x(t_k + 5), B_S21, &HD62F105D
			md5GG_ t_d, t_a, t_b, t_c, t_x(t_k + 10), B_S22, &H2441453
			md5GG_ t_c, t_d, t_a, t_b, t_x(t_k + 15), B_S23, &HD8A1E681
			md5GG_ t_b, t_c, t_d, t_a, t_x(t_k + 4), B_S24, &HE7D3FBC8
			md5GG_ t_a, t_b, t_c, t_d, t_x(t_k + 9), B_S21, &H21E1CDE6
			md5GG_ t_d, t_a, t_b, t_c, t_x(t_k + 14), B_S22, &HC33707D6
			md5GG_ t_c, t_d, t_a, t_b, t_x(t_k + 3), B_S23, &HF4D50D87
			md5GG_ t_b, t_c, t_d, t_a, t_x(t_k + 8), B_S24, &H455A14ED
			md5GG_ t_a, t_b, t_c, t_d, t_x(t_k + 13), B_S21, &HA9E3E905
			md5GG_ t_d, t_a, t_b, t_c, t_x(t_k + 2), B_S22, &HFCEFA3F8
			md5GG_ t_c, t_d, t_a, t_b, t_x(t_k + 7), B_S23, &H676F02D9
			md5GG_ t_b, t_c, t_d, t_a, t_x(t_k + 12), B_S24, &H8D2A4C8A
			'
			md5HH_ t_a, t_b, t_c, t_d, t_x(t_k + 5), B_S31, &HFFFA3942
			md5HH_ t_d, t_a, t_b, t_c, t_x(t_k + 8), B_S32, &H8771F681
			md5HH_ t_c, t_d, t_a, t_b, t_x(t_k + 11), B_S33, &H6D9D6122
			md5HH_ t_b, t_c, t_d, t_a, t_x(t_k + 14), B_S34, &HFDE5380C
			md5HH_ t_a, t_b, t_c, t_d, t_x(t_k + 1), B_S31, &HA4BEEA44
			md5HH_ t_d, t_a, t_b, t_c, t_x(t_k + 4), B_S32, &H4BDECFA9
			md5HH_ t_c, t_d, t_a, t_b, t_x(t_k + 7), B_S33, &HF6BB4B60
			md5HH_ t_b, t_c, t_d, t_a, t_x(t_k + 10), B_S34, &HBEBFBC70
			md5HH_ t_a, t_b, t_c, t_d, t_x(t_k + 13), B_S31, &H289B7EC6
			md5HH_ t_d, t_a, t_b, t_c, t_x(t_k + 0), B_S32, &HEAA127FA
			md5HH_ t_c, t_d, t_a, t_b, t_x(t_k + 3), B_S33, &HD4EF3085
			md5HH_ t_b, t_c, t_d, t_a, t_x(t_k + 6), B_S34, &H4881D05
			md5HH_ t_a, t_b, t_c, t_d, t_x(t_k + 9), B_S31, &HD9D4D039
			md5HH_ t_d, t_a, t_b, t_c, t_x(t_k + 12), B_S32, &HE6DB99E5
			md5HH_ t_c, t_d, t_a, t_b, t_x(t_k + 15), B_S33, &H1FA27CF8
			md5HH_ t_b, t_c, t_d, t_a, t_x(t_k + 2), B_S34, &HC4AC5665
			'
			md5II_ t_a, t_b, t_c, t_d, t_x(t_k + 0), B_S41, &HF4292244
			md5II_ t_d, t_a, t_b, t_c, t_x(t_k + 7), B_S42, &H432AFF97
			md5II_ t_c, t_d, t_a, t_b, t_x(t_k + 14), B_S43, &HAB9423A7
			md5II_ t_b, t_c, t_d, t_a, t_x(t_k + 5), B_S44, &HFC93A039
			md5II_ t_a, t_b, t_c, t_d, t_x(t_k + 12), B_S41, &H655B59C3
			md5II_ t_d, t_a, t_b, t_c, t_x(t_k + 3), B_S42, &H8F0CCC92
			md5II_ t_c, t_d, t_a, t_b, t_x(t_k + 10), B_S43, &HFFEFF47D
			md5II_ t_b, t_c, t_d, t_a, t_x(t_k + 1), B_S44, &H85845DD1
			md5II_ t_a, t_b, t_c, t_d, t_x(t_k + 8), B_S41, &H6FA87E4F
			md5II_ t_d, t_a, t_b, t_c, t_x(t_k + 15), B_S42, &HFE2CE6E0
			md5II_ t_c, t_d, t_a, t_b, t_x(t_k + 6), B_S43, &HA3014314
			md5II_ t_b, t_c, t_d, t_a, t_x(t_k + 13), B_S44, &H4E0811A1
			md5II_ t_a, t_b, t_c, t_d, t_x(t_k + 4), B_S41, &HF7537E82
			md5II_ t_d, t_a, t_b, t_c, t_x(t_k + 11), B_S42, &HBD3AF235
			md5II_ t_c, t_d, t_a, t_b, t_x(t_k + 2), B_S43, &H2AD7D2BB
			md5II_ t_b, t_c, t_d, t_a, t_x(t_k + 9), B_S44, &HEB86D391
			'
			t_a = addUnsigned_(t_a, t_aa)
			t_b = addUnsigned_(t_b, t_bb)
			t_c = addUnsigned_(t_c, t_cc)
			t_d = addUnsigned_(t_d, t_dd)
		Next
		If p_b = 16 Then
			MD5 = LCase(wordToHex_(t_b) & wordToHex_(t_c))
		Else
			MD5 = LCase(wordToHex_(t_a) & wordToHex_(t_b) & wordToHex_(t_c) & wordToHex_(t_d))
		End If
	End Function
	
	'''输出32位MD5值,默认函数
	Public Default Function To32(ByVal p_s)
		To32 = MD5(p_s,32)
	End Function
	
	'''输出16位MD5值
	Public Function To16(ByVal p_s)
		To16 = MD5(p_s,16)
	End Function
End Class
%>