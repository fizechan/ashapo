<%
Class Base64
	'ADODB.Stream/XMLDOM
	Private s_stream, s_xmldom

	'''构造
	Private Sub Class_Initialize()
		Set s_stream = Server.Createobject("ADODB.Stream")
		Set s_xmldom = Server.CreateObject("Msxml2.DOMDocument")
	End Sub

	'''析构
	Private Sub Class_Terminate()
		Set s_xmldom = Nothing
		Set s_stream = Nothing
	End Sub

	'''将gb2312字符串转化成Stream流
	'p_s:要转化的gb2312字符串
	Private Function gbk2Stream_(Byval p_s)
		With s_stream
			.Mode = 3
			.Type = 2
			.open
			.Charset = "gb2312"
			.WriteText p_s
			.position = 0
			.Type = 1
			gbk2Stream_ = .Read
			.Close
		End With
	End Function
 
	'''将Stream流转化成gb2312字符串
	'p_s:要转化的Stream流
	Private Function stream2Gbk_(Byval p_s)
		With s_stream
			.Mode = 3
			.Type = 1
			.open
			.Write p_s
			.position = 0
			.Type = 2
			.Charset = "gb2312"
			stream2Gbk_ = .ReadText
			.Close
		End With
	End Function
	
	'''使用Base64加密算法加密处理字符串
	'p_s:要加密的字符串
	Public Function Encode(Byval p_s)
		If p_s = "" Or IsNull(p_s) Then
			Encode = ""
			Exit Function
		End If
		Dim t_n
		s_xmldom.loadXML ("<root/>")
		Set t_n = s_xmldom.createElement("MyText")
		t_n.dataType = "bin.base64"
		t_n.nodeTypedValue = gbk2Stream_(p_s)
		Encode = t_n.Text
		s_xmldom.documentElement.appendChild(t_n)
	End Function

	'''使用Base64加密算法解密处理字符串
	'p_s:要解密的字符串
	Public Function Decode(Byval p_s)
		If p_s = "" Or IsNull(p_s) Then
			Decode = ""
			Exit Function
		End If
		Dim t_n
		s_xmldom.loadXML ("<root/>")
		Set t_n = s_xmldom.createElement("MyText")
		t_n.dataType = "bin.base64"
		t_n.Text = p_s
		Decode = stream2Gbk_(t_n.nodeTypedValue)
		s_xmldom.documentElement.appendChild(t_n)
	End Function
End Class
%>