<%
'''Aes������
Class Aes

	Private s_lOnBits(30), s_l2Power(30), s_bytOnBits(7), s_byt2Power(7)
	Private s_fbsub(255), s_rbsub(255), s_ptab(255), s_ltab(255), s_ftable(255), s_rtable(255)
	Private s_InCo(3), s_rco(29), s_nk, s_nb, s_nr, s_fi(23), s_ri(23), s_fkey(119), s_rkey(119), s_pass

	'''����
	Private Sub Class_Initialize()
		s_InCo(0) = &HB
		s_InCo(1) = &HD
		s_InCo(2) = &H9
		s_InCo(3) = &HE
		'
		s_bytOnBits(0) = 1
		s_bytOnBits(1) = 3
		s_bytOnBits(2) = 7
		s_bytOnBits(3) = 15
		s_bytOnBits(4) = 31
		s_bytOnBits(5) = 63
		s_bytOnBits(6) = 127
		s_bytOnBits(7) = 255
		'
		s_byt2Power(0) = 1
		s_byt2Power(1) = 2
		s_byt2Power(2) = 4
		s_byt2Power(3) = 8
		s_byt2Power(4) = 16
		s_byt2Power(5) = 32
		s_byt2Power(6) = 64
		s_byt2Power(7) = 128
		'
		s_lOnBits(0) = 1
		s_lOnBits(1) = 3
		s_lOnBits(2) = 7
		s_lOnBits(3) = 15
		s_lOnBits(4) = 31
		s_lOnBits(5) = 63
		s_lOnBits(6) = 127
		s_lOnBits(7) = 255
		s_lOnBits(8) = 511
		s_lOnBits(9) = 1023
		s_lOnBits(10) = 2047
		s_lOnBits(11) = 4095
		s_lOnBits(12) = 8191
		s_lOnBits(13) = 16383
		s_lOnBits(14) = 32767
		s_lOnBits(15) = 65535
		s_lOnBits(16) = 131071
		s_lOnBits(17) = 262143
		s_lOnBits(18) = 524287
		s_lOnBits(19) = 1048575
		s_lOnBits(20) = 2097151
		s_lOnBits(21) = 4194303
		s_lOnBits(22) = 8388607
		s_lOnBits(23) = 16777215
		s_lOnBits(24) = 33554431
		s_lOnBits(25) = 67108863
		s_lOnBits(26) = 134217727
		s_lOnBits(27) = 268435455
		s_lOnBits(28) = 536870911
		s_lOnBits(29) = 1073741823
		s_lOnBits(30) = 2147483647
		'
		s_l2Power(0) = 1
		s_l2Power(1) = 2
		s_l2Power(2) = 4
		s_l2Power(3) = 8
		s_l2Power(4) = 16
		s_l2Power(5) = 32
		s_l2Power(6) = 64
		s_l2Power(7) = 128
		s_l2Power(8) = 256
		s_l2Power(9) = 512
		s_l2Power(10) = 1024
		s_l2Power(11) = 2048
		s_l2Power(12) = 4096
		s_l2Power(13) = 8192
		s_l2Power(14) = 16384
		s_l2Power(15) = 32768
		s_l2Power(16) = 65536
		s_l2Power(17) = 131072
		s_l2Power(18) = 262144
		s_l2Power(19) = 524288
		s_l2Power(20) = 1048576
		s_l2Power(21) = 2097152
		s_l2Power(22) = 4194304
		s_l2Power(23) = 8388608
		s_l2Power(24) = 16777216
		s_l2Power(25) = 33554432
		s_l2Power(26) = 67108864
		s_l2Power(27) = 134217728
		s_l2Power(28) = 268435456
		s_l2Power(29) = 536870912
		s_l2Power(30) = 1073741824
		'
		s_pass = "AshapoAES"
	End Sub
	
	'''����
	Private Sub Class_Terminate()
	End Sub

	'''����AES���ܽ���ʱ����Կ
	Public Property Let Password(Byval p_p)
		s_pass = p_p
	End Property
	
	'''ʹ��AES���ܽ���ʱ����Կ
	Public Property Get Password()
		Password = s_pass
	End Property

	'''
	Private Function lShift_(ByVal p_v, Byval p_b)
		If p_b = 0 Then
			lShift_ = p_v
			Exit Function
		ElseIf p_b = 31 Then
			If p_v And 1 Then
				lShift_ = &H80000000
			Else
				lShift_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 31 Then
			'��������
			Errc.Raise(6)
		End If
		If (p_v And s_l2Power(31 - p_b)) Then
			lShift_ = ((p_v And s_lOnBits(31 - (p_b + 1))) * s_l2Power(p_b)) Or &H80000000
		Else
			lShift_ = ((p_v And s_lOnBits(31 - p_b)) * s_l2Power(p_b))
		End If
	End Function

	'''
	Private Function rShift_(Byval p_v, Byval p_b)
		If p_b = 0 Then
			rShift_ = p_v
			Exit Function
		ElseIf p_b = 31 Then
			If p_v And &H80000000 Then
				rShift_ = 1
			Else
				rShift_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 31 Then
			'��������
			Err.Raise(6)
		End If
		rShift_ = (p_v And &H7FFFFFFE) \ s_l2Power(p_b)
		If (p_v And &H80000000) Then
			rShift_ = (rShift_ Or (&H40000000 \ s_l2Power(p_b - 1)))
		End If
	End Function

	'''
	Private Function lShiftByte_(Byval p_v, Byval p_b)
		If p_b = 0 Then
			lShiftByte_ = p_v
			Exit Function
		ElseIf p_b = 7 Then
			If p_v And 1 Then
				lShiftByte_ = &H80
			Else
				lShiftByte_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 7 Then
			'��������
			Err.Raise(6)
		End If
		lShiftByte_ = ((p_v And s_bytOnBits(7 - p_b)) * s_byt2Power(p_b))
	End Function

	'''
	Private Function rShiftByte_(Byval p_v, Byval p_b)
		If p_b = 0 Then
			rShiftByte_ = p_v
			Exit Function
		ElseIf p_b = 7 Then
			If p_v And &H80 Then
				rShiftByte_ = 1
			Else
				rShiftByte_ = 0
			End If
			Exit Function
		ElseIf p_b < 0 Or p_b > 7 Then
			Err.Raise 6
		End If
		rShiftByte_ = p_v \ s_byt2Power(p_b)
	End Function

	'''
	Private Function rotateLeft_(Byval p_v, Byval p_b)
		rotateLeft_ = lShift_(p_v, p_b) Or rShift_(p_v, (32 - p_b))
	End Function

	'''
	Private Function rotateLeftByte_(Byval p_v, Byval p_b)
		rotateLeftByte_ = lShiftByte_(p_v, p_b) Or rShiftByte_(p_v, (8 - p_b))
	End Function

	'''
	Private Function pack_(Byval p_b())
		Dim t_c, t_t
		For t_c = 0 To 3
			t_t = p_b(t_c)
			pack_ = pack_ Or lShift_(t_t, (t_c * 8))
		Next
	End Function

	'''
	Private Function packFrom_(Byval p_b(), Byval p_k)
		Dim t_c, t_t
		For t_c = 0 To 3
			t_t = p_b(t_c + p_k)
			packFrom_ = packFrom_ Or lShift_(t_t, (t_c * 8))
		Next
	End Function

	'''
	Private Sub unPack_(Byval p_a, Byref p_b())
		p_b(0) = p_a And s_lOnBits(7)
		p_b(1) = rShift_(p_a, 8) And s_lOnBits(7)
		p_b(2) = rShift_(p_a, 16) And s_lOnBits(7)
		p_b(3) = rShift_(p_a, 24) And s_lOnBits(7)
	End Sub

	'''
	Private Sub unPackFrom_(Byval p_a, Byref p_b(), Byval p_k)
		p_b(0 + p_k) = p_a And s_lOnBits(7)
		p_b(1 + p_k) = rShift_(p_a, 8) And s_lOnBits(7)
		p_b(2 + p_k) = rShift_(p_a, 16) And s_lOnBits(7)
		p_b(3 + p_k) = rShift_(p_a, 24) And s_lOnBits(7)
	End Sub

	'''
	Private Function xtime_(Byval p_a)
		Dim t_b
		If (p_a And &H80) Then
			t_b = &H1B
		Else
			t_b = 0
		End If
		xtime_ = lShiftByte_(p_a, 1)
		xtime_ = xtime_ Xor t_b
	End Function

	'''
	Private Function bmul_(Byval p_x, Byval p_y)
		If p_x <> 0 And p_y <> 0 Then
			bmul_ = s_ptab((CLng(s_ltab(p_x)) + CLng(s_ltab(p_y))) Mod 255)
		Else
			bmul_ = 0
		End If
	End Function

	'''
	Private Function subByte_(Byval p_a)
		Dim t_b(3)
		unPack_ p_a, t_b
		t_b(0) = s_fbsub(t_b(0))
		t_b(1) = s_fbsub(t_b(1))
		t_b(2) = s_fbsub(t_b(2))
		t_b(3) = s_fbsub(t_b(3))
		subByte_ = pack_(t_b)
	End Function

	'''
	Private Function product_(Byval p_x, Byval p_y)
		Dim t_xb(3), t_yb(3)
		unPack_ p_x, t_xb
		unPack_ p_y, t_yb
		product_ = bmul_(t_xb(0), t_yb(0)) Xor bmul_(t_xb(1), t_yb(1)) Xor bmul_(t_xb(2), t_yb(2)) Xor bmul_(t_xb(3), t_yb(3))
	End Function

	'''
	Private Function invMixCol_(Byval p_x)
		Dim t_y, t_m, t_b(3)
		t_m = pack_(s_InCo)
		t_b(3) = product_(t_m, p_x)
		t_m = rotateLeft_(t_m, 24)
		t_b(2) = product_(t_m, p_x)
		t_m = rotateLeft_(t_m, 24)
		t_b(1) = product_(t_m, p_x)
		t_m = rotateLeft_(t_m, 24)
		t_b(0) = product_(t_m, p_x)
		t_y = pack_(t_b)
		invMixCol_ = t_y
	End Function

	'''
	Private Function byteSub_(Byval p_x)
		Dim t_y, t_z
		t_z = p_x
		t_y = s_ptab(255 - s_ltab(t_z))
		t_z = t_y
		t_z = rotateLeftByte_(t_z, 1)
		t_y = t_y Xor t_z
		t_z = rotateLeftByte_(t_z, 1)
		t_y = t_y Xor t_z
		t_z = rotateLeftByte_(t_z, 1)
		t_y = t_y Xor t_z
		t_z = rotateLeftByte_(t_z, 1)
		t_y = t_y Xor t_z
		t_y = t_y Xor &H63
		byteSub_ = t_y
	End Function

	'''
	Public Sub GenTables()
		Dim t_i, t_y, t_b(3), t_ib
		s_ltab(0) = 0
		s_ptab(0) = 1
		s_ltab(1) = 0
		s_ptab(1) = 3
		s_ltab(3) = 1
		For t_i = 2 To 255
			s_ptab(t_i) = s_ptab(t_i - 1) Xor xtime_(s_ptab(t_i - 1))
			s_ltab(s_ptab(t_i)) = t_i
		Next
		s_fbsub(0) = &H63
		s_rbsub(&H63) = 0
		For t_i = 1 To 255
			t_ib = t_i
			t_y = byteSub_(t_ib)
			s_fbsub(t_i) = t_y
			s_rbsub(t_y) = t_i
		Next
		t_y = 1
		For t_i = 0 To 29
			s_rco(t_i) = t_y
			t_y = xtime_(t_y)
		Next
		For t_i = 0 To 255
			t_y = s_fbsub(t_i)
			t_b(3) = t_y Xor xtime_(t_y)
			t_b(2) = t_y
			t_b(1) = t_y
			t_b(0) = xtime_(t_y)
			s_ftable(t_i) = pack_(t_b)
			t_y = s_rbsub(t_i)
			t_b(3) = bmul_(s_InCo(0), t_y)
			t_b(2) = bmul_(s_InCo(1), t_y)
			t_b(1) = bmul_(s_InCo(2), t_y)
			t_b(0) = bmul_(s_InCo(3), t_y)
			s_rtable(t_i) = pack_(t_b)
		Next
	End Sub

	'''
	Public Sub GKey(Byval p_b, Byval p_k, Byval p_e())
		Dim t_i, t_j, t_k, t_m, t_n, t_c1, t_c2, t_c3, t_ck(7)
		s_nb = p_b
		s_nk = p_k
		If s_nb >= s_nk Then
			s_nr = 6 + s_nb
		Else
			s_nr = 6 + s_nk
		End If
		t_c1 = 1
		If s_nb < 8 Then
			t_c2 = 2
			t_c3 = 3
		Else
			t_c2 = 3
			t_c3 = 4
		End If
		For t_j=0 To p_b-1
			t_m = t_j * 3
			s_fi(t_m) = (t_j + t_c1) Mod p_b
			s_fi(t_m + 1) = (t_j + t_c2) Mod p_b
			s_fi(t_m + 2) = (t_j + t_c3) Mod p_b
			s_ri(t_m) = (p_b + t_j - t_c1) Mod p_b
			s_ri(t_m + 1) = (p_b + t_j - t_c2) Mod p_b
			s_ri(t_m + 2) = (p_b + t_j - t_c3) Mod p_b
		Next
		t_n = s_nb * (s_nr + 1)
		For t_i = 0 To s_nk - 1
			t_j = t_i * 4
			t_ck(t_i) = packFrom_(p_e, t_j)
		Next
		For t_i = 0 To s_nk - 1
			s_fkey(t_i) = t_ck(t_i)
		Next
		t_j = s_nk
		t_k = 0
		Do While t_j < t_n
			s_fkey(t_j) = s_fkey(t_j - s_nk) Xor _
				subByte_(rotateLeft_(s_fkey(t_j - 1), 24)) Xor s_rco(t_k)
			If s_nk <= 6 Then
				t_i = 1
				Do While t_i < s_nk And (t_i + t_j) < t_n
					s_fkey(t_i + t_j) = s_fkey(t_i + t_j - s_nk) Xor s_fkey(t_i + t_j - 1)
					t_i = t_i + 1
				Loop
			Else
				t_i = 1
				Do While t_i < 4 And (t_i + t_j) < t_n
					s_fkey(t_i + t_j) = s_fkey(t_i + t_j - s_nk) Xor s_fkey(t_i + t_j - 1)
					t_i = t_i + 1
				Loop
				If t_j + 4 < t_n Then
					s_fkey(t_j + 4) = s_fkey(t_j + 4 - s_nk) Xor subByte_(s_fkey(t_j + 3))
				End If
				t_i = 5
				Do While t_i < s_nk And (t_i + t_j) < t_n
					s_fkey(t_i + t_j) = s_fkey(t_i + t_j - s_nk) Xor s_fkey(t_i + t_j - 1)
					t_i = t_i + 1
				Loop
			End If
			t_j = t_j + s_nk
			t_k = t_k + 1
		Loop
		For t_j = 0 To s_nb - 1
			s_rkey(t_j + t_n - p_b) = s_fkey(t_j)
		Next
		t_i = s_nb
		Do While t_i < t_n - s_nb
			t_k = t_n - s_nb - t_i
			For t_j = 0 To s_nb - 1
				s_rkey(t_k + t_j) = invMixCol_(s_fkey(t_i + t_j))
			Next
			t_i = t_i + s_nb
		Loop
		t_j = t_n - s_nb
		Do While t_j < t_n
			s_rkey(t_j - t_n + s_nb) = s_fkey(t_j)
			t_j = t_j + 1
		Loop
	End Sub

	'''
	Public Sub Encrypt(Byref p_b())
		Dim t_i, t_j, t_k, t_m, t_a(7), t_b(7), t_x, t_y, t_t
		For t_i = 0 To s_nb - 1
			t_j = t_i * 4
			t_a(t_i) = packFrom_(p_b, t_j)
			t_a(t_i) = t_a(t_i) Xor s_fkey(t_i)
		Next
		t_k = s_nb
		t_x = t_a
		t_y = t_b
		For t_i = 1 To s_nr - 1
			For t_j = 0 To s_nb - 1
				t_m = t_j * 3
				t_y(t_j) = s_fkey(t_k) Xor s_ftable(t_x(t_j) And s_lOnBits(7)) Xor _
					rotateLeft_(s_ftable(rShift_(t_x(s_fi(t_m)), 8) And s_lOnBits(7)), 8) Xor _
					rotateLeft_(s_ftable(rShift_(t_x(s_fi(t_m + 1)), 16) And s_lOnBits(7)), 16) Xor _
					rotateLeft_(s_ftable(rShift_(t_x(s_fi(t_m + 2)), 24) And s_lOnBits(7)), 24)
				t_k = t_k + 1
			Next
			t_t = t_x
			t_x = t_y
			t_y = t_t
		Next
		For t_j = 0 To s_nb - 1
			t_m = t_j * 3
			t_y(t_j) = s_fkey(t_k) Xor s_fbsub(t_x(t_j) And s_lOnBits(7)) Xor _
				rotateLeft_(s_fbsub(rShift_(t_x(s_fi(t_m)), 8) And s_lOnBits(7)), 8) Xor _
				rotateLeft_(s_fbsub(rShift_(t_x(s_fi(t_m + 1)), 16) And s_lOnBits(7)), 16) Xor _
				rotateLeft_(s_fbsub(rShift_(t_x(s_fi(t_m + 2)), 24) And s_lOnBits(7)), 24)
			t_k = t_k + 1
		Next
		For t_i = 0 To s_nb - 1
			t_j = t_i * 4
			unPackFrom_ t_y(t_i), p_b, t_j
			t_x(t_i) = 0
			t_y(t_i) = 0
		Next
	End Sub

	'''
	Public Sub Decrypt(Byref p_b())
		Dim t_i, t_j, t_k, t_m, t_a(7), t_b(7), t_x, t_y, t_t
		For t_i = 0 To s_nb - 1
			t_j = t_i * 4
			t_a(t_i) = packFrom_(p_b, t_j)
			t_a(t_i) = t_a(t_i) Xor s_rkey(t_i)
		Next
		t_k = s_nb
		t_x = t_a
		t_y = t_b
		For t_i = 1 To s_nr - 1
			For t_j = 0 To s_nb - 1
				t_m = t_j * 3
				t_y(t_j) = s_rkey(t_k) Xor s_rtable(t_x(t_j) And s_lOnBits(7)) Xor _
					rotateLeft_(s_rtable(rShift_(t_x(s_ri(t_m)), 8) And s_lOnBits(7)), 8) Xor _
					rotateLeft_(s_rtable(rShift_(t_x(s_ri(t_m + 1)), 16) And s_lOnBits(7)), 16) Xor _
					rotateLeft_(s_rtable(rShift_(t_x(s_ri(t_m + 2)), 24) And s_lOnBits(7)), 24)
				t_k = t_k + 1
			Next
			t_t = t_x
			t_x = t_y
			t_y = t_t
		Next
		For t_j = 0 To s_nb - 1
			t_m = t_j * 3
			t_y(t_j) = s_rkey(t_k) Xor s_rbsub(t_x(t_j) And s_lOnBits(7)) Xor _
				rotateLeft_(s_rbsub(rShift_(t_x(s_ri(t_m)), 8) And s_lOnBits(7)), 8) Xor _
				rotateLeft_(s_rbsub(rShift_(t_x(s_ri(t_m + 1)), 16) And s_lOnBits(7)), 16) Xor _
				rotateLeft_(s_rbsub(rShift_(t_x(s_ri(t_m + 2)), 24) And s_lOnBits(7)), 24)
			t_k = t_k + 1
		Next
		For t_i = 0 To s_nb - 1
			t_j = t_i * 4
			unPackFrom_ t_y(t_i), p_b, t_j
			t_x(t_i) = 0
			t_y(t_i) = 0
		Next
	End Sub

	'''
	Private Function isInitialized_(Byval p_a)
		'On Error Resume Next
		isInitialized_ = IsNumeric(UBound(p_a))
	End Function

	'''
	Private Sub copyBytesAsp_(Byref p_bd, Byval p_ds, Byval p_bs(), Byval p_ss, Byval p_ll)
		Dim t_c : t_c = 0
		Do
			p_bd(p_ds + t_c) = p_bs(p_ss + t_c)
			t_c = t_c + 1
		Loop Until t_c = p_ll
	End Sub

	'''
	Public Function EncryptData(Byval p_m, Byval p_p)
		Dim t_bk(31), t_in(), t_out(), t_bt(31), t_c, t_l, t_el
		If Not isInitialized_(p_m) Then
			Exit Function
		End If
		If Not isInitialized_(p_p) Then
			Exit Function
		End If
		For t_c = 0 To UBound(p_p)
			t_bk(t_c) = p_p(t_c)
			If t_c = 31 Then
				Exit For
			End If
		Next
		GenTables()
		GKey 8, 8, t_bk
		t_l = UBound(p_m) + 1
		t_el = t_l + 4
		If t_el Mod 32 <> 0 Then
			t_el = t_el + 32 - (t_el Mod 32)
		End If
		ReDim t_in(t_el - 1)
		ReDim t_out(t_el - 1)
		unPack_ t_l, t_in
		copyBytesAsp_ t_in, 4, p_m, 0, t_l
		For t_c = 0 To t_el - 1 Step 32
			copyBytesAsp_ t_bt, 0, t_in, t_c, 32
			Encrypt t_bt
			copyBytesAsp_ t_out, t_c, t_bt, 0, 32
		Next
		EncryptData = t_out
	End Function

	'''
	Public Function DecryptData(Byval p_i, Byval p_p)
		Dim t_m(), t_bk(31), t_bo(), t_bt(31), t_c, t_l, t_el
		If Not isInitialized_(p_i) Then
			Exit Function
		End If
		If Not isInitialized_(p_p) Then
			Exit Function
		End If
		t_el = UBound(p_i) + 1
		If t_el Mod 32 <> 0 Then
			Exit Function
		End If
		For t_c = 0 To UBound(p_p)
			t_bk(t_c) = p_p(t_c)
			If t_c = 31 Then
				Exit For
			End If
		Next
		GenTables()
		GKey 8, 8, t_bk
		ReDim t_bo(t_el - 1)
		For t_c = 0 To t_el - 1 Step 32
			copyBytesAsp_ t_bt, 0, p_i, t_c, 32
			Decrypt t_bt
			copyBytesAsp_ t_bo, t_c, t_bt, 0, 32
		Next
		t_l = pack_(t_bo)
		If t_l > t_el - 4 Then
			Exit Function
		End If
		ReDim t_m(t_l - 1)
		copyBytesAsp_ t_m, 0, t_bo, 4, t_l
		DecryptData = t_m
	End Function

	'''��Unicode������%���������ַ���
	'p_s:��������ַ���
	Private Function Escape(Byval p_s)
		If IsN(p_s) Then
			Escape = "" : Exit Function
		End If
		Dim t_i,t_c,t_a,t_s : t_s = ""
		For t_i = 1 To Len(p_s)
			t_c = Mid(p_s,t_i,1)
			t_a = ASCW(t_c)
			If (t_a>=48 And t_a<=57) Or (t_a>=65 And t_a<=90) Or (t_a>=97 And t_a<=122) Then
				t_s = t_s & t_c
			ElseIf InStr("@*_+-./",t_c)>0 Then
				t_s = t_s & t_c
			ElseIf t_a>0 And t_a<16 Then
				t_s = t_s & "%0" & Hex(t_a)
			ElseIf t_a>=16 And t_a<256 Then
				t_s = t_s & "%" & Hex(t_a)
			Else
				t_s = t_s & "%u" & Hex(t_a)
			End If
		Next
		Escape = t_s
	End Function

	'''��AES�㷨ʹ����ʱ��Կ����һ���ַ���
	'p_s:Ҫ���ܵ��ַ���
	'p_p:����ʱ����Կ
	Public Function AesEncrypt(Byval p_s, Byval p_p)
		If p_s = "" Then
			AesEncrypt = ""
			Exit Function
		End If
		p_s = Escape(p_s)
		p_p = Escape(p_p)
		Dim t_in(), t_out, t_pwd(), t_c, t_l, t_t
		t_l = Len(p_s)
		ReDim t_in(t_l-1)
		For t_c = 1 To t_l
			t_in(t_c-1) = CByte(AscB(Mid(p_s,t_c,1)))
		Next
		t_l = Len(p_p)
		ReDim t_pwd(t_l-1)
		For t_c = 1 To t_l
			t_pwd(t_c-1) = CByte(AscB(Mid(p_p,t_c,1)))
		Next
		t_out = EncryptData(t_in, t_pwd)
		t_t = ""
		For t_c = 0 To UBound(t_out)
			t_t = t_t & Right("0" & Hex(t_out(t_c)), 2)
		Next
		AesEncrypt = t_t
	End Function

	'''��AES�㷨ʹ����ʱ��Կ����һ���ַ���
	'p_s:Ҫ���ܵ��ַ���
	'p_p:����ʱ����Կ
	Public Function AesDecrypt(Byval p_s, Byval p_p)
		If p_s = "" Then
			AesDecrypt = ""
			Exit Function
		End If
		p_p = Escape(p_p)
		Dim t_in(), t_out, t_pwd(), t_c, t_l, t_t
		t_l = Len(p_s)
		ReDim t_in(t_l/2-1)
		For t_c = 0 To t_l/2-1
			t_in(t_c) = CByte("&H" & Mid(p_s,t_c*2+1,2))
		Next
		t_l = Len(p_p)
		ReDim t_pwd(t_l-1)
		For t_c = 1 To t_l
			t_pwd(t_c-1) = CByte(AscB(Mid(p_p,t_c,1)))
		Next
		t_out = DecryptData(t_in, t_pwd)
		t_l = UBound(t_out) + 1
		t_t = ""
		For t_c = 0 To t_l - 1
			t_t = t_t & Chr(t_out(t_c))
		Next
		AesDecrypt = UnEscape(t_t)
	End Function
	
	'''��AES�㷨����һ���ַ���
	'p_s:Ҫ���ܵ��ַ���
	Public Function Encode(Byval p_s)
		Encode = AesEncrypt(p_s,s_pass)
	End Function
	
	'��AES�㷨����һ���ַ���
	'p_s:Ҫ���ܵ��ַ���
	Public Function Decode(Byval p_s)
		Decode = AesDecrypt(p_s,s_pass)
	End Function
End Class
%>