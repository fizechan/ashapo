<%
Sa.Config("VAR_AJAX_SUBMIT") = "ajax" ''默认的AJAX提交判断变量


Sa.Config("RUN_OBJECT_NAME") = "ashapo_controller_now" ''最终执行定义的控制器类对象名
Sa.Config("LOAD_EXT_CONFIG") = "" ''加载扩展配置文件。自动加载额外的自定义配置文件，并且配置格式和项目配置一样。
Sa.Config("LOAD_EXT_FILE") = "" ''主要使用在额外加载的扩展业务函数

Sa.Config("LOGER_DRIVER") = "File" ''日志引擎

Sa.Config("DEFAULT_THEME") = "" ''默认模板主题名称,为空表示不启用主题
Sa.Config("TMPL_TEMPLATE_SUFFIX") = ".html" ''默认模板文件后缀
Set Sa.Config("TMPL_PARSE_STRING") = Sa.Dictionary ''解析后的模板文件结果要替换的字符串
Sa.Config("TMPL_ENGINE_DRIVER") = "Ashapo" ''默认模板引擎。关键字asp使用原生asp语法,性能最佳;使用第三方引擎类时,必须指定完整路径,文件名必须符合[类名.class.asp]规则，并且实现Resolve(p_tpl)方法,其中p_tpl参数为指定的模板完整路径,并应根据情况输出(CacheMap)ViewInclude的map情况文件以便于在DEBUG状态下检查编译缓存

Sa.Config("TMPL_ACTION_ERROR") = ASHAPO_CORE_PATH & "/Tpl/error.tpl" ''默认错误跳转对应的模板文件


Sa.Config("DB_TYPE") = ""
Sa.Config("DB_HOST") = ""
Sa.Config("DB_PORT") = ""
Sa.Config("DB_NAME") = ""
Sa.Config("DB_USER") = ""
Sa.Config("DB_PWD") = ""
Sa.Config("DB_PREFIX") = ""



Sa.Config("FITER_DISABLE_HTML") = "_html_" ''禁止页面HTML静态化的的GET参数标识符

Sa.Config("CACHE_DRIVER") = "File" ''数据缓存驱动,目前支持:File|Application
Sa.Config("CACHE_TIME") = 0 ''数据缓存有效期 0表示永久缓存
Sa.Config("CACHE_PREFIX") = "" ''缓存前缀

Sa.Config("COOKIE_EXPIRES") = 0 ''Cookie有效期，为0表示永久有效
Sa.Config("COOKIE_DOMAIN") = "" ''Cookie有效域名
Sa.Config("COOKIE_PATH") = "/" ''Cookie路径
Sa.Config("COOKIE_PREFIX") = "" ''Cookie前缀,避免冲突
Sa.Config("COOKIE_SECURE") = False ''是否只允许HTTPS
Sa.Config("COOKIE_HTTPONLY") = False ''是否不允许JS读取Cookie

'''todo Aes存在问题，暂时不使用
Sa.Config("COOKIE_AES_ENCODE") = False ''是否对Cookie进行加密
Sa.Config("COOKIE_AES_KEY") = "" ''Cookie的AES加解密密钥
%>