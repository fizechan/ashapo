<%
'''以Application方式实现的缓存驱动类
Class AshapoCacheDriverApplication

	'''缓存标识前缀
	Private s_prefix
	
	'''缓存有效期（单位为秒）,为0表示永久有效
	Private s_expire

	'''内部使用AshapoJson对象
	Private s_json
	
	'''构造
	Private Sub Class_Initialize()
		Call Runtime.Asp.Use( ASHAPO_CORE_PATH & "/core/AshapoJson" )
		Set s_json = New AshapoJson
		s_json.UnescapedUnicode = True
		s_json.UnescapedSlashes = True
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_json = Nothing
	End Sub
	
	'''初始化
	'p_expire:缓存有效期（单位为秒）,为0表示永久有效
	'p_prefix:缓存标识前缀
	Public Sub Init(Byval p_expire, Byval p_prefix)
		s_expire = p_expire
		s_prefix = p_prefix
		Dim t_x
		For Each t_x In Application.Contents
			If InStr(t_x, "ashapo_" & s_prefix) = 1 Then
				If Has(Application.Contents(t_x)) Then
					Dim t_json
					Call assign( t_json, s_json.Decode( Application.Contents(t_x) ) )
					If TypeName(t_json) = "Dictionary" And t_json.Exists("expire") And Has(t_json("expire")) Then
						If DateDiff("s", Now(), Runtime.Datetime.ToTimeObject(t_json("expire"))) < 0 Then
							Application.Contents.Remove(t_x)
						End If
					End If
				End If
			End If
		Next
	End Sub
	
	'''设置缓存过期时间
	'p_expire:有效时间（单位为秒）
	Public Property Let Expire(Byval p_expire)
		s_expire = p_expire
	End Property
	
	'''设置缓存标识前缀
	'p_prefix:缓存标识前缀
	Public Property Let Prefix(Byval p_prefix)
		s_prefix = p_prefix
	End Property
	
	'''存储缓存
	Public Property Let Value(Byval p_key, Byval p_value)
		Dim t_item : Set t_item = Server.CreateObject( Runtime.ProgID("Scripting.Dictionary") )
		
		'''assign是无法对数组和字典进行赋值的
		If IsObject(p_value) Then
			Set t_item("value") = p_value
		Else
			t_item("value") = p_value
		End If
		
		
		'Call assign( t_item("value"), p_value )
		
		If s_expire > 0 Then
			t_item("expire") = Runtime.Datetime.ToTimeStamp( DateAdd("s", s_expire, Now()))
		End If
		Dim t_json : t_json = s_json.Encode(t_item)
		Application.Lock()
		Application( "ashapo_" & s_prefix & p_key ) = t_json
		Application.Unlock()
	End Property
	
	'''取回缓存,默认
	Public Default Property Get Value(Byval p_key)
		Dim t_full_key : t_full_key = "ashapo_" & s_prefix & p_key
		If IsN( Application(t_full_key) ) Then
			Value  = vbNull
			Exit Property
		End If

		Dim t_json : Call assign( t_json, s_json.Decode( Application(t_full_key) ) )
		
		If TypeName(t_json) = "Dictionary" And t_json.Exists("value") Then
			If t_json.Exists("expire") And Has(t_json("expire")) Then
				If DateDiff("s", Now(), Runtime.Datetime.ToTimeObject(t_json("expire"))) < 0 Then
					Application.Contents.Remove(t_full_key)
					Value  = Null
					Exit Property
				End If
			End If
			If IsObject(t_json("value")) Then
				Set Value = t_json("value")
			Else
				Value = t_json("value")
			End If
			'Call assign( Value, t_json("value") )
		Else
			Value = Null
		End If
	End Property
	
	'''删除指定缓存
	Public Sub [Remove](Byval p_key)
		Application.Contents.Remove("ashapo_" & s_prefix & p_key)
	End Sub
	
	'''清理缓存
	Public Sub [RemoveAll]()
		Dim t_x
		For Each t_x In Application.Contents
			If InStr(t_x, "ashapo_" & s_prefix) = 1 Then
				Application.Contents.Remove(t_x)
			End If
		Next
	End Sub
End Class
%>