<%
'''以文件方式实现的缓存驱动类
'''文件缓存类
Class AshapoCacheDriverFile
	
	'''缓存标识前缀
	Private s_prefix
	
	'''缓存有效期（单位为秒）,为0表示永久有效
	Private s_expire
	
	'''内部使用AshapoJson对象
	Private s_json
	
	'''缓存文件夹路径
	Private s_dir
	
	'''构造
	Private Sub Class_Initialize()
		s_dir = ASHAPO_RUNTIME_PATH & "/cache/data/"
		''AshapoJson类时已载入的
		Set s_json = New AshapoJson
		s_json.UnescapedUnicode = True
		s_json.UnescapedSlashes = True
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_json = Nothing
	End Sub
	
	'''初始化
	'p_expire:缓存有效期（单位为秒）,为0表示永久有效
	'p_prefix:缓存标识前缀
	'p_length:限制缓存的数量
	Public Sub Init(Byval p_expire, Byval p_prefix)
		s_expire = p_expire
		s_prefix = p_prefix
		
		If Runtime.Fso.IsFolder(s_dir) Then
		
			Dim t_cache, t_caches : t_caches = Runtime.Fso.List( s_dir, "file" )

		End If
		
	End Sub
	
	'''设置缓存过期时间
	'p_expire:有效时间（单位为秒）
	Public Property Let Expire(Byval p_expire)
		s_expire = p_expire
	End Property
	
	'''设置缓存过期时间
	'p_expire:有效时间（单位为秒）
	Public Property Get Expire()
		Expire = s_expire
	End Property
	
	'''设置缓存标识前缀
	'p_prefix:缓存标识前缀
	Public Property Let Prefix(Byval p_prefix)
		s_prefix = p_prefix
	End Property
	
	'''存储缓存
	'p_key:键值，可以使用/来表示缓存文件夹层级
	'p_value:要储存的缓存值
	Public Property Let Value(Byval p_key, Byval p_value)		
		Dim t_item : Set t_item = Server.CreateObject( Runtime.ProgID("Scripting.Dictionary") )
		
		'''assign是无法对数组和字典进行赋值的
		If IsObject(p_value) Then
			Set t_item("value") = p_value
		Else
			t_item("value") = p_value
		End If
		
		
		'Call assign( t_item("value"), p_value )
		
		If s_expire > 0 Then
			t_item("expire") = Runtime.Datetime.ToTimeStamp( DateAdd("s", s_expire, Now()))
		End If
		Dim t_json : t_json = s_json.Encode(t_item)
		Call Runtime.Fso.CreateFile(s_dir & s_prefix & p_key, t_json)
	End Property
	
	'''取回缓存,默认
	Public Default Property Get Value(Byval p_key)
		Dim t_full_path : t_full_path = s_dir & s_prefix & p_key
		If Not Runtime.Fso.IsFile(t_full_path) Then
			Value  = Null
			Exit Property
		End If
		Dim t_json : Call assign( t_json, s_json.Decode( Runtime.Fso.ReadFile(t_full_path) ) )
		
		If TypeName(t_json) = "Dictionary" And t_json.Exists("value") Then
			If t_json.Exists("expire") And Has(t_json("expire")) Then
				If DateDiff("s", Now(), Runtime.Datetime.ToTimeObject(t_json("expire"))) < 0 Then
					Runtime.Fso.DeleteFile(t_full_path)
					Value  = Null
					Exit Property
				End If
			End If

			'Response.Write(VarType(t_json("value")) & "<br/>")
			
			If IsObject(t_json("value")) Then
				Set Value = t_json("value")
			Else
				Value = t_json("value")
			End If
			
			
			'Call assign( Value, t_json("value") )
		Else
			Value = Null
		End If
	End Property
	
	'''删除指定缓存
	Public Sub [Remove](Byval p_key)
		Dim t_full_path : t_full_path = s_dir & s_prefix & p_key
		Runtime.Fso.DeleteFile(t_full_path)
	End Sub
	
	'''清理缓存
	'todo
	Public Sub [RemoveAll]()
	End Sub
	
End Class
%>