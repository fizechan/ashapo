<%
'''以文件方式实现的底层日志类
Class AshapoLogerDriverFile
	'日志文件路径
	Private s_path
	
	'''仅单次有效的PATH
	Private s_pathOnce
	
	'''构造
	Private Sub Class_Initialize()
		s_path =""
		s_pathOnce = ""
	End Sub

	'''析构
	Private Sub Class_Terminate()
	End Sub
	
	'''设置输出的路径。如果是文件夹路径则自动建立以时间为基准的日志文件,否则按指定路径输出
	'p_p:文件或文件夹路径
	Public Property Let Path(Byval p_p)
	     s_path = p_p
	End Property
	
	'''获取日志输出的路径,写入业务日志时会用到
	Public Property Get Path()
		Path = s_path
	End Property
	
	'''设置单次输出路径,使用一次后路径自动改回原本的路径
	Public Function PathOnce(Byval p_p)
		s_pathOnce = p_p
	End Function

	'''格式化当前时间日期格式
	Private Function formatDt_()
		Dim t_y : t_y = Year(Now())
		Dim t_m : t_m = Right("0" & Month(Now()), 2)
		Dim t_d : t_d = Right("0" & Day(Now()), 2)
		formatDt_ = t_y & t_m & t_d
	End Function
	
	'''(更新/创建日志文件)
	'p_p:日志路径,如果是文件夹路径则自动建立以时间为基准的日志文件,否则按指定路径输出
	'p_s:单行日志内容
	Private Sub createAfile_(Byval p_p, Byval p_s)
		p_p  = Replace(p_p,"\","/")
		If InStrRev(p_p,".") < InStrRev(p_p,"/") Then
			p_p = p_p & "/" & formatDt_() & ".log"
		End If
		p_p  = Replace(p_p,"//","/")
		If Runtime.Fso.IsFile(p_p) Then
			Call Runtime.Fso.AppendFile(p_p, p_s & vbcrlf)
		Else
			Call Runtime.Fso.CreateFile(p_p, p_s & vbcrlf)
		End If
	End Sub
	
	'写入日志。总入口，其余扩展请基于本函数实现
	'p_s:日志实体,不含前缀格式化
	Public Sub [Write](Byval p_s)
		If s_pathOnce = "" Then
			Call createAfile_(s_path, p_s)
		Else
			Call createAfile_(s_pathOnce, p_s)
			s_pathOnce = ""
		End If
	End Sub
End Class
%>