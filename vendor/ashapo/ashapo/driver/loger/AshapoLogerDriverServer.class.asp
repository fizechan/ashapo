﻿<%
'''底层日志驱动类，使用Response.AppendToLog写入系统日志
Class AshapoLogerDriverServer
	
	'''构造
	Private Sub Class_Initialize()
	End Sub

	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''设置输出的路径。如果是文件夹路径则自动建立以时间为基准的日志文件,否则按指定路径输出
	'p_p:文件或文件夹路径
	Public Property Let Path(Byval p_p)
		''nothing
	End Property
	
	'''获取日志输出的路径,写入业务日志时会用到
	Public Property Get Path()
		Path = ""
	End Property
	
	'''设置单次输出路径,使用一次后路径自动改回原本的路径
	Public Function PathOnce(Byval p_p)
		''nothing
	End Function
	
	'写入日志。
	'p_str:日志实体,不含前缀格式化
	Public Sub [Write](Byval p_str)
		Response.AppendToLog(p_str)
	End Sub
End Class
%>