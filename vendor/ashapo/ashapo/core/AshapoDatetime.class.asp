<%
'''依赖文件:无
'''底层时间处理类
Class AshapoDatetime

	'''时区
	Public TimeZone


	'''构造
	Private Sub Class_Initialize()
		TimeZone = "+8"
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub


	'''把标准时间转换为UNIX时间戳
	'''@param: string str_time 要转换的时间  
	'''@return: int strTime相对于1970年1月1日午夜0点经过的秒数    
	Public Function ToTimeStamp(Byval str_time)        
	    If IsEmpty(str_time) Or Not IsDate(str_time) Then
			str_time = Now()
		End If
		ToTimeStamp = DateAdd("h", -TimeZone, str_time)      
		ToTimeStamp = DateDiff("s", "1970-1-1 0:0:0", ToTimeStamp)        
	End Function       
	       
	'''把UNIX时间戳转换为标准时间
	'''@todo 待移除    
	'''@param: int int_time 要转换的UNIX时间戳
	'''@return: string intTime所代表的标准时间    
	Public Function ToTimeObject(Byval int_time)        
	    If IsEmpty(int_time) Or Not IsNumeric(int_time) Then       
			ToTimeObject = Now()        
			Exit Function       
	    End If
		ToTimeObject = DateAdd("s", int_time, "1970-1-1 0:0:0")        
		ToTimeObject = DateAdd("h", TimeZone, ToTimeObject)
	End Function

End Class
%>