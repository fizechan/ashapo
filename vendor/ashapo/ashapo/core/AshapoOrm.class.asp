<%
'''数据库ORM模型类
'''注意如果使用了相关驱动类应记得加载
Class AshapoOrm

	'''通过驱动对外暴露ORM接口
	'p_dirver:驱动名
	'p_host:主机名，针对access则是文件路径
	'p_port:端口号
	'p_name:指定数据库名
	'p_user:用户名
	'p_pwd:用户密码，针对access则是文件打开密码
	Public Function Init(Byval p_dirver, Byval p_host, Byval p_port, Byval p_name, Byval p_user, Byval p_pwd)
		Runtime.Asp.Use(ASHAPO_CORE_PATH & "/core/AshapoDatabase")
		Select Case p_dirver
		Case "Access"
			Set Init = access_(p_host, p_pwd)
		Case "Access2007"
			Set Init = access2007_(p_host, p_pwd)
		Case "Mssql"
			Set Init = mssql_(p_host, p_port, p_name, p_user, p_pwd)
		Case "Mysql"
			Set Init = mysql_(p_host, p_port, p_name, p_user, p_pwd)
		Case "Oracle"
			Set Init = oracle_(p_host, p_user, p_pwd)
		End Select
	End Function

	'''获取一个Access模型
	'p_path:Access文件路径
	'p_pwd:如果文件需要密码则填写，否则制空
	Private Function access_(Byval p_path, Byval p_pwd)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/orm/AshapoOrmDriverAccess")
		Set access_ = New AshapoOrmDriverAccess
		Call access_.Init(p_path, p_pwd)
	End Function
	
	'''获取一个Access(2007以上版本)模型
	'p_path:Access文件路径
	'p_pwd:如果文件需要密码则填写，否则制空
	Private Function access2007_(Byval p_path, Byval p_pwd)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/orm/AshapoOrmDriverAccess2007")
		Set access2007_ = New AshapoOrmDriverAccess2007
		Call access2007_.Init(p_path, p_pwd)
	End Function
	
	'''获取一个MSSQL模型
	'p_server:MSSQL服务器
	'p_port:MSSQL端口
	'p_database:指定数据库
	'p_user:账户
	'p_pwd:密码
	Private Function mssql_(Byval p_server, Byval p_port, Byval p_database, Byval p_user, Byval p_pwd)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/orm/AshapoOrmDriverMssql")
		Set mssql_ = New AshapoOrmDriverMssql
		Call mssql_.Init(p_server, p_port, p_database, p_user, p_pwd)
	End Function
	
	'''获取一个MySQL模型
	'p_server:MySQL服务器
	'p_port:MySQL端口
	'p_database:指定数据库
	'p_user:账户
	'p_pwd:密码
	Private Function mysql_(Byval p_server, Byval p_port, Byval p_database, Byval p_user, Byval p_pwd)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/orm/AshapoOrmDriverMysql")
		Set mysql_ = New AshapoOrmDriverMysql
		Call mysql_.Init(p_server, p_port, p_database, p_user, p_pwd)
	End Function
	
	'''获取一个Oracle模型
	'p_server:Oracle服务器
	'p_user:账户
	'p_pwd:密码
	Private Function oracle_(Byval p_server, Byval p_user, Byval p_pwd)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/orm/AshapoOrmDriverOracle")
		Set oracle_ = New AshapoOrmDriverOracle
		Call oracle_.Init(p_server, p_user, p_pwd)
	End Function

End Class
%>