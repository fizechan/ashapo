<%
'''依赖层级:004
'''依赖文件:functions.asp, AshapoConfig.class.asp
'''底层HTTP类,用于HTML实时静态化
Class AshapoHttp

	'指定编码
	Private s_charset
	
	'解析超时时间,连接超时时间,发送数据超时时间,接受数据超时时间
	Private s_ResolveTimeout, s_ConnectTimeout, s_SendTimeout, s_ReceiveTimeout
	
	'''构造
	Private Sub Class_Initialize()
		s_url = ""
		s_charset = "GBK"
		
		s_html = ""
		s_data = ""
		s_body = Empty
		
		s_ResolveTimeout = 2000
		s_ConnectTimeout = 2000
		s_SendTimeout = 3000
		s_ReceiveTimeout = 6000
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''二进制流转字符串
	'p_s:二进制流
	'p_c:编码
	Private Function bytes2Bstr_(Byval p_s, Byval p_c) 
		Dim t_s : Set t_s = Server.CreateObject( Global.ProgID("Adodb.Stream") )
		With t_s
			.Type = 1
			.Mode =3
			.Open()
			.Write(p_s)
			.Position = 0
			.Type = 2
			.Charset = p_c
			bytes2Bstr_ = .ReadText()
			.Close()
		End With
		Set t_s = Nothing
	End Function

	'''设置HTTP对象
	Private Function httpObj_()
		If IsInstall( Global.ProgID("MSXML2.serverXMLHTTP") ) Then
			Set httpObj_ = Server.CreateObject( Global.ProgID("MSXML2.serverXMLHTTP") )
		ElseIf IsInstall( Global.ProgID("MSXML2.XMLHTTP") ) Then
			Set httpObj_ = Server.CreateObject( Global.ProgID("MSXML2.XMLHTTP") )
		ElseIf IsInstall( Global.ProgID("Microsoft.XMLHTTP") ) Then
			Set httpObj_ = Server.CreateObject( Global.ProgID("Microsoft.XMLHTTP") )
		Else
			Set httpObj_ = Nothing
			'抛出错误
			Die("CORE ERROR 101:cannot create the HTTP object")
		End If
	End Function
	
	'''格式化链接
	'p_i:本地运行asp文件地址(可含GET参数)
	Private Function formatUrl_(Byval p_i)
		If InStr(p_i, "http") = 1 Then
			formatUrl_ = p_i
			Exit Function
		End If
		If Lcase(Request.ServerVariables("HTTPS"))="on" Then
			formatUrl_ = "https://"
		Else
			formatUrl_ = "http://" 
		End If
		formatUrl_ = formatUrl_ & Request.ServerVariables("HTTP_HOST") & p_i
	End Function
	
	'''用参数配置的方式获取本地运行文件数据
	'p_i:本地运行asp文件地址(可含GET参数)
	Public Function [Get](Byval p_i)
		On Error Resume Next
		Dim t_o : Set t_o = httpObj_()
		Call t_o.SetTimeOuts( s_ResolveTimeout, s_ConnectTimeout, s_SendTimeout, s_ReceiveTimeout )
		p_i = formatUrl_(p_i)
		Call t_o.Open( "GET", p_i, False)
		t_o.Send()
		If t_o.ReadyState <> 4 Then
			[Get] = ""
			'出错
			Die("CORE ERROR 102:""" & p_i & """ HTTP ReadyState ERROR:" & t_o.ReadyState)
		ElseIf t_o.Status = 200 Then
			[Get] = bytes2Bstr_(t_o.ResponseBody, s_charset)
		Else
			[Get] = ""
			'出错
			Die("CORE ERROR 103:""" & p_i & """ HTTP Status ERROR:" & t_o.Status & " " & t_o.StatusText)
		End If
		Set t_o = Nothing
		If Err.Number<>0 Then
			Die("CORE ERROR 101:cannot create the HTTP object")
		End If
		Err.Clear()
	End Function
End Class
%>