<%
'''ASP功能扩展类
'''使用了Global全局对象
Class AshapoAsp

	'''最终执行代码的文件名(不含后缀)
	Private s_cacheFileName
	
	'''加载收集器
	Private s_collector

	'''构造
	Private Sub Class_Initialize()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_collector = Nothing
	End Sub

	'''初始化
	'''@todo 系统自动调用该方法初始化，我们并不希望外部手动调用该方法
	'''p_dict_name: 组件Scripting.Dictionary的真实名称
	Public Sub Init(Byval p_dict_name)
		Set s_collector = Nothing
		Set s_collector = Server.CreateObject( p_dict_name )
	End Sub
	
	'''判断指定的文件是否已加载进来
	'p_path:待判断文件的路径
	Private Function hasIncluded_(Byval p_path)
		Dim t_map_path : t_map_path = ASHAPO_RUNTIME_PATH & "/cache/map/context_free/" & s_cacheFileName
		
		If Runtime.Fso.IsFile(t_map_path) Then
			Dim t_maps : t_maps = Split( Runtime.Fso.ReadFile(t_map_path), "|" )
			If IsIn(Runtime.Fso.AbsPathForWeb(p_path), t_maps) Then
				hasIncluded_ = True
			Else
				hasIncluded_ = False
			End If
		Else
			hasIncluded_ = False
		End If	
	End Function
	
	'''从完整路径格式化得到待用函数名
	Private Function fullPathToFunName_(Byval p_fullpath)
		Dim t_funname
		t_funname = "ashapo_fun_" & p_fullpath 
		t_funname = Replace( t_funname, "/", "_1_" )
		t_funname = Replace( t_funname, ".", "_0_" )
		fullPathToFunName_ = t_funname
	End Function
	
	'''检查已加载的文件
	'''如果是中间缓存文件，检查最新状态
	'''如果文件已不存在则删去该加载记录
	'''注意，本函数并不处理已加载但不使用的情况，如果需要全部刷新，建议删除全部运行时文件
	Private Sub checkIncludedFiles_()
		'''上下文无关的USEAS处理
		Dim t_map_path0 : t_map_path0 = ASHAPO_RUNTIME_PATH & "/cache/map/name_space/" & s_cacheFileName
		If Runtime.Fso.IsFile(t_map_path0) Then
			Dim t_items0 : t_items0 = Split( Runtime.Fso.ReadFile(t_map_path0), "|" )
			Dim t_full_code0, t_delete_code0, t_item0, t_str0 : t_str0 = ""
			t_full_code0 = Runtime.Fso.ReadFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp" )
			Dim t_must_rebuild0 : t_must_rebuild0 = False
			For Each t_item0 In t_items0
				Dim t_values0 : t_values0 = Split( t_item0, ">" )
				Dim t_original_file : t_original_file = t_values0(0)
				Dim t_values1 : t_values1 = Split( t_values0(1), ":" )
				Dim t_target_file : t_target_file = t_values1(0)
				Dim t_last_modified_last : t_last_modified_last = t_values1(1)
				Dim t_original_items : t_original_items = Split(t_original_file, "/")
				Dim t_original_class_name : t_original_class_name = Replace(Lcase(t_original_items(UBound(t_original_items))), ".class.asp", "")
				Dim t_target_items : t_target_items = Split(t_target_file, "/")
				Dim t_target_class_name : t_target_class_name = Replace(Lcase(t_target_items(UBound(t_target_items))), ".class.asp", "")
				If Runtime.Fso.IsFile(t_original_file) Then
					Dim t_code, t_last_modified_now, t_item
					t_code = Runtime.Fso.ReadFile(t_original_file)
					t_code = Runtime.Preg.Replace(t_code, "class[\s]+" & t_original_class_name, "Class " & t_target_class_name)
					If Not Runtime.Fso.IsFile(t_target_file) Then
						'''创建目标文件
						Call Runtime.Fso.CreateFile(t_target_file, t_code)
					End If
					t_last_modified_now = Cstr(Runtime.Fso.GetFileDateLastModified(t_original_file))
					If t_last_modified_now <> t_last_modified_last Then
						'''修改目标文件
						Call Runtime.Fso.CreateFile(t_target_file, t_code)
						t_must_rebuild0 = True
					End If

					t_item = t_original_file & ">" & t_target_file & ":" & t_last_modified_now
					If t_str0 <> "" Then
						t_str0 = t_str0 & "|"
					End If
					t_str0 = t_str0 & t_item
				Else
					'''删除目标文件
					Call Runtime.Fso.DeleteFile(t_target_file)
					t_must_rebuild0 = True
					t_delete_code0 = vbCrLf & "<!--#include virtual=""" & t_target_file & """-->"
					t_full_code0 = Replace( t_full_code0, t_delete_code0, "" )
				End If
			Next
			If t_must_rebuild0 Then
				Call Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp")
				Call Runtime.Fso.DeleteFile(t_map_path0)
				Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_full_code0)
				Call Runtime.Fso.CreateFile(t_map_path0, t_str0)
			End If
		End If
		'''上下文无关的INCLUDE处理
		Dim t_map_path1 : t_map_path1 = ASHAPO_RUNTIME_PATH & "/cache/map/context_free/" & s_cacheFileName
		If Runtime.Fso.IsFile(t_map_path1) Then
			Dim t_files1 : t_files1 = Split( Runtime.Fso.ReadFile(t_map_path1), "|" )
			Dim t_full_code1, t_delete_code1, t_file1, t_str1 : t_str1 = ""
			t_full_code1 = Runtime.Fso.ReadFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp" )
			Dim t_must_rebuild1 : t_must_rebuild1 = False
			For Each t_file1 In t_files1
				If Runtime.Fso.IsFile(t_file1) Then
					If t_str1 <> "" Then
						t_str1 = t_str1 & "|"
					End If
					t_str1 = t_str1 & t_file1
				Else
					t_must_rebuild1 = True
					t_delete_code1 = vbCrLf & "<!--#include virtual=""" & t_file1 & """-->"
					t_full_code1 = Replace( t_full_code1, t_delete_code1, "" )
				End If
			Next
			If t_must_rebuild1 Then
				Call Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp")
				Call Runtime.Fso.DeleteFile(t_map_path1)
				Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_full_code1, False)
				Call Runtime.Fso.CreateFile(t_map_path1, t_str1, True)
			End If
		End If
		'''上下文有关的INCLUDE处理
		Dim t_map_path2 : t_map_path2 = ASHAPO_RUNTIME_PATH & "/cache/map/context_sensitive/" & s_cacheFileName
		If Runtime.Fso.IsFile(t_map_path2) Then
			Dim t_files2 : t_files2 = Split( Runtime.Fso.ReadFile(t_map_path2), "|" )
			Dim t_full_code2, t_delete_code2, t_file2, t_str2 : t_str2 = ""
			t_full_code2 = Runtime.Fso.ReadFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp" )
			Dim t_must_rebuild2 : t_must_rebuild2 = False
			For Each t_file2 In t_files2
				If Runtime.Fso.IsFile(t_file2) Then
					If t_str2 <> "" Then
						t_str2 = t_str2 & "|"
					End If
					t_str2 = t_str2 & t_file2
				Else
					t_must_rebuild2 = True
					t_delete_code2 = vbCrLf & "<" & "%Sub " & fullPathToFunName_(t_file2) & "()%" & "><!--#include virtual=""" & t_file2 & """--><" & "%End Sub%" & ">"
					t_full_code2 = Replace( t_full_code2, t_delete_code2, "" )
				End If
			Next
			If t_must_rebuild2 Then
				Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_full_code2)
				Call Runtime.Fso.CreateFile(t_map_path2, t_str2)
			End If
		End If
	End Sub
	
	'''检验环境初始化，并实现环境初始化，必须在所有实际项目操作前先进行，否则会导致重复操作
	'p_name:要作为最终执行代码文件的文件名(不含后缀)
	Public Sub Check(p_name)
	
		s_cacheFileName = p_name
		
		If VarType(ASHAPO_DEBUG) <> vbEmpty And ASHAPO_DEBUG = True Then
			checkIncludedFiles_()
		End If
	
		If VarType(ASHAPO_COMPILED) <> vbEmpty And ASHAPO_COMPILED = True Then
			''已编译的则直接运行不再编译
			Exit Sub
		End If
		
		'''首次生成
		If Not Runtime.Fso.IsFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp" ) Then
			Dim t_code
			t_code = "<" & "%" & "Const ASHAPO_COMPILED = True" & "%" & ">" & vbCrLf
			t_code = t_code & "<!--#include virtual=""" & Request.ServerVariables("SCRIPT_NAME") & """-->"
			Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_code)
		End If
		
		Server.Transfer( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp" )  ''执行真正的代码
		Response.End()
	End Sub
	
	'''判断函数是否存在
	'p_fun_name:函数名称
	Public Function FunExists(Byval p_fun_name)
  		ON Error Resume Next
  		Dim t_o : Set t_o = GetRef(p_fun_name)
  		If Err Then
      		FunExists = False
  		Else
      		FunExists = True
  		End If
	End Function 
	
	'''由文件路径获取完整的include代码(包括内部再include)
	'p_path:文件路径
	Private Function incReadByFile_(Byval p_path)
		Dim t_c, t_m, t_rule, t_inc, t_incFile, t_incStr
		t_c = Runtime.Fso.ReadFile(p_path)
		If IsN(t_c) Then
			Exit Function
		End If
		t_c = Runtime.Preg.Replace(t_c, "<"&"%"&" *?@.*?%"&">", "")
		t_c = Runtime.Preg.Replace(t_c, "(<"&"%"&"[^>]+?)(option +?explicit)([^>]*?%"&">)", "$1'$2$3")
		t_rule = "<!-- *?#include +?(file|virtual) *?= *?""??([^"":?*\f\n\r\t\v]+?)""?? *?-->"
		If Runtime.Preg.Test(t_c, t_rule) Then
			Set t_inc = Runtime.Preg.Match(t_c,t_rule)
			For Each t_m In t_inc
				If LCase(t_m.SubMatches(0))="virtual" Then
					t_incFile = t_m.SubMatches(1)
				Else
					t_incFile = Mid(p_path, 1, InstrRev(p_path, IIF(Instr(p_path, ":") > 0, "\", "/"))) & t_m.SubMatches(1)
				End If
				t_incStr = incReadByFile_(t_incFile)
				t_c = Replace(t_c,t_m,t_incStr)
			Next
			Set t_inc = Nothing
		End If
		incReadByFile_ = t_c
	End Function
	
	'''格式化include代码
	'p_code:要格式化的代码
	Private Function getFinalCode_(Byval p_code)
		Dim t_ts, t_c, t_tc, t_sc, t_st, t_en
		
		'Unix到Dos
		'p_code = Replace(p_code, vbLf, vbCrLf)
		
		'格式化隔行/%/>/</%/这种情况,防止Select语句被非法阻断
		p_code = Runtime.Preg.Replace(p_code, "%"&">[\n\s*\r]+<"&"%", vbCrLf)
		
		'Response.Write(p_code)
		
		'开始
		t_c = "" : t_st = 1 : t_en = Instr(p_code,"<"&"%") + 2
		t_sc = "Response.Write "
		While t_en > t_st + 1
			t_ts = Mid(p_code, t_st, t_en-t_st-2)
			t_st = Instr(t_en, p_code, "%"&">") + 2
			If Has(t_ts) Then
				t_ts = Replace(t_ts, """", """""")
				t_ts = Replace(t_ts, vbCrLf, """&vbCrLf&""")
				t_c = t_c & t_sc & """" & t_ts & """" & vbCrLf
			End If
			t_ts = Mid(p_code, t_en, t_st-t_en-2)
			t_tc = Runtime.Preg.Replace(t_ts, "^\s*=\s*", t_sc) & vbCrLf
			t_c = t_c & t_tc
			t_en = Instr(t_st, p_code, "<"&"%") + 2
		Wend
		t_ts = Mid(p_code, t_st)
		If Has(t_ts) Then
			t_ts = Replace(t_ts, """", """""")
			t_ts = Replace(t_ts, vbcrlf, """&vbCrLf&""")
			t_c = t_c & t_sc & """" & t_ts & """" & vbCrLf
		End If
		
		''消除空行
		''getFinalCode_ = Runtime.Preg.Replace(t_c,"(\n\s*\r)+",vbCrLf)
		getFinalCode_ = Runtime.Preg.Replace(t_c, "(\n\s*\r)+", "")
	End Function
	
	'''载入上下文相关的文件，可以多次执行以实现重复载入
	'由于载入的文件作为一个方法来运行，因此不能含函数定义代码段、类定义代码段，只能是纯执行代码(可夹杂HTML)
	'p_path:要运行的代码文件路径
	'p_recheck:是否重新检查文件以重新载入文件内容
	Public Sub RunFile(Byval p_path, Byval p_recheck)
		Dim t_fun_name : t_fun_name = fullPathToFunName_( Runtime.Fso.AbsPathForWeb( p_path ) )
		If FunExists( t_fun_name ) And Not p_recheck Then
			ExecuteGlobal( t_fun_name & "()" )
		Else
			Dim t_code : t_code = vbCrLf & "<" & "%Sub " & t_fun_name & "()%" & "><!--#include virtual=""" & Runtime.Fso.AbsPathForWeb( p_path ) & """--><" & "%End Sub%" & ">"
			
			Dim t_map_path : t_map_path = ASHAPO_RUNTIME_PATH & "/cache/map/context_sensitive/" & s_cacheFileName
		
			If Runtime.Fso.IsFile(t_map_path) Then
				Dim t_maps : t_maps = Split( Runtime.Fso.ReadFile(t_map_path), "|" )
				If Not IsIn(Runtime.Fso.AbsPathForWeb(p_path), t_maps) Then
					Call Runtime.Fso.AppendFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_code )
					Call Runtime.Fso.AppendFile(t_map_path, "|" & Runtime.Fso.AbsPathForWeb(p_path))
				End If
			Else
				Call Runtime.Fso.AppendFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_code )
				Call Runtime.Fso.CreateFile(t_map_path, Runtime.Fso.AbsPathForWeb(p_path))
			End If
			
			Dim t_run_code : t_run_code = getFinalCode_( incReadByFile_(p_path) )
			On Error Resume Next
			ExecuteGlobal( t_run_code )
			If Err.Number<>0 Then
				
				'''直接刷新当前页面可以直接看到错误信息
				'Server.Transfer( Request.ServerVariables("SCRIPT_NAME") )
				
				Die("CORE ERROR 101:""" & p_path & """ code error [" & Err.Number & "], refresh page to see the reason.<br/>[" & Err.ASPDescription & "]")
			End If
		End If
	End Sub
	
	'''底层include
	'p_path:文件路径
	Private Sub coreInclude_(Byval p_path)
		s_collector(Runtime.Fso.AbsPath(p_path)) = "HAS_INCLUDED"
		Dim t_code : t_code = incReadByFile_(p_path)
		'On Error Resume Next
		ExecuteGlobal(getFinalCode_(t_code))
		If Err.Number<>0 Then
			
			'''直接刷新当前页面可以直接看到错误信息
			'Server.Transfer( Request.ServerVariables("SCRIPT_NAME") )
			
			Call Die("CORE ERROR 102:""" & p_path & """ code error [" & Err.Number & "], refresh page to see the reason.<br/>[" & Err.ASPDescription & "]")
		End If
	End Sub
	
	'''上下文无关动态include
	'''由于是上下文无关，故重复include是不会产生效果的
	'''@todo IncludeOnce
	'p_path:文件路径
	Public Sub Include(Byval p_path)
		If hasIncluded_(p_path) Then
			'''@todo 此处是否要抛出错误
			Exit Sub
		End If
		
		Dim t_code
		Dim t_map_path : t_map_path = ASHAPO_RUNTIME_PATH & "/cache/map/context_free/" & s_cacheFileName
		
		If Runtime.Fso.IsFile(t_map_path) Then
			Dim t_maps : t_maps = Split( Runtime.Fso.ReadFile(t_map_path), "|" )
			If IsIn(Runtime.Fso.AbsPathForWeb(p_path), t_maps) Then
				Exit Sub
			Else
				t_code = vbCrLf & "<!--#include virtual=""" & Runtime.Fso.AbsPathForWeb(p_path) & """-->"
				Call Runtime.Fso.AppendFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_code )
				Call Runtime.Fso.AppendFile(t_map_path, "|" & Runtime.Fso.AbsPathForWeb(p_path))
				Call coreInclude_(p_path)
			End If
		Else
			t_code = vbCrLf & "<!--#include virtual=""" & Runtime.Fso.AbsPathForWeb(p_path) & """-->"
			Call Runtime.Fso.AppendFile( ASHAPO_RUNTIME_PATH & "/cache/code/compile/" & s_cacheFileName & ".asp", t_code )
			Call Runtime.Fso.CreateFile(t_map_path, Runtime.Fso.AbsPathForWeb(p_path))
			Call coreInclude_(p_path)
		End If		
	End Sub

	'''载入符合规则的类
	'p_class_full_name:类全名
	Public Sub [Use](Byval p_class_full_name)
		Dim t_real_file : t_real_file = p_class_full_name & ".class.asp"
		Call Include(t_real_file)
	End Sub

	'''载入符合规则的类，并赋予别名
	'''使用该方法可解决类命名重名问题，同时起到命名空间的作用
	'p_class_full_name:类全名
	'p_alias_name:类别名
	Public Sub UseAs(Byval p_class_full_name, Byval p_alias_name)
		Dim t_original_file : t_original_file = p_class_full_name & ".class.asp"
		Dim t_target_file : t_target_file = ASHAPO_RUNTIME_PATH & "/cache/code/name_space" & p_class_full_name & "/" & p_alias_name & ".class.asp"
		If hasIncluded_(t_target_file) Then
			'''@todo 此处是否要抛出错误
			Exit Sub
		End If
		Dim t_map_path : t_map_path = ASHAPO_RUNTIME_PATH & "/cache/map/name_space/" & s_cacheFileName
		Dim t_item
		t_item = Runtime.Fso.AbsPathForWeb(t_original_file) & ">" & Runtime.Fso.AbsPathForWeb(t_target_file) & ":" & Cstr(Runtime.Fso.GetFileDateLastModified(t_original_file))
		If Runtime.Fso.IsFile(t_map_path) Then
			Dim t_maps : t_maps = Split( Runtime.Fso.ReadFile(t_map_path), "|" )
			If IsIn(t_item, t_maps) Then
				Exit Sub
			Else
				Call Runtime.Fso.AppendFile(t_map_path, "|" & t_item)
			End If
		Else
			Call Runtime.Fso.CreateFile(t_map_path, t_item)
		End If

		Dim t_items : t_items = Split(p_class_full_name, "/")
		Dim t_original_class_name : t_original_class_name = Lcase(t_items(UBound(t_items)))
		Dim t_class_code
		t_class_code = Runtime.Fso.ReadFile(t_original_file)
		t_class_code = Runtime.Preg.Replace(t_class_code, "class[\s]+[\[]*" & t_original_class_name & "[\]]*", "Class [" & p_alias_name & "]")
		Call Runtime.Fso.CreateFile(t_target_file, t_class_code)

		Call Include(t_target_file)
	End Sub
End Class
%>