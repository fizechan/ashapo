<%
'''底层数据库操作类
Class AshapoDatabase

	'当前数据库连接对象
	Private s_conn
	
	'''构造
	Private Sub Class_Initialize()
		Set s_conn = Server.CreateObject(Runtime.ProgID("Adodb.Connection"))
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		If TypeName(s_conn) = "Connection" Then
			If s_conn.State = 1 Then
				s_conn.Close()
			End If
			Set s_conn = Nothing
		End If
	End Sub
	
	'''实例化一个DbClass,用于开启多个数据库连接
	Public Function [New]()
		Set [New] = New AshapoDatabase
	End Function

	'''取得当前原生连接用于原生操作
	Public Property Get Conn()
		Set Conn = s_conn
	End Property
	
	'''开始事务
	Public Sub [BeginTrans]()
		s_conn.BeginTrans()
	End Sub
	
	'''提交事务
	Public Sub [CommitTrans]()
		s_conn.CommitTrans()
	End Sub
	
	'''事务回滚
	Public Sub [RollbackTrans]()
		s_conn.RollbackTrans()
	End Sub

	'''关闭数据连接或记录集对象并资源释放资源
	'''todo:准备废弃
	'p_o:对象名
	Public Sub Close(ByRef p_o)
		p_o.Close()
		Set p_o = Nothing
	End Sub
	
	'''公用数据库日志输出
	'p_s:要输出的额外部分
	Private Function dbLog_(Byval p_s)
		Dim t_log : t_log = p_s & ":\n"
		For Each objErr In s_conn.Errors
			t_log = t_log & "Description: \n"
			t_log = t_log & objErr.Description & "\n"
			t_log = t_log & "Help context: \n"
			t_log = t_log & objErr.HelpContext & "\n"
			t_log = t_log & "Help file: \n"
			t_log = t_log & objErr.HelpFile & "\n"
			t_log = t_log & "Native error: \n"
			t_log = t_log & objErr.NativeError & "\n"
			t_log = t_log & "Error number: \n"
			t_log = t_log & objErr.Number & "\n"
			t_log = t_log & "Error source: \n"
			t_log = t_log & objErr.Source & "\n"
			t_log = t_log & "SQL state: \n"
			t_log = t_log & objErr.SQLState
		Next
		dbLog_ = t_log
	End Function
	
	'''初始化，根据自定义连接字符串建立数据库连接对象
	'p_s:连接字符
	Public Sub Init(Byval p_s)
		'On Error Resume Next
		s_conn.Open(p_s)
		If Err.number <> 0 Then
			Dim t_log
			t_log = dbLog_("建立连接时出现错误!")
			Call Sa.Console.SQL(t_log)
			s_conn.Close()
			Set s_conn = Nothing
		End If
	End Sub

	'''根据SQL语句获取记录集,出错应直接出错
	'p_sql:SQL语句
	Public Function Query(Byval p_sql)
		'On Error Resume Next
		Dim t_rs : Set t_rs = Server.CreateObject(Runtime.ProgID("Adodb.Recordset"))
		With t_rs
			.ActiveConnection = s_conn
			.CursorType = 1
			.LockType = 1
			.Source = p_sql
			.Open
		End With
		Set Query = t_rs
		
		'Console.SQL(p_s)
		'Console.DbQueryTimes = Console.DbQueryTimes + 1
		
		If Err.number <> 0 Then
			Dim t_log
			t_log = dbLog_("根据SQL语句获取记录集!")
			Call Sa.Console.SQL(t_log)
			Err.Clear()
		End If
	End Function
	
	'''执行指定的SQL语句
	'执行成功返回受影响行数，执行失败返回False
	'p_s:sql语句
	Public Function [Execute](Byval p_s)
		'On Error Resume Next
		Dim t_e : Set t_e = Server.CreateObject(Runtime.ProgID("Adodb.Command"))
		Dim t_ra
		With t_e
			.ActiveConnection = s_conn
			.CommandText = p_s
			.Execute t_ra, , 129
		End With
		Set t_e = Nothing
		[Execute] = t_ra
		If Err.number <> 0 Then
			Dim t_log
			t_log = dbLog_("执行SQL语句时发生错误" & Err.number & "!")
			Call Sa.Console.SQL(t_log)
			[Execute] = False
		End If
		'Console.DbQueryTimes = Console.DbQueryTimes + 1
	End Function
End Class
%>