<%
'''缓存类
'''使用了Global全局对象
Class AshapoCache
	
	'''构造
	Private Sub Class_Initialize()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub
	
	'''通过驱动对外暴露缓存接口，初始化Sa.Cache对象
	'p_dirver:驱动名
	'p_expire:缓存有效期（单位为秒）,为0表示永久有效
	'p_prefix:缓存标识前缀
	Public Function Init(Byval p_dirver, Byval p_expire, Byval p_prefix)
		Select Case p_dirver
		Case "Application"
			Call Runtime.Asp.Use( ASHAPO_CORE_PATH & "/driver/cache/AshapoCacheDriverApplication" )
			Set Init = New AshapoCacheDriverApplication
		Case "File"
			Call Runtime.Asp.Use( ASHAPO_CORE_PATH & "/driver/cache/AshapoCacheDriverFile" )
			Set Init = New AshapoCacheDriverFile
		End Select
		Call Init.Init( p_expire, p_prefix )
	End Function
End Class
%>