<%
'''上下文对象类，作为核心入口
Class AshapoSa
	
	'''当前模块
	Private s_module
	
	'''当前控制器
	Private s_controller
	
	'''当前操作
	Private s_action
	
	'''todo 待删除
	Private s_nowL
	
	'''核心使用的AshapoConfig对象
	Public Config
	
	'''核心使用的AshapoCache对象
	Public Cache
	
	'''核心使用的AshapoCookie对象
	Public Cookie
	
	'''Sa控制下的控制台，只能有一个实例
	Public Console

	'''Sa控制下的Request，只能有一个实例
	Public [Request]

	'''核心使用的AshapoView对象
	Public View

	'''核心使用的AshapoJson对象
	Public Json
	
	'''构造
	Private Sub Class_Initialize()
		Session.CodePage = 936
		Response.Charset = "GBK"
		
		Set Config = New AshapoConfig
		Call Config.Init()
		
		Set Console = New AshapoConsole
		Set Cache = New AshapoCache
		Set Cookie = New AshapoCookie

		Set [Request] = New AshapoRequest
		Set View = New AshapoView
		Set Json = New AshapoJson
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set Json = Nothing
		Set View = Nothing
		Set [Request] = Nothing

		Set Cookie = Nothing

		Set Cache = Nothing

		Set Console = Nothing
		
		Set Config = Nothing
	End Sub
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''核心相关方法
	
	'''获取当前的系统常量
	'''MODULE_NAME:当前模块名
	'''CONTROLLER_NAME:当前控制器名
	'''ACTION_NAME:当前操作名
	'''__SELF__当前URL
	'p_key:设置键名，不区分大小写，但建议都使用大写
	Public Property Get Constant(Byval p_key)
		Select Case Ucase(p_key)
		Case "MODULE_NAME"
			Constant = s_module
		Case "CONTROLLER_NAME"
			Constant = s_controller
		Case "ACTION_NAME"
			Constant = s_action
		Case "__SELF__"
			Constant = [Request].ServerVariables("SCRIPT_NAME")
			Dim t_s : t_s = [Request].ServerVariables("QUERY_STRING")
			If t_s <> "" Then
				Constant = Constant & "?" & t_s
			End If
		End Select
		'Response.Write(Constant)
	End Property
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''模板相关方法
	
	'''通用的错误页面显示
	'p_message:错误信息
	'p_jumpUrl:跳转URL
	'p_waitSecond:跳转等待时间(单位秒) todo 考虑将p_waitSecond移入配置项
	Public Sub [Error](Byval p_message, Byval p_jumpUrl, Byval p_waitSecond)
		[Dim]("message")
		message = p_message
		[Dim]("jumpUrl")
		jumpUrl = p_jumpUrl
		[Dim]("waitSecond")
		waitSecond = p_waitSecond
		View.Path( Config("TMPL_ACTION_ERROR") ).Display()
		Response.End()
	End Sub
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''JSON相关方法
	
	'''输出JSON
	'p_objt:可以是JSON字符串，也可以是Json对象
	Public Sub AjaxReturn(Byval p_objt)
		Response.ContentType = "application/json"
		If Not VarType( p_objt ) = VbString Then
			p_objt = Json.Encode(p_objt)
		End If
		Response.Write(p_objt)
		'Response.End()
	End Sub

	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''入口相关方法	
	
	'''ashapo编译入口
	Public Sub Init()
		s_module = [Request].GetWithDefault(ASHAPO_VAR_MODULE, ASHAPO_DEFAULT_MODULE)
		s_controller = [Request].GetWithDefault(ASHAPO_VAR_CONTROLLER, ASHAPO_DEFAULT_CONTROLLER)
		s_action = [Request].GetWithDefault(ASHAPO_VAR_ACTION, ASHAPO_DEFAULT_ACTION)

		'''Runtime.Asp.Check由于是在构建运行环境，所以必须在第一位执行
		Call Runtime.Asp.Check( Lcase( s_module & "_" & s_controller & "_" & s_action ) )
	
		Console.GroupBegin("开始执行")

		Console.Debug("载入ashapo配置")
		Call Config.LoadAshapo()

		Console.Debug("载入公共配置")
		Call Config.LoadCommon()

		Console.Debug("载入模块设置")
		Config.LoadModule(s_module)

		'''取得最终真实配置后在初始化Console
		Call Console.SetLogerDriver(Config("LOGER_DRIVER"))
		Console.SetLogPath( ASHAPO_RUNTIME_PATH & "/log/" & s_module & "/" & Year(Date()) & "_" & Month(Date()) & "_" & Day(Date()) & ".log" )

		s_theme = Config("DEFAULT_THEME") ''设置默认模板主题
		Call View.Init(s_module, s_controller, s_action)

		Console.Debug("初始化Cache")
		Set Cache = Cache.Init(Config("CACHE_DRIVER"), Config("CACHE_TIME"), Config("CACHE_PREFIX"))

		Console.Debug("初始化Cookie")
		Cookie.Expires = Config("COOKIE_EXPIRES")
		Cookie.Domain = Config("COOKIE_DOMAIN")
		Cookie.Path = Config("COOKIE_PATH")
		Cookie.Prefix = Config("COOKIE_PREFIX")
		Cookie.Secure = Config("COOKIE_SECURE")
		Cookie.HttpOnly = Config("COOKIE_HTTPONLY")
		Cookie.AesEncode = Config("COOKIE_AES_ENCODE")
		Cookie.AesKey = Config("COOKIE_AES_KEY")
		
		Console.Debug("载入控制器类")
		Runtime.Asp.Use( ASHAPO_APP_PATH & "/" & s_module & "/controller/" & s_controller)
		
		Console.Debug("开始执行动作")
		ExecuteGlobal("Dim " & Config("RUN_OBJECT_NAME"))
		ExecuteGlobal("Set " & Config("RUN_OBJECT_NAME") & " = New [" & s_controller & "]")
		ExecuteGlobal(Config("RUN_OBJECT_NAME") & "." & s_action & "()")
		ExecuteGlobal("Set " & Config("RUN_OBJECT_NAME") & " = Nothing")
		
		Console.GroupEnd("执行结束")
	End Sub
	
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''工具相关方法

	'''实例化一个表模型
	'p_name:表名称，不含前缀
	Public Function Model(Byval p_name)
		Runtime.Asp.Use(ASHAPO_CORE_PATH & "/core/AshapoOrm")
		Set Model = (New AshapoOrm).Init(Config("DB_TYPE"), Config("DB_HOST"), Config("DB_PORT"), Config("DB_NAME"), Config("DB_USER"), Config("DB_PWD") )
		Model.Prefix(Config("DB_PREFIX"))
		Model.Table(p_name)
	End Function
	
	'''设置是否进行当前页面HTML静态缓存
	'p_b:布尔值,是否生成HTML，如果设置成False时会顺便删除已生成的HTML缓存
	Public Property Let HTML(Byval p_b)
		Runtime.Asp.Use(ASHAPO_CORE_PATH & "/core/AshapoHtml")
		Dim t_html : Set t_html = New AshapoHtml
		Call t_html.Init(s_module, s_controller, s_action)
		t_html.Make(p_b)
	End Property

	'''简易的通用字典定义
	Public Property Get Dictionary()
		Set Dictionary = Runtime.Dictionary
	End Property

	'''上下文无关动态include
	'p_path:文件路径
	Public Sub Include(Byval p_path)
		Call Runtime.Asp.Include(p_path)
	End Sub

	'''载入符合规则的类
	'p_class_full_name:类全名
	Public Sub [Use](Byval p_class_full_name)
		Call Runtime.Asp.Use(p_class_full_name)
	End Sub

	'''载入符合规则的类，并赋予别名
	'''使用该方法可解决类命名重名问题，同时起到命名空间的作用
	'p_class_full_name:类全名
	'p_alias_name:类别名
	Public Sub UseAs(Byval p_class_full_name, Byval p_alias_name)
		Call Runtime.Asp.UseAs(p_class_full_name, p_alias_name)
	End Sub

	'''变量预定义
	'''支持数组定义,数组定义和原生语法保持一致
	'''例参数"arr(5)"表示定义名为arr的一维数组,其数组大小为6(最大可用下标5)
	'''例参数"arr(5,4)"表示定义名为arr的二维数组,其数组大小为6X5
	'p_name:变量名
	Public Sub [Dim](Byval p_name)
		Call Runtime.Dim(p_name)
	End Sub
End Class
%>