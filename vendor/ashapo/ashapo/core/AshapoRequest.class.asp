<%
'''依赖层级:001
'''依赖文件:functions.asp
'''底层请求类,
'''请使用Sa唯一实例，而不是直接new一个AshapoRequest对象
Class AshapoRequest
	
	'''安全化获取一个GET参数
	'p_key:参数名
	Public Function [Get](Byval p_key)
		'''@todo 需要加入安全化处理
		[Get] = Request.QueryString(p_key)
	End Function
	
	'''安全化获取一个GET参数，如果没有该参数则返回一个默认值
	'p_key:参数名
	'p_default:默认值
	Public Function GetWithDefault(Byval p_key, Byval p_default)
		GetWithDefault = IfHas( [Get](p_key), p_default )
	End Function

	'''获取所有的Get参数
	Public Property Get Gets()
		Set Gets = Request.QueryString
		'''todo 进行剔除和安全化
	End Property

	'''安全化获取POST参数
	'p_key:参数名，如果是数组则获取多个
	Public Function Post(Byval p_key)
		'''todo 需要加入安全化处理
		If IsArray(p_key) Then
			Dim t_key
			Dim t_dict : Set t_dict = Sa.Dictionary
			For Each t_key In p_key
				t_dict(t_key) = Request.Form(t_key)
			Next
			Set Post = t_dict
		Else
			Post = Request.Form( p_key )
		End If
	End Function

	'''安全化获取一个POST参数，如果没有该参数则返回一个默认值
	'p_key:参数名
	'p_default:默认值
	Public Function PostWithDefault(Byval p_key, Byval p_default)
		PostWithDefault = IfHas( Post(p_key), p_default )
	End Function

	'''获取所有的Post参数
	Public Property Get Posts()
		Set Posts = Request.Form
		'''todo 进行安全化
	End Property

	'''型如PHP的REQUEST
	Private Function request_(Byval p_key)
		request_ = [Get](p_key)
		If IsN(request_) Then
			request_ = Post(p_key)
		End If
	End Function

	'''安全化获取REQUEST参数
	'p_key:参数名，如果是数组则获取多个
	Public Default Function Value(Byval p_key)
		If IsArray(p_key) Then
			Dim t_key
			Dim t_dict : Set t_dict = Sa.Dictionary
			For Each t_key In p_key
				t_dict(t_key) = request_(t_key)
			Next
			Set Value = t_dict
		Else
			Value = request_( p_key )
		End If
	End Function

	'''安全化获取一个Request参数，如果没有该参数则返回一个默认值
	'p_key:参数名
	'p_default:默认值
	Public Function Value2(Byval p_key, Byval p_default)
		Value2 = IfHas( [Request](p_key), p_default )
	End Function

	'''获取所有的Get、Post参数
	'''同名状态下，GET的权重比POST高
	Public Property Get Values()
		Dim t_dict : Set t_dict = Sa.Dictionary
		Dim t_key
		For Each t_key In Request.Form
			'''todo 进行安全化
			t_dict(t_key) = Request.Form(t_key)
		Next
		'''GET权重更高
		For Each t_key In Request.QueryString
			'''todo 进行安全化
			t_dict(t_key) = Request.QueryString(t_key)
		Next
		Set Values = t_dict
	End Property

	'''判断是否是GET提交
	Public Property Get IsGet()
		If Request.ServerVariables("REQUEST_METHOD") = "GET" Then
			IsGet = True
		Else
			IsGet = False
		End If
	End Property

	'''判断是否是POST提交
	Public Property Get IsPost()
		If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
			IsPost = True
		Else
			IsPost = False
		End If
	End Property

	'''判断是否是AJAX提交
	'''注意本方法使用到Sa对象
	Public Property Get IsAjax()
		If Has( Request.ServerVariables("HTTP_X_REQUESTED_WITH") ) Then
			If Lcase( Request.ServerVariables("HTTP_X_REQUESTED_WITH") ) = "xmlhttprequest" Then
				IsAjax = True
				Exit Property
			End If
		End If
		If Has( Request.Form( Sa.Config("VAR_AJAX_SUBMIT") ) ) Then
			IsAjax = True
			Exit Property
		End If
		If Has( Request.QueryString( Sa.Config("VAR_AJAX_SUBMIT") ) ) Then
			IsAjax = True
			Exit Property
		End If
		IsAjax = False
	End Property

	'''由于Sa直接调用了Request，则需要在此实现使用到的Request功能转化
	'p_key:参数名
	Public Function ServerVariables(Byval p_key)
		ServerVariables = Request.ServerVariables( p_key )
	End Function


	'''安全化值
	'''todo 待移除
	'p_s:值
	'p_t:预计的类型,(根据该类型进行安全转化)
	Public Function S(Byval p_s, Byval p_t)
		Dim t_s, t_d, t_l, t_r, t_i, t_t, t_a() : t_l = False
		If Instr(p_t, ":")>0 Then
			t_d = CRight(p_t, ":") : p_t = CLeft(p_t, ":")
		End If
		If Instr("sdn", Left(LCase(p_t), 1))>0 Then
			If Len(p_t)>1 Then
				t_s = Mid(p_t,2) : p_t = LCase(Left(p_t, 1)) : t_l = True
			End If
		ElseIf Has(p_t) Then
			t_s = p_t : p_t = "" : t_l = True
		End If
		t_r = Split(p_s, t_s)
		If t_l Then
			Redim t_a(Ubound(t_r))
		End If
		For t_i = 0 To Ubound(t_r)
			If t_i<>0 Then
				t_t = t_t & t_s
			End If
			Select Case p_t
			Case "s"
				If IsN(t_r(t_i)) Then
					t_r(t_i) = t_d
				End If
				t_t = t_t & Replace(t_r(t_i), "'", "''")
				If t_l Then
					t_a(t_i) = Replace(t_r(t_i), "'", "''")
				End If
			Case "d"
				t_t = IIF(IsDate(t_r(t_i)), t_t & t_r(t_i), t_t & t_d)
				If t_l Then
					t_a(t_i) = IIF(IsDate(t_r(t_i)), t_r(t_i), t_d)
				End If
			Case "n"
				t_t = IIF(IsNumeric(t_r(t_i)), t_t & t_r(t_i), t_t & t_d)
				If t_l Then
					t_a(t_i) = IIF(IsNumeric(t_r(t_i)), t_r(t_i), t_d)
				End If
			Case Else
				t_t = IIF(IsN(t_r(t_i)), t_t & t_d, t_t & t_r(t_i))
				If t_l Then
					t_a(t_i) = IIF(IsN(t_r(t_i)), t_d, t_r(t_i))
				End If
			End Select
			If t_l Then
				If IsN(t_a(t_i)) Then
					t_a(t_i) = Empty
				End If
			End If
		Next
		If IsN(t_t) Then
			t_t = Empty
		End If
		S = IIF(t_l, t_a, t_t)
	End Function
End Class
%>