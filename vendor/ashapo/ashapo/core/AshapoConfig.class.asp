<%
'''底层配置类
Class AshapoConfig
	
	'''所有配置项字典
	Private s_configs
	
	'''构造
	Private Sub Class_Initialize()
		
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_configs = Nothing
	End Sub

	'''初始化
	'''@todo 系统自动调用该方法初始化，我们并不希望外部手动调用该方法
	Public Sub Init()
		Set s_configs = Nothing
		Set s_configs = Server.CreateObject(Runtime.ProgID("Scripting.Dictionary"))
	End Sub
	
	'''获取当前的设置
	'''todo 待修改
	'p_key:设置键名，不区分大小写
	Public Default Property Get Value(Byval p_key)
		'If IsObject( s_config(Ucase(p_key)) ) Then
		'	Set Value = s_config(Ucase(p_key))
		'Else
		'	Value = s_config(Ucase(p_key))
		'End If

		Call Assign( Value, s_configs(Ucase(p_key)) )
	
		'Config = s_config(Ucase(p_key))
	End Property
	
	'''设置当前设置
	'''todo 待修改
	'''如果在公共模块中调用，则作用于全部模块
	'''如果在配置文件中调用，则作用于整个所属模块
	'''如果临时调用则在当前运行时有效
	'p_key:设置键名，不区分大小写
	'p_val:键值，可以是任意变量
	Public Property Let Value(Byval p_key, Byval p_val)
		s_configs(Ucase(p_key)) = p_val
	End Property
	
	'''设置当前设置
	'''todo 待修改
	'''如果在公共模块中调用，则作用于全部模块
	'''如果在配置文件中调用，则作用于整个所属模块
	'''如果临时调用则在当前运行时有效
	'p_key:设置键名，不区分大小写
	'p_val:键值，可以是任意变量
	Public Property Set Value(Byval p_key, Byval p_val)
		Set s_configs(Ucase(p_key)) = p_val
	End Property

	'''载入ASHAPO设置
	'''使用该方法前需保证已实例化ashapo的sa对象
	Public Sub LoadAshapo()
		'''载入ashapo默认配置
		Call Runtime.Asp.RunFile( ASHAPO_CORE_PATH & "/config/convention.asp", False )
		If ASHAPO_DEBUG Then
			Call Runtime.Asp.RunFile( ASHAPO_CORE_PATH & "/config/debug.asp", False )
		End If
	End Sub

	'''载入公共设置
	'''使用该方法前需保证已实例化ashapo的sa对象
	Public Sub LoadCommon()
		Dim t_i
		'''载入公共设置
		Call Runtime.Asp.RunFile( ASHAPO_ROOT_PATH & "/config/config.asp", False )
		'''载入公共扩展设置
		If s_configs("LOAD_EXT_CONFIG") <> "" Then
			Dim t_cfl_c : t_cfl_c = Split(s_configs("LOAD_EXT_CONFIG"), ",")
			For t_i = 0 To UBound(t_cfl_c)
				Call Runtime.Asp.RunFile( ASHAPO_ROOT_PATH & "/config/" & t_cfl_c(t_i) & ".asp", False )
			Next
			'置空LOAD_EXT_CONFIG,防止接下来判断错误
			s_configs("LOAD_EXT_CONFIG") = ""
		End If
	
		'''待移除
		'''载入公共函数
		If Runtime.Fso.IsFile( ASHAPO_APP_PATH & "/common/common/function.asp" ) Then
			Runtime.Asp.Include( ASHAPO_APP_PATH & "/common/common/function.asp" )
		End If
		'''载入公共扩展函数
		If s_configs("LOAD_EXT_FILE") <> "" Then
			Dim t_fun_c : t_fun_c = Split(s_configs("LOAD_EXT_FILE"), ",")
			For t_i = 0 To UBound(t_fun_c)
				Runtime.Asp.Include( ASHAPO_APP_PATH & "/common/common/" & t_fun_c(t_i) & ".asp" )
			Next
			'置空LOAD_EXT_FILE,防止接下来判断错误
			s_configs("LOAD_EXT_FILE") = ""
		End If
	End Sub

	'''载入模块设置
	'''使用该方法前需保证已实例化ashapo的sa对象
	'p_module:要载入的模块配置
	Public Sub LoadModule(Byval p_module)
		Dim t_i
		'''载入模块设置
		If Runtime.Fso.IsFile( ASHAPO_ROOT_PATH & "/config/" & p_module & "/config.asp" ) Then
			Call Runtime.Asp.RunFile( ASHAPO_ROOT_PATH & "/config/" & p_module & "/config.asp", False )
			'''加载模块扩展设置
			If s_configs("LOAD_EXT_CONFIG") <> "" Then
				Dim t_cfl_m : t_cfl_m = Split(s_configs("LOAD_EXT_CONFIG"), ",")
				For t_i = 0 To UBound(t_cfl_m)
					Call Runtime.Asp.RunFile( ASHAPO_ROOT_PATH & "/config/" & p_module & "/" & t_cfl_m(t_i) & ".asp", False )
				Next
				'置空LOAD_EXT_CONFIG,防止接下来判断错误
				s_configs("LOAD_EXT_CONFIG") = ""
			End If
		End If

		'''待移除
		'''载入模块函数
		If Runtime.Fso.IsFile( ASHAPO_APP_PATH & "/" & p_module & "/common/function.asp" ) Then
			Runtime.Asp.Include( ASHAPO_APP_PATH & "/" & p_module & "/common/function.asp" )
		End If
		'''载入模块扩展函数
		If s_configs("LOAD_EXT_FILE") <> "" Then
			Dim t_fun_m : t_fun_m = Split(s_configs("LOAD_EXT_FILE"), ",")
			For t_i = 0 To UBound(t_fun_m)
				Runtime.Asp.Include( ASHAPO_APP_PATH & "/" & p_module & "/common/" & t_fun_m(t_i) & ".asp" )
			Next
			'置空LOAD_EXT_FILE,防止接下来判断错误
			s_configs("LOAD_EXT_FILE") = ""
		End If
		
		'''加载完配置后要执行的动作
		s_theme = s_configs("DEFAULT_THEME")
	End Sub
	
End Class
%>