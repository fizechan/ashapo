<%
'''依赖层级:000
'''底层字符串类,
'如果外部使用本类可以直接进行实例化，但不建议使用，应使用外部string类
Class AshapoString
	
	'''截取用某个特殊字符(串)分隔的字符串的特殊字符
	'''本函数为底层函数,外部请使用CLeft\CRight函数
	'p_string:要截取的字符串
	'p_search:特殊字符(串)
	'p_mark:标识位,值0时取截取左部,值1时取截取右部,其余返回空字符串
	Private Function CutLR_(Byval p_string, Byval p_search, Byval p_mark)
		Dim t_index : t_index = Instr(p_string, p_search)
		If t_index > 0 Then
			If p_mark = 0 Then
				CutLR_ = Left(p_string, t_index - 1)
			ElseIf p_mark = 1 Then
				CutLR_ = Mid(p_string, t_index + Len(p_search))
			End If
		Else
			CutLR_ = p_string
		End If
	End Function

	'''截取用某个特殊字符(串)分隔的字符串的特殊字符左边部分
	'p_string:要截取的字符串
	'p_search:特殊字符(串)
	Public Function CLeft(Byval p_string, Byval p_search)
		CLeft = CutLR_(p_string, p_search, 0)
	End Function

	'''截取用某个特殊字符分隔的字符串的特殊字符右边部分
	'p_string:要截取的字符串
	'p_search:特殊字符(串)
	Public Function CRight(Byval p_string, Byval p_search)
		CRight = CutLR_(p_string, p_search, 1)
	End Function


	'''将类似“Table:ID:Name”字符按“:”分割成数组Table\ID:Name
	'p_string:要分割的字符串
	Public Function Param(Byval p_string)
		Dim t_arr(1), t_index
		t_index = Instr(p_string, ":")
		If t_index > 0 Then
			t_arr(0) = Left(p_string, t_index - 1)
			t_arr(1) = Mid(p_string, t_index + 1)
		Else
			t_arr(0) = p_string
			t_arr(1) = ""
		End If
		Param = t_arr
	End Function

	'''将类似“Table:ID:Name”字符按“:”分割成数组Name\Table:ID
	'p_string:要分割的字符串
	Public Function ParamRev(Byval p_string)
		Dim t_arr(1), t_index
		t_index = InstrRev(p_string, ":")
		If t_index > 0 Then
			t_arr(0) = Mid(p_string, t_index + 1)
			t_arr(1) = Left(p_string, t_index - 1)
		Else
			t_arr(0) = p_string
			t_arr(1) = ""
		End If
		ParamRev = t_arr
	End Function
End Class
%>