<%
'''依赖层级:000
'''依赖文件:无
'''底层正则类
'本底层类服务于框架，不允许抛出错误
'为防止未知的正则污染，每个函数使用单独的RegExp对象
Class AshapoPreg


	'''正则Test
	'p_string:要判断的字符串
	'p_pattern:正则表达式
	Public Function Test(Byval p_string, Byval p_pattern)
		Dim t_reg
		Set t_reg = New RegExp
		t_reg.Global = True
		t_reg.IgnoreCase = True
		t_reg.Multiline = False
		If IsN(p_string) Then
			Test = False
			Exit Function
		End If
		t_reg.Pattern = p_pattern
		Test = t_reg.Test(CStr(p_string))
		Set t_r = Nothing
	End Function

	'''底层根据正则表达式替换字符串内容
	'''本函数为底层函数,外部请使用RegReplace\RegReplaceM函数
	'p_string:要进行替换的字符串
	'p_pattern:正则表达式
	'p_replace:替换内容,支持正则捕获组
	'p_multiline:多行模式标识符,为1时为多行模式
	Private Function RuleReplace_(Byval p_string, Byval p_pattern, Byval p_replace, Byval p_multiline)
		Dim t_reg
		Set t_reg = New RegExp
		t_reg.Global = True
		t_reg.IgnoreCase = True
		t_reg.Multiline = False
		Dim t_string
		t_string = p_string
		If Has(p_string) Then
			If p_multiline = 1 Then
				t_reg.Multiline = True
			End If
			t_reg.Pattern = p_pattern
			t_string = t_reg.Replace(t_string, p_replace)
		End If
		RuleReplace_ = t_string
		Set t_string = NoThing
	End Function

	'''根据正则表达式替换字符串内容
	'p_string:要进行替换的字符串
	'p_pattern:正则表达式
	'p_replace:替换内容,支持正则捕获组
	Public Function [Replace](Byval p_string, Byval p_pattern, Byval p_replace)
		[Replace] = RuleReplace_(p_string, p_pattern, p_replace, 0)
	End Function
	
	'''根据正则代表式替换文本(多行模式)
	'p_string:要进行替换的字符串
	'p_pattern:正则表达式
	'p_replace:替换内容,支持正则捕获组
	Public Function ReplaceM(Byval p_string, Byval p_pattern, Byval p_replace)
		RegReplaceM = RuleReplace_(p_string, p_pattern, p_replace, 1)
	End Function

	'''正则表达式编组捕获
	'p_string:要判断的字符串
	'p_pattern:正则表达式
	Public Function Match(Byval p_string, Byval p_pattern)
		Dim t_reg
		Set t_reg = New RegExp
		t_r.Global = True
		t_r.IgnoreCase = True
		t_r.Multiline = False
		t_r.Pattern = p_pattern
		Set Match = t_reg.Execute(p_string)
		Set t_reg = NoThing
	End Function

End Class
%>