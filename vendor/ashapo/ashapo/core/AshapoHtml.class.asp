<%
'''底层HTTPHTML实时静态化类
Class AshapoHtml

	'''当前模块
	Private s_module
	
	'''当前控制器
	Private s_controller
	
	'''当前操作
	Private s_action
	
	'''构造
	Private Sub Class_Initialize()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
	End Sub

	'''初始化
	'p_module:当前模块
	'p_controller:当前控制器
	'p_action:当前操作
	Public Sub Init(Byval p_module, Byval p_controller, Byval p_action)
		s_module = p_module
		s_controller = p_controller
		s_action = p_action
	End Sub

	'''设置是否进行当前页面HTML静态缓存
	'todo:经调试，发现服务器需关闭服务器端调试才能正常执行
	'(由于是在本处进行静态化缓存判断，故使用时应将该语句放在所有语句的最前面，以防止动态代码的再次执行)
	'设置该属性为True时即可以对当前页面进行缓存
	'对应缓存页面应是在多次运行时无逻辑变动(因为使用HTTP进行了二次访问,当然,既然进行了HTML缓存那肯定就是因为无逻辑变动)
	'内置一个特殊的GET值,用于判断是否要进行HTML静态化(防止死循环)
	'p_bool:布尔值,是否生成HTML，如果设置成False时会顺便删除已生成的HTML缓存
	Public Sub Make(Byval p_bool)
		'HTML缓存位置
		Dim t_p : t_p = ASHAPO_RUNTIME_PATH & "/cache/html/" & s_module & "/" & s_controller & "/" & s_action & "/"
		Dim t_k
		'所有GET参数(不含GMAL及标识位)
		For Each t_k In Request.QueryString
			If t_k <> ASHAPO_VAR_MODULE And t_k <> ASHAPO_VAR_CONTROLLER And t_k <> ASHAPO_VAR_ACTION And t_k <> Sa.Config("FITER_DISABLE_HTML") Then
				t_p = t_p & t_k & "-" & Request.QueryString(t_k) & "_"
			End If
		Next
		t_p = Left(t_p, Len(t_p)-1)
		t_p = t_p & ".asp"
		Dim t_file : t_file = t_p
		If p_bool Then
			'标识位为空时则进行HTML缓存
			If Request.QueryString( Sa.Config("FITER_DISABLE_HTML") )="" Then
				If Global.Fso.IsFile( t_file ) Then
					'访问当前已缓存的asp运行文件
					Server.Transfer( t_file )
				Else
					Global.Asp.Use(ASHAPO_CORE_PATH & "/core/AshapoHttp")
					Dim t_http : Set t_http = New AshapoHttp
					
					Dim t_url : t_url = Request.ServerVariables("SCRIPT_NAME")
					Dim t_par : t_par = Request.ServerVariables("QUERY_STRING")
					If t_par <> "" Then
						t_url = t_url & "?" & t_par & "&" & Sa.Config("FITER_DISABLE_HTML") & "=1"
					Else
						t_url = t_url & "?" & Sa.Config("FITER_DISABLE_HTML") & "=1"
					End If
					Dim t_html : t_html = t_http.Get( t_url )
					Set t_http = Nothing
					'保存到HTML文件夹
					Call Global.Fso.CreateFile(t_file, t_html)
				End If
			End If
		Else
			'''是否已有生成的HTML文件需要删除
			If Global.Fso.IsFile( t_file ) Then
				Global.Fso.DeleteFile( t_file )
			End If
		End If
	End Sub
End Class
%>