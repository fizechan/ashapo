<%
'''全局对象Runtime
'''Runtime提供了一个业务无关的系统运行时
'''一般情况下你不会用到这个对象
'''依赖文件:
'''functions.asp
'''AshapoPreg.class.asp
'''AshapoString.class.asp
'''AshapoDatetime.class.asp
'''AshapoFso.class.asp
'''AshapoAsp.class.asp
'''注意外部仅实例化一个Runtime对象，不允许再实例化另外的对象
Class AshapoRuntime

	'''所有使用到的环境组件名字典
	Private s_progIDs

	'''全局使用的正则Preg对象
	Public Preg

	'''全局使用的String对象
	Public [String]

	'''全局使用的DateTime对象
	Public [DateTime]

	'''全局使用的Fso对象
	Public Fso

	'''全局使用的Asp增强对象
	Public Asp

	'''其他静态对象字典
	Private s_static
	
	'''构造
	Private Sub Class_Initialize()
		'如果修改了词典组件名则必须也在本处进行修改，否则导致组件无法找到
		Dim t_dict_name : t_dict_name = "Scripting.Dictionary"
		Set s_progIDs = Server.CreateObject(t_dict_name)
		Call setProgID_()

		Set Preg = New AshapoPreg

		Set [String] = New AshapoString

		Set [DateTime] = New AshapoDatetime

		Set Fso = New AshapoFso
		Call Fso.init(ProgID("Scripting.FileSystemObject"))
		
		Set Asp = New AshapoAsp
		Call Asp.init(ProgID("Scripting.Dictionary"))

		Set s_static = Server.CreateObject(t_dict_name)
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Dim t_key
		For Each t_key In s_static.Keys
			Set s_static(t_key) = Nothing
		Next
		Set s_static = Nothing
		Set Asp = Nothing
		Set Fso= Nothing
		Set [DateTime] = Nothing
		Set [String] = Nothing
		Set Preg = Nothing
	End Sub

	'''设置所有可能的组件
	'''如果有用到其他的自定义组件名，可自行添加，或者调用Let ProgID方法
	Private Sub setProgID_()
		
		s_progIDs("adodb.connection") 				= "Adodb.Connection"
		s_progIDs("adodb.command") 					= "Adodb.Command"
		s_progIDs("adodb.recordset") 				= "Adodb.Recordset"
		s_progIDs("adodb.stream") 					= "Adodb.Stream"
		
		
		s_progIDs("microsoft.xmlhttp")				= "Microsoft.XMLHTTP"
		
		s_progIDs("msscriptcontrol.scriptcontrol")  = "MSScriptControl.ScriptControl"
		
		s_progIDs("msxml2.serverxmlhttp")			= "MSXML2.serverXMLHTTP"
		s_progIDs("msxml2.xmlhttp")					= "MSXML2.XMLHTTP"
		

		s_progIDs("scripting.dictionary") 			= "Scripting.Dictionary"
		s_progIDs("scripting.filesystemobject") 	= "Scripting.FileSystemObject"

	End Sub

	'''返回当前环境的组件的真实名称，这对于组件名称自定义的情况非常有用
	'p_name:组件的官方名，不区分大小写
	Public Property Get ProgID(Byval p_name)
		ProgID = s_progIDs(Lcase(p_name))
	End Property

	'''设置组件名
	'p_original_name: 组件原本的官方名称，不区分大小写
	'p_current_name: 当前环境下的该组件名称
	Public Property Let ProgID(Byval p_original_name, Byval p_current_name)
		s_progIDs(Lcase(p_original_name)) = p_current_name
	End Property

	'''获取其他内部使用的静态对象
	'p_name: 对象名
	Public Default Property Get [Static](Byval p_name)
		Select Case p_name
		Case "Asp"
			Set [Static] = Asp
		Case "DateTime"
			Set [Static] = [DateTime]
		Case "Fso"
			Set [Static] = Fso
		Case "Preg"
			Set [Static] = Preg
		Case "String"
			Set [Static] = [String]
		Case Else
			If Not Has(s_static(p_name)) Then
				Asp.Include(ASHAPO_CORE_PATH & "/core/Ashapo" & p_name & ".class.asp")
				Set s_static(p_name) = Eval("New Ashapo" & p_name)
			End If
			Set [Static] = s_static(p_name)
		End Select
	End Property

	'''设置当前设置
	'''todo 考虑移除
	'''如果在公共模块中调用，则作用于全部模块
	'''如果在配置文件中调用，则作用于整个所属模块
	'''如果临时调用则在当前运行时有效
	'p_key:设置键名，不区分大小写
	'p_val:键值，可以是任意变量
	Public Property Let [Static](Byval p_name, Byval p_object)
		Set s_static(Ucase(p_name)) = p_object
	End Property

	'''变量预定义
	'''支持数组定义,数组定义和原生语法保持一致
	'''例参数"arr(5)"表示定义名为arr的一维数组,其数组大小为6(最大可用下标5)
	'''例参数"arr(5,4)"表示定义名为arr的二维数组,其数组大小为6X5
	'p_name:变量名
	Public Sub [Dim](Byval p_name)
		On Error Resume Next
		ExecuteGlobal("Dim " & p_name)
		If Err.Number<>0 Then
			Die("CORE ERROR XXX: Error while defining variable " & p_name & ".")
		End If
	End Sub

	'''简易的通用字典定义
	Public Property Get Dictionary()
		Set Dictionary = Server.CreateObject( ProgID("Scripting.Dictionary") )
	End Property
End Class
%>