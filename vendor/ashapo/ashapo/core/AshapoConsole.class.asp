<%
'''底层控制台类
'获取运行时的各环境变量及响应变动
Class AshapoConsole
	
	'''进行的数据库查询次数(待移除)
	Public DbQueryTimes
	
	'''内部使用AshapoLoger对象名称
	Private s_loger
	
	'''当前是否处于控制台分组模式
	Private s_isGroup
	
	'''分组模式下要写入的日志
	Private s_preLog
	
	'''分组开始的时间
	Private s_time_begin
	
	'''构造
	Private Sub Class_Initialize()
		DbQueryTimes = 0
		s_isGroup = False
		s_preLog = vbCrLf
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_loger = Nothing
	End Sub

	'''设置控制台的日志驱动
	'p_driver:日志驱动名称
	Public Function SetLogerDriver(Byval p_driver)
		''AshapoLoger已载入
		Set s_loger = New AshapoLoger
		s_loger.Path = "console.log"
		Call s_loger.Init(p_driver)
		Set SetLogerDriver = Me
	End Function
	
	'''设置控制台的日志路径
	'p_path:日志路径
	Public Default Function SetLogPath(Byval p_path)
		s_loger.Path = p_path
		Set SetLogPath = Me
	End Function
	
	'''在分组中写入日志
	'p_type:日志类型
	'p_msg:日志主体
	Private Sub preLog_(Byval p_type, Byval p_msg)
		s_preLog = s_preLog & "[" & p_type & "]" & p_msg & vbCrLf
	End Sub
	
	'''开始新的一个控制台记录组
	'p_msg:日志主体
	Public Sub GroupBegin(Byval p_msg)
		If s_isGroup Then
			'已有一个控制台则发生警告信息
			Call preLog_("WARNX", "当前控制台记录组被强制结束了")
			s_loger.Info(s_preLog)
			s_preLog = p_msg & vbCrLf
		Else
			s_time_begin = Timer()
			s_isGroup = True
			Call preLog_("INFOX", "[" & s_time_begin &"]" & p_msg)
		End If
	End Sub
	
	'''结束当前的控制台记录组
	'p_msg:日志主体
	Public Sub GroupEnd(Byval p_msg)
		If s_isGroup Then
			Dim t_time_end : t_time_end = Timer()
			Dim t_time_used : t_time_used = t_time_end - s_time_begin
			Call preLog_("INFOX", "[" & t_time_end & "][用时" & t_time_used & "秒]" & p_msg)
			s_loger.Level = "GROUP"
			s_loger.Write(s_preLog)
		Else
			'非法关闭控制台记录组
			s_log.Warn("非法关闭控制台记录组:" & p_msg)
		End If
		s_isGroup = False
		s_preLog = vbCrLf
		s_loger.Info("")
	End Sub

	'''全局写入日志
	'p_msg:日志主体
	Public Sub [Log](Byval p_msg)
		If s_isGroup Then
			Call preLog_("LOGXX", p_msg)
		Else
			s_loger.Info(p_msg)
		End If
	End Sub
	
	'''全局INFO日志
	'p_msg:日志主体
	Public Sub Info(Byval p_msg)
		If s_isGroup Then
			Call preLog_("INFOX", p_msg)
		Else
			s_loger.Info(p_msg)
		End If
	End Sub
	
	'''全局SQL日志
	'p_msg:SQL日志主体
	Public Sub SQL(Byval p_msg)
		If s_isGroup Then
			Call preLog_("SQLXX", p_msg)
		Else
			s_loger.SQL(p_msg)
		End If
	End Sub
	
	'''全局Debug日志
	'p_msg:日志主体
	Public Sub [Debug](Byval p_msg)
		If s_isGroup Then
			Call preLog_("DEBUG", p_msg)
		Else
			s_loger.Debug(p_msg)
		End If
	End Sub
	
	'''全局Warn日志
	'p_msg:日志主体
	Public Sub [Warn](Byval p_msg)
		If s_isGroup Then
			Call preLog_("WARNX", p_msg)
		Else
			s_loger.Warn(p_msg)
		End If
	End Sub
	
	'''严重错误，直接抛出错误信息
	'p_msg:日志主体
	Public Sub [Error](Byval p_msg)
		If s_isGroup Then
			'写入日志后立马阻断运行
			Call preLog_("ERROR", p_msg)
			GroupEnd("当前程序由于发生错误而停止")
		Else
			s_loger.Error(p_msg)
		End If
		'阻断运行
		Die(p_msg)
	End Sub
	
	'''断言
	'p_tag TAG标识位
	'p_ass 布尔值,如果为False则出错
	Public Sub [Assert](Byval p_tag, Byval p_ass)
		If Not p_ass Then
			[Error](p_tag & "断言失败")
		End If
	End Sub
End Class
%>