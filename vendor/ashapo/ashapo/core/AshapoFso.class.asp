<%
'''底层文件类
'''使用了Global全局对象
'''核心FSO错误需直接抛出
Class AshapoFso

	'内部使用Fso名称
	Private s_fso
	
	'''构造
	Private Sub Class_Initialize()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_fso = Nothing
	End Sub

	'''初始化
	'''@todo 系统自动调用该方法初始化，我们并不希望外部手动调用该方法
	'''p_fso_name: 组件Scripting.FileSystemObject的真实名称
	Public Sub Init(Byval p_fso_name)
		Set s_fso = Server.CreateObject(p_fso_name)
	End Sub
	
	'''获取实际路径MapPath,不支持通配符
	'p_path:路径
	Public Function AbsPath(Byval p_path)
		On Error Resume Next
		If CStr(p_path) = "" Then
			AbsPath = ""
			Exit Function
		End If
		If Mid(p_path, 2, 1) <> ":" Then
			p_path = Server.MapPath(p_path)
		End If
		If Right(p_path, 1) = "\" Then
			p_path = Left(p_path, Len(p_path) - 1)
		End If
		AbsPath = p_path
		If Err.Number<>0 Then
			Die("CORE ERROR 001:""" & p_path & """ is not a valid path")
		End If
	End Function
	
	'''获取能用于include的文件实际路径
	'''@todo 写法不够标准，待修正
	'p_path:路径
	Public Function AbsPathForWeb(Byval p_path)
		AbsPathForWeb = "/" & Runtime.String.CRight( AbsPath(p_path), Request.ServerVariables("APPL_PHYSICAL_PATH") )
		AbsPathForWeb = Replace(AbsPathForWeb, "\", "/")
	End Function

	'''判断文件是否存在
	'p_path:文件路径
	Public Function IsFile(Byval p_path)
		p_path = AbsPath(p_path)
		IsFile = False
		If s_fso.FileExists(p_path) Then
			IsFile = True
		End If
	End Function
	
	'''判断服务器上的文件夹是否存在
	'p_path:文件夹路径
	Public Function IsFolder(Byval p_path)
		p_path = AbsPath(p_path)
		IsFolder = False
		If s_fso.FolderExists(p_path) Then
			IsFolder = True
		End If
	End Function
	
	'''读取服务器端文件内容，仅支持UTF-8编码
	'p_path:文件路径
	Public Function ReadFile(Byval p_path)
		p_path = AbsPath(p_path)
		If IsFile(p_path) Then
			Dim t_obj
			Set t_obj = s_fso.OpenTextFile(p_path, 1, false)
			ReadFile = t_obj.ReadAll()
			t_obj.Close()
			Set t_obj = Nothing
		Else
			'抛出错误
			Die("CORE ERROR 002:the file """ & p_path & """ could not be read")
		End If
	End Function
	
	'''在服务器上创建文件夹,如果文件夹已存在则不需创建,支持多层目录创建
	'''如果创建失败，将显示底层错误
	'p_path:文件夹路径
	Public Sub CreateFolder(Byval p_path)
		'On Error Resume Next
		Dim t_p, t_ap, t_i
		p_path = AbsPath(p_path)
		t_ap = Split(p_path, "\") : t_p = ""
		For t_i = 0 To Ubound(t_ap)
			t_p = t_p & t_ap(t_i) & "\"
			If Instr(t_p, AbsPath("/") & "\")>0 Then
				If Not IsFolder(t_p) And t_i>0 Then
					s_fso.CreateFolder(t_p)
				End If
			End If
		Next
		If Err.Number<>0 Then
			Die("CORE ERROR 003:unable to create folder """ & p_path & """")
		End If
	End Sub
	
	'''在服务器端创建文件并写入内容
	'p_path:文件路径
	'p_content:写入内容
	Public Sub CreateFile(Byval p_path, Byval p_content)
		On Error Resume Next
		p_path = AbsPath(p_path)
		CreateFolder(Left(p_path, InstrRev(p_path, "\")-1))
		Dim t_obj
		Set t_obj = s_fso.OpenTextFile(p_path, 2, true)
		t_obj.Write(p_content)
		t_obj.Close()
		Set t_obj = Nothing
		If Err.Number<>0 Then
			Die("CORE ERROR 004:unable to create file """ & p_path & """")
		End If
	End Sub
	
	'''追加文件的内容(目前仅用于Log调用)
	'p_path:路径
	'p_content:文件追加内容字符串
	Public Sub AppendFile(Byval p_path, Byval p_content)
		On Error Resume Next
		p_path = AbsPath(p_path)
		CreateFolder(Left(p_path, InstrRev(p_path, "\")-1))
		Dim t_obj
		Set t_obj = s_fso.OpenTextFile(p_path, 8, true)
		t_obj.Write(p_content)
		t_obj.Close()
		Set t_obj = Nothing
		If Err.Number<>0 Then
			Die("CORE ERROR 004:unable to append file """ & p_path & """")
		End If
	End Sub
	
	'''列出服务器上指定目录下所有的文件或文件夹
	'p_path:目录路径
	'p_type:指定类型(文件或文件夹或全部)
	Public Function [List](Byval p_path, Byval p_type)
		'On Error Resume Next
		Dim t_f, t_fs, t_k, t_a(), t_i, t_l
		p_path = AbsPath(p_path)
		t_i = 0
		Select Case LCase(p_t)
		Case "1","file"
			t_l = 1
		Case "2","folder"
			t_l = 2
		Case Else
			t_l = 0
		End Select
		Set t_f = s_fso.GetFolder(p_path)
		If t_l = 0 Or t_l = 2 Then
			Set t_fs = t_f.SubFolders
			ReDim Preserve t_a(t_fs.Count + t_i - 1)
			For Each t_k In t_fs
				Set t_a(t_i) = Server.CreateObject( Runtime.ProgID("Scripting.Dictionary") )
				t_a(t_i)("Name") = t_k.Name
				t_a(t_i)("DateLastModified") = t_k.DateLastModified
				t_a(t_i)("Type") = t_k.Type
				t_i = t_i + 1
			Next
		End If
		If t_l = 0 Or t_l = 1 Then
			Set t_fs = t_f.Files
			ReDim Preserve t_a(t_fs.Count + t_i - 1)
			For Each t_k In t_fs
				Set t_a(t_i) = Server.CreateObject( Runtime.ProgID("Scripting.Dictionary") )
				t_a(t_i)("Name") = t_k.Name
				t_a(t_i)("DateLastModified") = t_k.DateLastModified
				t_a(t_i)("Type") = t_k.Type
				t_i = t_i + 1
			Next
		End If
		Set t_fs = Nothing
		Set t_f = Nothing
		[List] = t_a
		If Err.Number<>0 Then
			Die("CORE ERROR 005:unable to get """ & p_path & """ information")
		End If
		Err.Clear()
	End Function
	
	'''从路径或者完整文件名获取不含后缀的文件名
	'p_file:文件名(可以包含路径)
	Public Function NameOfFile(Byval p_file)
		Dim t_re
		If IsN(p_file) Then
			NameOfFile = ""
			Exit Function
		End If
		p_file = Replace(p_file, "\", "/")
		If Right(p_file, 1) = "/" Then
			t_re = Split(p_f, "/")
			NameOfFile = IIF(p_t = 0, t_re(Ubound(t_re)-1), "")
			Exit Function
		ElseIf Instr(p_file, "/") > 0 Then
			t_re = Split(p_file, "/")(Ubound(Split(p_file, "/")))
		Else
			t_re = p_f
		End If
		If Instr(t_re,".")>0 Then
			NameOfFile = Left(t_re,InstrRev(t_re,".")-1)
		Else
			NameOfFile = t_re
		End If
	End Function
	
	'''删除文件
	'p_path:要删除的文件路径
	Public Sub DeleteFile(Byval p_path)
		If Not IsFile(p_path) Then
			Exit Sub
		End If
		'On Error Resume Next
		p_path = AbsPath(p_path)
		Call s_fso.DeleteFile(p_path, True)
		If Err.Number<>0 Then
			Die("CORE ERROR XXX:unable to delete file """ & p_path & """")
		End If
	End Sub
	
	'''获取指定文件的最后修改时间戳
	'p_path:指定文件路径
	Public Function GetFileDateLastModified(Byval p_path)
		On Error Resume Next
		p_path = AbsPath(p_path)
		Dim t_f
		Set t_f = s_fso.GetFile(p_path)
		If Err.Number<>0 Then
			Die("CORE ERROR XXX:unable to Get File DateLastModified """ & p_path & """")
		End If
		GetFileDateLastModified = Global.DateTime.ToTimeStamp(t_f.DateLastModified)
		Set t_f = Nothing
	End Function
End Class
%>