<%
'''底层日志类
'''使用了Global全局对象
Class AshapoLoger
	'日志输出等级: Error < Warning < Info < Debug
	'等级,格式,产生日志的页面
	Private s_level, s_format, s_npage
	'Level的替代转化符,Path的替代转化符, Time的替代转化符
	Private s_lvp, s_pap, s_tmp
	
	'''日志驱动
	Private s_driver
	
	'''构造
	Private Sub Class_Initialize()
		Init("File")
		s_level = "INFOX"
		s_lvp = "{%level%}"
		s_pap = "{%path%}"
		s_tmp = "{%time%}"
		s_npage = Request.ServerVariables("SCRIPT_NAME")
		Dim t_s : t_s = Request.ServerVariables("QUERY_STRING")
		If t_s <> "" Then
			s_npage = s_npage & "?" & t_s
		End If
		s_format = "Time:[" & s_tmp & "] Level:[" & s_lvp & "] Path:[""" & s_pap & """]"
	End Sub

	'''析构
	Private Sub Class_Terminate()
		Set s_driver = Nothing
	End Sub

	'''通过驱动对外暴露缓存接口
	'p_dirver:驱动名
	Public Function Init(Byval p_dirver)
		Select Case p_dirver
		Case "File"
			Call Runtime.Asp.Use( ASHAPO_CORE_PATH & "/driver/loger/AshapoLogerDriverFile" )
			Set s_driver = New AshapoLogerDriverFile
		Case "File"
			Call Runtime.Asp.Use( ASHAPO_CORE_PATH & "/driver/loger/AshapoLogerDriverServer" )
			Set s_driver = New AshapoLogerDriverServer
		End Select
	End Function

	'''设置输出的路径。如果是文件夹路径则自动建立以时间为基准的日志文件,否则按指定路径输出
	'p_p:文件或文件夹路径
	Public Property Let Path(Byval p_p)
	     s_driver.Path = p_p
	End Property
	
	'''获取日志输出的路径,写入业务日志时会用到
	Public Property Get Path()
		Path = s_driver.Path
	End Property
	
	'''设置单次输出路径,使用一次后路径自动改回原本的路径
	Public Function PathOnce(Byval p_p)
		s_driver.PathOnce = p_p
	End Function

	'''格式化当前时间格式
	Private Function formatDt_()
		Dim t_y : t_y = Year(Now())
		Dim t_m : t_m = Right("0" & Month(Now()), 2)
		Dim t_d : t_d = Right("0" & Day(Now()), 2)
		Dim t_h : t_h = Right("0" & Hour(Now()), 2)
		Dim t_n : t_n = Right("0" & Minute(Now()), 2)
		Dim t_s : t_s = Right("0" & Second(Now()), 2)
		formatDt_ = t_y & "-" & t_m & "-" & t_d & " " & t_h & ":" & t_n & ":" & t_s
	End Function
	
	'''为当前欲写入的日志设置等级
	'p_s:Error < Warning < Info < Debug
	Public Property Let Level(Byval p_s)
		Select Case Lcase(p_s)
		Case "info"
			s_level = "INFOX"
		Case "sql"
			s_level = "SQLXX"
		Case "warning"
			s_level = "WARNX"
		Case Else
			s_level = Ucase(p_s)
		End Select
	End Property
	
	'''自定义格式化日志
	'p_s:格式化字符,特征值:1\2\3\simple\middle\full
	Public Property Let [Format](Byval p_s)
		Select Case Lcase(Cstr(p_s))
		Case "1","simple"
			s_format = "Level:[" & s_lvp & "]"
		Case "2","middle"
			s_format = "Time:[" & s_tmp & "] Level:[" & s_lvp & "]"
		Case "3","full"
			s_format = "Time:[" & s_tmp & "] Level:[" & s_lvp & "] Path:[""" & s_pap & """]"
		Case Else
			s_format = p_s
		End Select
	End Property
	
	'写入日志。总入口，其余扩展请基于本函数实现
	'p_s:日志实体,不含前缀格式化
	Public Sub [Write](Byval p_s)
		Dim t_t : t_t = s_format
	    t_t = Replace(t_t, s_lvp, s_level)
		t_t = Replace(t_t, s_pap, s_npage)
		t_t = Replace(t_t, s_tmp, formatDt_())
		Call s_driver.Write(t_t & " " & p_s)
	End Sub
	
	'''写Debug日志
	'p_s:日志实体,不含前缀格式化
	Public Sub [Debug](Byval p_s)
		s_level = "DEBUG"
		[Write](p_s)
	End Sub
	
	'''写入Info日志
	'p_s:日志实体,不含前缀格式化
	Public Sub Info(Byval p_s)
		s_level = "INFOX"
		[Write](p_s)
	End Sub
	
	'''写入Warn日志
	'p_s:日志实体,不含前缀格式化
	Public Sub Warn(Byval p_s)
		s_level = "WARNX"
		[Write](p_s)
	End Sub
	
	'''写入SQL日志
	'p_s:日志实体，建议纯SQL语句
	Public Sub SQL(Byval p_s)
		s_level = "SQLXX"
		[Write](p_s)
	End Sub
	
	'''写入Error日志
	'p_s:日志实体,不含前缀格式化
	Public Sub [Error](Byval p_s)
		s_level = "ERROR"
		[Write](p_s)
	End Sub
End Class
%>