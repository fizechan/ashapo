<%
'''视图模版引擎类
Class AshapoView

	'''驱动类对象
	Private s_tpl

	'''当前模块
	Private s_module
	
	'''当前控制器
	Private s_controller
	
	'''当前操作
	Private s_action

	'''当前主题
	Private s_theme

	'''当前视图完整路径
	Private s_view
	
	'''当前视图文件解析后保存的文件名称(不含路径)
	Private s_view_output_file
	
	'''当前文件在解析过程中可能涉及的各include文件检测工具文件
	Private s_view_output_map

	'''构造
	Private Sub Class_Initialize()
	End Sub
	
	'''析构
	Private Sub Class_Terminate()
		Set s_tpl = Nothing
	End Sub

	'''初始化
	'p_module:模块
	'p_controller:控制器
	'p_action:操作
	Public Default Function Init(Byval p_module, Byval p_controller, Byval p_action)
		Call Runtime.Asp.Use(ASHAPO_CORE_PATH & "/driver/view/AshapoViewDriver" & Sa.Config("TMPL_ENGINE_DRIVER"))
		Set s_tpl = Eval("New AshapoViewDriver" & Sa.Config("TMPL_ENGINE_DRIVER"))

		s_module = p_module
		s_controller = p_controller
		s_action = p_action
	End Function
	
	'''返回模板驱动原型
	Public Property Get Prototype()
		Set Prototype = s_tpl
	End Property
	
	'''设置要解析的模板路径
	'p_path:模板路径
	Public Sub SetPath(Byval p_path)
		s_tpl.SetPath(p_path)
	End Sub
	
	'''解析模板,返回处理结果
	'p_replace_dict:要进行替换的Scripting.Dictionary
	Public Function Resolve(Byval p_replace_dict)
		Dim t_key, t_str
		t_str = s_tpl.Resolve()
		For Each t_key In p_replace_dict
			t_str = Replace(t_str, t_key, p_replace_dict(t_key))
		Next
		Resolve = t_str
	End Function
	
	'''获取当前解析的文件共使用了哪些文件及每个文件的最后修改时间
	Public Property Get CacheMap()
		CacheMap = s_tpl.CacheMap
	End Property

	'''指定当前使用的模板主题，支持链式调用
	'p_name:模板主题名称
	Public Function Theme(Byval p_name)
		s_theme = p_name
		Set Theme = Me
	End Function

	'''指定视图文件，支持链式调用
	'''路径使用:
	'''1:完整路径，以"/"作为开头
	'''2:当前模块模版(read)
	'''3:其他模块模板(public:header)
	'''4:其他主题模块模板(bule:user:read),不建议使用
	'''5:其他分组主题模块模板(admin:bule:user:read),不建议使用
	'p_tpl:要指定的视图文件
	Public Function Path(Byval p_tpl)
		If Not Left(p_tpl, 1) = "/" Then
			Dim t_module, t_theme, t_controller, t_action
			t_module = s_module
			t_theme = s_theme
			t_controller = s_controller
			t_action = s_action
			Dim t_a1, t_a2, t_a3
			t_a1 = Runtime.String.ParamRev(p_tpl)
			t_action = t_a1(0)
			If Has(t_a1(1)) Then
				t_a2 = Runtime.String.ParamRev(t_a1(1))
				t_controller = t_a2(0)
				If Has(t_a2(1)) Then
					t_a3 = Runtime.String.ParamRev(t_a2(1))
					t_theme = t_a3(0)
					If Has(t_a3(1)) Then
						t_module = t_a3(1)
					End If
				End If
			End If
			If t_theme = "" Then
				s_view = ASHAPO_APP_PATH & "/" & t_module & "/view/" & t_controller & "/" & t_action & Sa.Config("TMPL_TEMPLATE_SUFFIX")
				s_view_output_file = Lcase(t_module & "__" & t_controller & "_" & t_action & ".asp")
				s_view_output_map = Lcase(t_module & "__" & t_controller & "_" & t_action)
			Else
				s_view = ASHAPO_APP_PATH & "/" & t_module & "/view/" & t_theme & "/" & t_controller & "/" & t_action & Sa.Config("TMPL_TEMPLATE_SUFFIX")
				s_view_output_file = Lcase(t_module & "_" & t_theme & "_" & t_controller & "_" & t_action & ".asp")
				s_view_output_map = Lcase(t_module & "_" & t_theme & "_" & t_controller & "_" & t_action)
			End If
		Else
			s_view = p_tpl
			Dim t_temp
			t_temp = Replace( p_tpl, "/", "_1_" )
			t_temp = Replace( t_temp, ".", "_0_" )
			s_view_output_file = Lcase(t_temp & ".asp")
			s_view_output_map = Lcase(t_temp)
		End If
		Set Path = Me
	End Function

	'''解析模板文件并运行
	Public Sub Display()
		If IsN(s_view) Then
			Path(s_action)
		End If
		
		If Not Runtime.Fso.IsFile(s_view) Then
			Die("CORE ERROR XXX:template file [" & s_view & "] does not exist.")
		End If
		
		If Ucase(Sa.Config("TMPL_ENGINE_TYPE")) = "ASP" Then
			'''原生ASP文件
			Call Runtime.Asp.RunFile(s_view, False)
		Else
			'''使用模板引擎
			'''检测缓存是否过期
			If ASHAPO_DEBUG Then
				Sa.Console.Debug("开始检测视图缓存文件是否过期")
				If Runtime.Fso.IsFile(ASHAPO_RUNTIME_PATH & "/cache/map/view_include/" & s_view_output_map) Then
					Dim t_str : t_str = Runtime.Fso.ReadFile(ASHAPO_RUNTIME_PATH & "/cache/map/view_include/" & s_view_output_map)
					Dim t_map, t_maps : t_maps = Split(t_str, "|")
					
					For Each t_map In t_maps
						Dim t_enm : t_enm = Split(t_map, ":")
						If UBound(t_enm) <> 1 Then
							Sa.Console.Warn("发现意外情况的MAP个数" & t_enm)
							Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/map/view_include/" & s_view_output_map)
							Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/view/" & s_view_output_file)
							Exit For
						End If
						If Cstr( Runtime.Fso.GetFileDateLastModified( t_enm(0) ) ) <> t_enm(1) Then
							Sa.Console.Info("视图缓存文件过期")
							Sa.Console.Info("实际文件时间" & Cstr( Runtime.Fso.GetFileDateLastModified( t_enm(0) ) ))
							Sa.Console.Info("缓存文件时间" & Cstr( t_enm(1) ))

							Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/map/view_include/" & s_view_output_map)
							Runtime.Fso.DeleteFile(ASHAPO_RUNTIME_PATH & "/cache/view/" & s_view_output_file)
						End If
					Next
					
				End If
			End If


			Dim t_recheck : t_recheck = False  ''是否重新检查RunFile文件
			If Not Runtime.Fso.IsFile(ASHAPO_RUNTIME_PATH & "/cache/view/" & s_view_output_file) Then
				Sa.Console.Debug("视图缓存文件不存在或已过期，将重新生成")
				t_recheck = True
				s_tpl.SetPath(s_view)
				Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/view/" & s_view_output_file, Resolve( Sa.Config("TMPL_PARSE_STRING") ))
				If s_tpl.CacheMap <> "" Then
					Call Runtime.Fso.CreateFile(ASHAPO_RUNTIME_PATH & "/cache/map/view_include/" & s_view_output_map, s_tpl.CacheMap)
				End If
			End If
			Call Runtime.Asp.RunFile(ASHAPO_RUNTIME_PATH & "/cache/view/" & s_view_output_file, t_recheck)
		End If
	End Sub
End Class
%>