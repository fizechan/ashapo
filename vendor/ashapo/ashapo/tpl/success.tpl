<!DOCTYPE html>
<html>
 <head>
  <title>{$T_Title}</title>
  <meta http-equiv="Content-Type" content="text/html; charset=gbk"/>		
 </head>
 <body>
<div id="content" class="white">            
<h1>
 {$T_Title}
</h1>
<div class="notif success bloc">
	<strong>{$T_Title} :</strong>
	{$T_Message}
    <a href="javascript:void();" class="close"></a>
</div>
</div>
<script language="javascript">
	setTimeout("location.href='{$T_JumpUrl}';",{$T_WaitSecond}*1000);
</script>
</body>
</html>