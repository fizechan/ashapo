<%
'''依赖层级:000
'''ashapo系统函数库
'''核心运行所需的基本函数,不允许删除,可在项目中直接使用

'''对变量和对象的赋值方式进行一致性处理，这在目标为未知变量时特别有用
'p_param:要被赋值的变量
'p_value:要赋值的值，可以是任意类型的变量
Function Assign(Byref p_param,  Byval p_value)
	If IsObject(p_value) Then
		Set p_param = p_value
	Else
		p_param = p_value
	End If
End Function


'''判断三元表达式
'p_condition:判断条件
'p_true:条件符合时返回值
'p_false:条件未符合返回值
Function IIF(Byval p_condition, Byval p_true, Byval p_false)
	If p_condition Then
		IIF = p_true
	Else
		IIF = p_false
	End If
End Function

'''二元表达式，如果条件成立则返回某个值,否则返回空字符串
'p_condition:判断条件
'p_true:条件符合时返回值
Function IfThen(Byval p_condition, Byval p_true)
	IfThen = IIF(p_condition, p_true, "")
End Function

'''判断变量是否为空值,支持字符串、数组、记录集、Dictionary等对象
'p_variable:要判断的变量
Function IsN(Byval p_variable)
	IsN = False
	Select Case VarType(p_variable)
	Case vbEmpty, vbNull
		IsN = True
		Exit Function
	Case vbString
		If p_variable = "" Then
			IsN = True
			Exit Function
		End If
	Case vbObject
		Select Case TypeName(p_variable)
		Case "Nothing","Empty"
			IsN = True
			Exit Function
		Case "Recordset"
			If p_variable.State = 0 Then
				IsN = True
				Exit Function
			End If
			If p_variable.Bof And p_variable.Eof Then
				IsN = True
				Exit Function
			End If
		Case "Dictionary"
			If p_variable.Count = 0 Then
				IsN = True
				Exit Function
			End If
		End Select
	Case vbArray, 8194, 8204, 8209
		If Ubound(p_variable) = -1 Then
			IsN = True
			Exit Function
		End If
	End Select
End Function

'''判断变量是否不为空，支持字符串、数组、记录集、Dictionary等对象
'p_variable:要判断的变量
Function Has(Byval p_variable)
	Has = Not IsN(p_variable)
End Function

'''如果某值存在返回某值，否则返回指定值
'p_variable:要判断的变量
'p_default:判断值不存在时返回的指定值
Function IfHas(ByVal p_variable, ByVal p_default)
	IfHas = IIF(Has(p_variable), p_variable, p_default)
End Function

'''获取两值的大值
'''@todo 待转移
'p_value1:第一个值
'p_value2:第二个值
Function Max(Byval p_value1, Byval p_value2)
	Max = IIF(p_value1 >= p_value2, p_value1, p_value2)
End Function
	
'''获取两值的小值
'''@todo 待转移
'p_value1:第一个值
'p_value2:第二个值
Function Min(Byval p_value1, Byval p_value2)
	Min = IIF(p_value1 < p_value2, p_value1, p_value2)
End Function

'''判断值是否在数组内
'p_value:进行判断的值
'p_array：被判断的数组
Function IsIn(Byval p_value, Byval p_array)
	IsIn = False
	Dim t_item
	For Each t_item In p_array
		If t_item = p_value Then
			IsIn = True
			Exit Function
		End If
	Next
End Function

'''模拟php的die函数
'''如果全局AshapoDebug变量为true则会输出具体错误
'p_s:要输出的字符串
Sub Die(Byval p_string)
	'Response.Clear()
	If VarType(ASHAPO_DEBUG) <> vbEmpty And ASHAPO_DEBUG = True Then
		Response.Write("<h1 style='color:red;'>SERIOUS ERROR</h1><p>" & p_string & "</p>")
	Else
		Response.Write("<h1 style='color:red;'>SERIOUS ERROR</h1>")
	End If
	Response.End()
End Sub


'''检测某个服务器组件是否安装
'''todo 待转移
'p_objname:组件名
Function IsInstall(Byval p_objname)
	On Error Resume Next
	IsInstall = False
	Dim t_obj : Set t_obj = Server.CreateObject(p_objname)
	If Err.Number = 0 Then
		IsInstall = True
	End If
	Set t_obj = Nothing
	Err.Clear()
End Function

'''构造链接
'''指定p_num规则id时可以提升性能
'''@todo 待移除
Function U(Byval p_u, Byval p_n)
	If C_URL_MODEL = 1 Then
		U = p_u
		Exit Function
	End If
	Dim t_r, t_k, t_u : t_u = p_u
	Set t_r = New RegExp
	t_r.Global = True
	t_r.IgnoreCase = True
	t_r.Multiline = False
	If IsNumeric(p_n) Then
		If C_ROUTE_RULE.Exists(p_n) Then
			t_r.Pattern = C_ROUTE_RULE(p_n)(1)
			If t_r.Test(p_u) Then
				t_u = t_r.Replace(p_u, C_ROUTE_RULE(p_n)(2))
			Else
				t_u = p_u
			End If
		Else
			t_u = p_u
		End If
	Else
		'遍历C_ROUTE_RULE
		For Each t_k In C_ROUTE_RULE
			t_r.Pattern = C_ROUTE_RULE(t_k)(1)
			If t_r.Test(p_u) Then
				t_u = t_r.Replace(p_u, C_ROUTE_RULE(t_k)(2))
				Exit For
			End If
		Next
	End If
	Set t_r = Nothing
	U = t_u
End Function
%>