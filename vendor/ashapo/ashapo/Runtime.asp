<%
'''构建Global运行环境
'''是否调试模式
If VarType(ASHAPO_DEBUG) = vbEmpty Then
	ASHAPO_DEBUG = False
End If

'''站点根目录
If VarType(ASHAPO_ROOT_PATH) = vbEmpty Then
	ASHAPO_ROOT_PATH = ""
End If

'''业务目录
If VarType(ASHAPO_APP_PATH) = vbEmpty Then
	ASHAPO_APP_PATH = ASHAPO_ROOT_PATH & "/app"
End If

'''核心目录
If VarType(ASHAPO_CORE_PATH) = vbEmpty Then
	ASHAPO_CORE_PATH = ASHAPO_ROOT_PATH & "/ashapo"
End If

'''运行时目录
If VarType(ASHAPO_RUNTIME_PATH) = vbEmpty Then
	ASHAPO_RUNTIME_PATH = ASHAPO_ROOT_PATH & "/data/runtime"
End If

'''默认模块
If VarType(ASHAPO_DEFAULT_MODULE) = vbEmpty Then
	ASHAPO_DEFAULT_MODULE = "home"
End If

'''默认控制器
If VarType(ASHAPO_DEFAULT_CONTROLLER) = vbEmpty Then
	ASHAPO_DEFAULT_CONTROLLER = "Index"
End If

'''默认操作
If VarType(ASHAPO_DEFAULT_ACTION) = vbEmpty Then
	ASHAPO_DEFAULT_ACTION = "index"
End If

'''默认模块获取变量
If VarType(ASHAPO_VAR_MODULE) = vbEmpty Then
	ASHAPO_VAR_MODULE = "m"
End If

'''默认控制器获取变量
If VarType(ASHAPO_VAR_CONTROLLER) = vbEmpty Then
	ASHAPO_VAR_CONTROLLER = "c"
End If

'''默认操作获取变量
If VarType(ASHAPO_VAR_ACTION) = vbEmpty Then
	ASHAPO_VAR_ACTION = "a"
End If
%>
<!--#include file="common/functions.asp"-->
<!--#include file="core/AshapoPreg.class.asp"-->
<!--#include file="core/AshapoString.class.asp"-->
<!--#include file="core/AshapoDatetime.class.asp"-->
<!--#include file="core/AshapoFso.class.asp"-->
<!--#include file="core/AshapoAsp.class.asp"-->
<!--#include file="core/AshapoRuntime.class.asp"-->
<%Dim Runtime : Set Runtime = New AshapoRuntime%>